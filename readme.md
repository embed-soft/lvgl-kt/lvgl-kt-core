# LVGL KT Core (lvglkt-core)

A Kotlin Native library that provides Kotlin bindings to the [LVGL library](https://lvgl.io/), which only covers core
functionality. This library depends on Kotlin Native which is currently in beta. Below is the key functionality
provided by this library:

- Creation and management of LVGL objects
- Built in canvas for 2D drawing
- Basic color management
- Styling (covers local object styling, and separate styles)
- Some LVGL utility functions
- Basic screen management
- Management of string references
- LVGL object event handling
- Animations
- Fonts

The following Kotlin Native targets are supported:
- linuxX64
- linuxArm32Hfp


## Requirements

The following is required by the library in order to use it:

- Kotlin Native 1.7.21 or later
- Gradle 6.9.2 or later
- LVGL 8.3.3

Note that the **liblvgl.a** file (a static library file) **MUST** be generated for a Kotlin Native program using this 
library, from the LVGL source code via [cmake](https://cmake.org/). Alternatively an existing **liblvgl.a** file can be 
acquired from the [Counter sample](https://gitlab.com/embed-soft/lvgl-kt/samples/counter-sample/-/tree/master/lib).


## Usage

To use this library in a Kotlin Native program using Gradle add the library as a dependency. Add the following to 
the build file: `implementation(io.gitlab.embed-soft:lvglkt-core:0.4.0)`


## Objects

LVGL has its own object system. Most objects have a parent (one of the main exceptions are objects that act as 
screens). Each object acts as a widget/graphics primitive and has the following builtin features:
- Local styling/external styling via styles
- Event handling
- State (incl flags)
- User data
- Attributes (parent, size, position etc)

Use the `lvglObject` function (from 
[lvglObject.kt](src/commonMain/kotlin/io.gitlab.embedSoft.lvglKt.core/lvglObject/lvglObject.kt)) to create a new LVGL 
object, eg:
```kotlin
// ...
lvglObject {
    width = 30.toShort().percent
    height = 30.toShort().percent
    // ...
}
```

When finished with a LVGL object use the `close` function (from 
[LvglObjectBase.kt](src/commonMain/kotlin/io.gitlab.embedSoft.lvglKt.core/lvglObject/LvglObjectBase.kt)) to clean up 
the object (frees up resources like RAM for example). Note that closing an object which is a parent will also close its 
children, meaning that the children (incl its parent) can no longer be used.


## Styling

Each LVGL object can have local styling (at the object level), and external styling via styles. For local styling it 
looks like the following, eg:
```kotlin
// ...
lvglObject(parent = parent) {
    setWidthStyle(40.toShort().percent)
    setHeightStyle(60.toShort().percent)
}
```

With external styling it looks like the following, eg:
```kotlin
// ...
val objStyle = style {
    setWidth(40.toShort().percent)
    setHeight(60.toShort().percent)
}
lvglObject(parent = parent) {
    addStyle(objStyle)
}
```
