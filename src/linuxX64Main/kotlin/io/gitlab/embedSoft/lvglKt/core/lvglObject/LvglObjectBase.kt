package io.gitlab.embedSoft.lvglKt.core.lvglObject

import io.gitlab.embedSoft.lvglKt.core.Area
import io.gitlab.embedSoft.lvglKt.core.Closable
import io.gitlab.embedSoft.lvglKt.core.Point
import io.gitlab.embedSoft.lvglKt.core.StringReference
import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.*
import io.gitlab.embedSoft.lvglKt.core.event.LvglEventCallback
import io.gitlab.embedSoft.lvglKt.core.event.LvglEventType
import io.gitlab.embedSoft.lvglKt.core.event.value
import io.gitlab.embedSoft.lvglKt.core.styling.Style
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret

public actual interface LvglObjectBase : Closable {
    public val lvObjPtr: CPointer<lv_obj_t>?
    public actual var parent: LvglObjectBase?
        get() {
            val ptr = lv_obj_get_parent(lvObjPtr)
            return if (ptr != null) LvglObject.fromPointer(ptr) else null
        }
        set(value) = lv_obj_set_parent(lvObjPtr, value?.lvObjPtr)
    public actual val screen: LvglObjectBase
        get() = LvglObject.fromPointer(lv_obj_get_screen(lvObjPtr))
    public actual var contentWidth: Short
        get() = lv_obj_get_content_width(lvObjPtr)
        set(value) = lv_obj_set_content_width(lvObjPtr, value)
    public actual var contentHeight: Short
        get() = lv_obj_get_content_height(lvObjPtr)
        set(value) = lv_obj_set_content_height(lvObjPtr, value)
    public actual val isEditable: Boolean
        get() = lv_obj_is_editable(lvObjPtr)
    public actual val isScrolling: Boolean
        get() = lv_obj_is_scrolling(lvObjPtr)
    public actual val isVisible: Boolean
        get() = lv_obj_is_visible(lvObjPtr)
    public actual val selfWidth: Short
        get() = lv_obj_get_self_width(lvObjPtr)
    public actual val selfHeight: Short
        get() = lv_obj_get_self_height(lvObjPtr)
    public actual var xPos: Short
        get() = lv_obj_get_x(lvObjPtr)
        set(value) {
            lv_obj_set_x(lvObjPtr, value)
        }
    public actual var yPos: Short
        get() = lv_obj_get_y(lvObjPtr)
        set(value) {
            lv_obj_set_y(lvObjPtr, value)
        }
    public actual val scrollBottom: Short
        get() = lv_obj_get_scroll_bottom(lvObjPtr)
    public actual val scrollTop: Short
        get() = lv_obj_get_scroll_top(lvObjPtr)
    public actual val scrollLeft: Short
        get() = lv_obj_get_scroll_left(lvObjPtr)
    public actual val scrollRight: Short
        get() = lv_obj_get_scroll_right(lvObjPtr)
    public actual val scrollEnd: Point
        get() {
            val result = Point.create()
            lv_obj_get_scroll_end(lvObjPtr, result.lvPointPtr)
            return result
        }
    public actual val scrollXPos: Short
        get() = lv_obj_get_scroll_x(lvObjPtr)
    public actual val scrollYPos: Short
        get() = lv_obj_get_scroll_y(lvObjPtr)
    public actual val scrollBarArea: Pair<Area, Area>
        get() {
            val hor = Area.create()
            val ver = Area.create()
            lv_obj_get_scrollbar_area(obj = lvObjPtr, hor = hor.lvAreaPtr, ver = ver.lvAreaPtr)
            return hor to ver
        }
    public actual var scrollDir: UByte
        get() = lv_obj_get_scroll_dir(lvObjPtr)
        set(value) {
            lv_obj_set_scroll_dir(lvObjPtr, value)
        }
    public actual val scrollSnapXPos: UByte
        get() = lv_obj_get_scroll_snap_x(lvObjPtr)
    public actual val scrollSnapYPos: UByte
        get() = lv_obj_get_scroll_snap_y(lvObjPtr)
    public actual var scrollBarMode: ScrollBarMode
        get() = lv_obj_get_scrollbar_mode(lvObjPtr).toScrollBarMode()
        set(newValue) {
            lv_obj_set_scrollbar_mode(lvObjPtr, newValue.value)
        }

    override fun close() {
        if (lvObjPtr != null && isValid) lv_obj_del(lvObjPtr)
    }

    public actual var height: Short
        get() = lv_obj_get_height(lvObjPtr)
        set(value) = lv_obj_set_height(lvObjPtr, value)
    public actual var width: Short
        get() = lv_obj_get_width(lvObjPtr)
        set(value) = lv_obj_set_width(lvObjPtr, value)

    public actual fun addState(state: UShort) {
        lv_obj_add_state(lvObjPtr, state)
    }

    public actual fun clearState(state: UShort) {
        lv_obj_clear_state(lvObjPtr, state)
    }

    public actual fun getState(): UShort = lv_obj_get_state(lvObjPtr)

    public actual fun hasState(state: UShort): Boolean = lv_obj_has_state(lvObjPtr, state)

    public actual fun addFlag(flags: UInt) {
        lv_obj_add_flag(lvObjPtr, flags)
    }

    public actual fun hasFlag(flags: UInt): Boolean = lv_obj_has_flag(lvObjPtr, flags)

    public actual fun clearFlag(flags: UInt) {
        lv_obj_clear_flag(lvObjPtr, flags)
    }

    public actual fun hasAnyFlag(flags: UInt): Boolean = lv_obj_has_flag_any(lvObjPtr, flags)

    public actual val group: LvglObjectBase?
        get() = LvglObject.fromPointer(lv_obj_get_group(lvObjPtr)?.reinterpret())

    public actual val isValid: Boolean
        get() = lv_obj_is_valid(lvObjPtr)

    public actual fun scalePixels(pixels: Short): Short = lv_obj_dpx(lvObjPtr, pixels)

    public actual fun addEventCallback(filter: LvglEventType, callback: LvglEventCallback) {
        lv_obj_add_event_cb(obj = lvObjPtr, filter = filter.value, event_cb = callback.lvEventCbPtr,
            user_data = callback.stableRef.asCPointer())
    }

    public actual fun addEventCallback(filter: UInt, callback: LvglEventCallback) {
        lv_obj_add_event_cb(obj = lvObjPtr, filter = filter, event_cb = callback.lvEventCbPtr,
            user_data = callback.stableRef.asCPointer())
    }

    public actual fun removeEventCallback(callback: LvglEventCallback): Boolean =
        lv_obj_remove_event_cb(lvObjPtr, callback.lvEventCbPtr)

    public actual fun align(align: UByte, xOffset: Short, yOffset: Short) {
        lv_obj_align(obj = lvObjPtr, align = align, x_ofs = xOffset, y_ofs = yOffset)
    }

    public actual fun alignTo(
        base: LvglObjectBase,
        align: UByte,
        xOffset: Short,
        yOffset: Short
    ) {
        lv_obj_align_to(obj = lvObjPtr, align = align, base = base.lvObjPtr, x_ofs = xOffset, y_ofs = yOffset)
    }

    public actual fun center() {
        lv_obj_center(lvObjPtr)
    }

    public actual fun clean() {
        lv_obj_clean(lvObjPtr)
    }

    public actual fun addStyle(style: Style, selector: UInt) {
        lv_obj_add_style(obj = lvObjPtr, style = style.lvStylePtr, selector = selector)
    }

    public actual fun removeStyle(style: Style, selector: UInt) {
        lv_obj_remove_style(obj = lvObjPtr, style = style.lvStylePtr, selector = selector)
    }

    public actual fun removeAllStyles() {
        lv_obj_remove_style_all(lvObjPtr)
    }

    public actual fun invalidate() {
        lv_obj_invalidate(lvObjPtr)
    }

    public actual fun scrollBy(xPos: Short, yPos: Short, enableAnimation: Boolean) {
        lv_obj_scroll_by(obj = lvObjPtr, x = xPos, y = yPos,
            anim_en = if (enableAnimation) lv_anim_enable_t.LV_ANIM_ON else lv_anim_enable_t.LV_ANIM_OFF)
    }

    public actual fun scrollTo(xPos: Short, yPos: Short, enableAnimation: Boolean) {
        lv_obj_scroll_to(obj = lvObjPtr, x = xPos, y = yPos,
            anim_en = if (enableAnimation) lv_anim_enable_t.LV_ANIM_ON else lv_anim_enable_t.LV_ANIM_OFF)
    }

    public actual fun scrollToView(enableAnimation: Boolean) {
        lv_obj_scroll_to_view(lvObjPtr,
            if (enableAnimation) lv_anim_enable_t.LV_ANIM_ON else lv_anim_enable_t.LV_ANIM_OFF)
    }

    public actual fun scrollToViewRecursive(enableAnimation: Boolean) {
        lv_obj_scroll_to_view_recursive(lvObjPtr,
            if (enableAnimation) lv_anim_enable_t.LV_ANIM_ON else lv_anim_enable_t.LV_ANIM_OFF)
    }

    public actual fun scrollToX(xPos: Short, enableAnimation: Boolean) {
        lv_obj_scroll_to_x(obj = lvObjPtr, x = xPos,
            anim_en = if (enableAnimation) lv_anim_enable_t.LV_ANIM_ON else lv_anim_enable_t.LV_ANIM_OFF)
    }

    public actual fun scrollToY(yPos: Short, enableAnimation: Boolean) {
        lv_obj_scroll_to_y(obj = lvObjPtr, y = yPos,
            anim_en = if (enableAnimation) lv_anim_enable_t.LV_ANIM_ON else lv_anim_enable_t.LV_ANIM_OFF)
    }

    public actual fun setSize(width: Short, height: Short) {
        lv_obj_set_size(obj = lvObjPtr, w = width, h = height)
    }

    public actual fun setLayout(layout: UInt) {
        lv_obj_set_layout(lvObjPtr, layout)
    }

    public actual fun updateLayout() {
        lv_obj_update_layout(lvObjPtr)
    }

    public actual fun setPosition(xPos: Short, yPos: Short) {
        lv_obj_set_pos(obj = lvObjPtr, x = xPos, y = yPos)
    }

    public actual fun setAlignment(alignment: UByte) {
        lv_obj_set_align(lvObjPtr, alignment)
    }

    public actual fun moveForeground() {
        lv_obj_move_foreground(lvObjPtr)
    }

    public actual fun moveBackground() {
        lv_obj_move_background(lvObjPtr)
    }

    public actual fun moveTo(xPos: Short, yPos: Short) {
        lv_obj_move_to(obj = lvObjPtr, x = xPos, y = yPos)
    }

    public actual fun sendEvent(eventCode: UInt, param: StringReference?) {
        lv_event_send(obj = lvObjPtr, event_code = eventCode, param = param?.ref)
    }

    public actual fun sendEvent(
        eventCode: UInt,
        param: String?
    ): StringReference? {
        val result = if (param != null) StringReference.create(param) else null
        lv_event_send(obj = lvObjPtr, event_code = eventCode, param = result?.ref)
        return result
    }

    public actual fun invalidateScrollBar() {
        lv_obj_scrollbar_invalidate(lvObjPtr)
    }

    public actual fun readjustScroll(enableAnimation: Boolean) {
        lv_obj_readjust_scroll(lvObjPtr,
            if (enableAnimation) lv_anim_enable_t.LV_ANIM_ON else lv_anim_enable_t.LV_ANIM_OFF)
    }

    public actual fun addFlag(flag: LvglFlag) {
        lv_obj_add_flag(lvObjPtr, flag.value)
    }

    public actual fun hasFlag(flag: LvglFlag): Boolean = lv_obj_has_flag(lvObjPtr, flag.value)

    public actual fun clearFlag(flag: LvglFlag) {
        lv_obj_clear_flag(lvObjPtr, flag.value)
    }

    public actual fun addState(state: LvglState) {
        lv_obj_add_state(lvObjPtr, state.value.toUShort())
    }

    public actual fun clearState(state: LvglState) {
        lv_obj_clear_state(lvObjPtr, state.value.toUShort())
    }

    public actual fun hasState(state: LvglState): Boolean = lv_obj_has_state(lvObjPtr, state.value.toUShort())
}
