package io.gitlab.embedSoft.lvglKt.core.styling

import io.gitlab.embedSoft.lvglKt.core.Closable
import io.gitlab.embedSoft.lvglKt.core.StringReference
import kotlinx.cinterop.Arena
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.alloc
import kotlinx.cinterop.ptr
import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.*
import io.gitlab.embedSoft.lvglKt.core.layout.FlexAlignment
import io.gitlab.embedSoft.lvglKt.core.layout.toLvFlexAlign

public actual class Style private constructor(ptr: CPointer<lv_style_t>? = null) : Closable {
    private val arena = if (ptr == null) Arena() else null
    public val lvStylePtr: CPointer<lv_style_t>? = ptr ?: createLvStyle()?.ptr
    public actual val isEmpty: Boolean
        get() = lv_style_is_empty(lvStylePtr)

    public actual companion object {
        public actual fun create(): Style = Style()

        public fun fromPointer(ptr: CPointer<lv_style_t>?): Style = Style(ptr)
    }

    private fun createLvStyle(): lv_style_t? {
        val result = arena?.alloc<lv_style_t>()
        lv_style_init(result?.ptr)
        return result
    }

    public actual fun reset() {
        lv_style_reset(lvStylePtr)
    }

    override fun close() {
        arena?.clear()
    }

    public actual infix fun setRadius(value: Short) {
        lv_style_set_radius(lvStylePtr, value)
    }

    public actual infix fun setBorderWidth(value: Short) {
        lv_style_set_border_width(lvStylePtr, value)
    }

    public actual infix fun setArcRounded(value: Boolean) {
        lv_style_set_arc_rounded(lvStylePtr, value)
    }

    public actual infix fun setArcColor(value: Color) {
        lvglkt_style_set_arc_color(lvStylePtr, value.toCValue())
    }

    public actual infix fun setArcWidth(value: Short) {
        lv_style_set_arc_width(lvStylePtr, value)
    }

    public actual infix fun setBorderColor(value: Color) {
        lvglkt_style_set_border_color(lvStylePtr, value.toCValue())
    }

    public actual infix fun setBorderPost(value: Boolean) {
        lv_style_set_border_post(lvStylePtr, value)
    }

    public actual infix fun setAlign(value: UByte) {
        lv_style_set_align(lvStylePtr, value)
    }

    public actual infix fun setWidth(value: Short) {
        lv_style_set_width(lvStylePtr, value)
    }

    public actual infix fun setMinWidth(value: Short) {
        lv_style_set_min_width(lvStylePtr, value)
    }

    public actual infix fun setMaxWidth(value: Short) {
        lv_style_set_max_width(lvStylePtr, value)
    }

    public actual infix fun setHeight(value: Short) {
        lv_style_set_height(lvStylePtr, value)
    }

    public actual infix fun setMinHeight(value: Short) {
        lv_style_set_min_height(lvStylePtr, value)
    }

    public actual infix fun setMaxHeight(value: Short) {
        lv_style_set_max_height(lvStylePtr, value)
    }

    public actual infix fun setXPos(value: Short) {
        lv_style_set_x(lvStylePtr, value)
    }

    public actual infix fun setYPos(value: Short) {
        lv_style_set_y(lvStylePtr, value)
    }

    public actual infix fun setTransformWidth(value: Short) {
        lv_style_set_transform_width(lvStylePtr, value)
    }

    public actual infix fun setTransformHeight(value: Short) {
        lv_style_set_transform_height(lvStylePtr, value)
    }

    public actual infix fun setTranslateX(value: Short) {
        lv_style_set_translate_x(lvStylePtr, value)
    }

    public actual infix fun setTranslateY(value: Short) {
        lv_style_set_translate_y(lvStylePtr, value)
    }

    public actual infix fun setTransformZoom(value: Short) {
        lv_style_set_transform_zoom(lvStylePtr, value)
    }

    public actual infix fun setTransformAngle(value: Short) {
        lv_style_set_transform_angle(lvStylePtr, value)
    }

    public actual infix fun setPadAll(value: Short) {
        lv_style_set_pad_all(lvStylePtr, value)
    }

    public actual infix fun setPadTop(value: Short) {
        lv_style_set_pad_top(lvStylePtr, value)
    }

    public actual infix fun setPadBottom(value: Short) {
        lv_style_set_pad_bottom(lvStylePtr, value)
    }

    public actual infix fun setPadLeft(value: Short) {
        lv_style_set_pad_left(lvStylePtr, value)
    }

    public actual infix fun setPadRight(value: Short) {
        lv_style_set_pad_right(lvStylePtr, value)
    }

    public actual infix fun setPadRow(value: Short) {
        lv_style_set_pad_row(lvStylePtr, value)
    }

    public actual infix fun setPadColumn(value: Short) {
        lv_style_set_pad_column(lvStylePtr, value)
    }

    public actual infix fun setBackgroundColor(value: Color) {
        lvglkt_style_set_bg_color(lvStylePtr, value.toCValue())
    }

    public actual infix fun setBackgroundGradientColor(value: Color) {
        lvglkt_style_set_bg_grad_color(lvStylePtr, value.toCValue())
    }

    public actual infix fun setBackgroundGradientDirection(value: UByte) {
        lv_style_set_bg_grad_dir(lvStylePtr, value)
    }

    public actual infix fun setBackgroundMainStop(value: Short) {
        lv_style_set_bg_main_stop(lvStylePtr, value)
    }

    public actual infix fun setBackgroundGradientStop(value: Short) {
        lv_style_set_bg_grad_stop(lvStylePtr, value)
    }

    public actual infix fun setBackgroundImageRecolor(value: Color) {
        lvglkt_style_set_bg_img_recolor(lvStylePtr, value.toCValue())
    }

    public actual infix fun setBackgroundImageTiled(value: Boolean) {
        lv_style_set_bg_img_tiled(lvStylePtr, value)
    }

    public actual infix fun setBorderSide(value: UByte) {
        lv_style_set_border_side(lvStylePtr, value)
    }

    public actual infix fun setTextColor(value: Color) {
        lvglkt_style_set_text_color(lvStylePtr, value.toCValue())
    }

    public actual infix fun setTextLetterSpace(value: Short) {
        lv_style_set_text_letter_space(lvStylePtr, value)
    }

    public actual infix fun setTextLineSpace(value: Short) {
        lv_style_set_text_line_space(lvStylePtr, value)
    }

    public actual infix fun setTextDecor(value: UByte) {
        lv_style_set_text_decor(lvStylePtr, value)
    }

    public actual infix fun setTextAlign(value: TextAlignment) {
        lv_style_set_text_align(lvStylePtr, value.value)
    }

    public actual infix fun setImageOpacity(value: UByte) {
        lv_style_set_img_opa(lvStylePtr, value)
    }

    public actual infix fun setImageRecolor(value: Color) {
        lvglkt_style_set_img_recolor(lvStylePtr, value.toCValue())
    }

    public actual infix fun setImageRecolorOpa(value: UByte) {
        lv_style_set_img_recolor_opa(lvStylePtr, value)
    }

    public actual infix fun setOutlineWidth(value: Short) {
        lv_style_set_outline_width(lvStylePtr, value)
    }

    public actual infix fun setOutlineColor(value: Color) {
        lvglkt_style_set_outline_color(lvStylePtr, value.toCValue())
    }

    public actual infix fun setOutlineOpacity(value: UByte) {
        lv_style_set_outline_opa(lvStylePtr, value)
    }

    public actual infix fun setOutlinePad(value: Short) {
        lv_style_set_outline_pad(lvStylePtr, value)
    }

    public actual infix fun setShadowWidth(value: Short) {
        lv_style_set_shadow_width(lvStylePtr, value)
    }

    public actual infix fun setShadowXOffset(value: Short) {
        lv_style_set_shadow_ofs_x(lvStylePtr, value)
    }

    public actual infix fun setShadowYOffset(value: Short) {
        lv_style_set_shadow_ofs_y(lvStylePtr, value)
    }

    public actual infix fun setShadowSpread(value: Short) {
        lv_style_set_shadow_spread(lvStylePtr, value)
    }

    public actual infix fun setShadowColor(value: Color) {
        lvglkt_style_set_shadow_color(lvStylePtr, value.toCValue())
    }

    public actual infix fun setShadowOpacity(value: UByte) {
        lv_style_set_shadow_opa(lvStylePtr, value)
    }

    public actual infix fun setLineWidth(value: Short) {
        lv_style_set_line_width(lvStylePtr, value)
    }

    public actual infix fun setLineDashWidth(value: Short) {
        lv_style_set_line_dash_width(lvStylePtr, value)
    }

    public actual infix fun setLineDashGap(value: Short) {
        lv_style_set_line_dash_gap(lvStylePtr, value)
    }

    public actual infix fun setLineRounded(value: Boolean) {
        lv_style_set_line_rounded(lvStylePtr, value)
    }

    public actual infix fun setLineColor(value: Color) {
        lvglkt_style_set_line_color(lvStylePtr, value.toCValue())
    }

    public actual infix fun setLineOpacity(value: UByte) {
        lv_style_set_line_opa(lvStylePtr, value)
    }

    public actual infix fun setArcOpacity(value: UByte) {
        lv_style_set_arc_opa(lvStylePtr, value)
    }

    public actual infix fun setOpacity(value: UByte) {
        lv_style_set_opa(lvStylePtr, value)
    }

    public actual infix fun setColorFilterOpacity(value: UByte) {
        lv_style_set_color_filter_opa(lvStylePtr, value)
    }

    public actual infix fun setBackgroundOpacity(value: UByte) {
        lv_style_set_bg_opa(lvStylePtr, value)
    }

    public actual infix fun setBackgroundImageOpacity(value: UByte) {
        lv_style_set_bg_img_opa(lvStylePtr, value)
    }

    public actual infix fun setBackgroundImageRecolorOpacity(value: UByte) {
        lv_style_set_bg_img_recolor_opa(lvStylePtr, value)
    }

    public actual infix fun setBorderOpacity(value: UByte) {
        lv_style_set_border_opa(lvStylePtr, value)
    }

    public actual infix fun setTextOpacity(value: UByte) {
        lv_style_set_text_opa(lvStylePtr, value)
    }

    public actual fun setLayout(value: UShort) {
        lv_style_set_layout(lvStylePtr, value)
    }

    public actual fun setFlexFlow(value: UInt) {
        lv_style_set_flex_flow(lvStylePtr, value)
    }

    public actual fun setFlexGrow(value: UByte) {
        lv_style_set_flex_grow(lvStylePtr, value)
    }

    public actual fun setFlexCrossPlace(value: FlexAlignment) {
        lv_style_set_flex_cross_place(lvStylePtr, value.toLvFlexAlign())
    }

    public actual fun setFlexMainPlace(value: FlexAlignment) {
        lv_style_set_flex_main_place(lvStylePtr, value.toLvFlexAlign())
    }

    public actual fun setFlexTrackPlace(value: FlexAlignment) {
        lv_style_set_flex_track_place(lvStylePtr, value.toLvFlexAlign())
    }

    public actual fun setBackgroundImageSource(value: String): StringReference {
        val result = StringReference.create(value)
        lv_style_set_bg_img_src(lvStylePtr, result.ref)
        return result
    }

    public actual fun setBackgroundImageSource(value: StringReference) {
        lv_style_set_bg_img_src(lvStylePtr, value.ref)
    }

    public actual fun setArcImageSource(value: StringReference) {
        lv_style_set_arc_img_src(lvStylePtr, value.ref)
    }

    public actual fun setArcImageSource(value: String): StringReference {
        val result = StringReference.create(value)
        lv_style_set_arc_img_src(lvStylePtr, result.ref)
        return result
    }

    public actual fun setTextFont(value: Font) {
        lv_style_set_text_font(lvStylePtr, value.lvFontPtr)
    }
}

public fun CPointer<lv_style_t>?.toStyle(): Style = Style.fromPointer(this)
