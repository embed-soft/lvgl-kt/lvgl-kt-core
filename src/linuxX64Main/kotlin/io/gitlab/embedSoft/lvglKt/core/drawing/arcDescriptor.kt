package io.gitlab.embedSoft.lvglKt.core.drawing

import io.gitlab.embedSoft.lvglKt.core.Closable
import io.gitlab.embedSoft.lvglKt.core.StringReference
import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.lv_draw_arc_dsc_init
import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.lv_draw_arc_dsc_t
import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.lvglkt_draw_arc_dsc_get_color
import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.lvglkt_draw_arc_dsc_set_color
import io.gitlab.embedSoft.lvglKt.core.styling.Color
import io.gitlab.embedSoft.lvglKt.core.styling.toCValue
import io.gitlab.embedSoft.lvglKt.core.styling.toColor
import kotlinx.cinterop.*

public actual class ArcDescriptor private constructor(ptr: CPointer<lv_draw_arc_dsc_t>? = null): Closable {
    private val arena = if (ptr == null) Arena() else null
    public val lvDrawArcDscPtr: CPointer<lv_draw_arc_dsc_t>? = ptr ?: createPointer()
    public actual var blendMode: UByte
        get() = lvDrawArcDscPtr?.pointed?.blend_mode ?: 0u.toUByte()
        set(value) {
            lvDrawArcDscPtr?.pointed?.blend_mode = value
        }
    public actual var color: Color
        get() = lvglkt_draw_arc_dsc_get_color(lvDrawArcDscPtr).toColor()
        set(value) {
            lvglkt_draw_arc_dsc_set_color(lvDrawArcDscPtr, value.toCValue())
        }
    public actual var opacity: UByte
        get() = lvDrawArcDscPtr?.pointed?.opa ?: 0u.toUByte()
        set(value) {
            lvDrawArcDscPtr?.pointed?.opa = value
        }
    public actual var width: Short
        get() = lvDrawArcDscPtr?.pointed?.width ?: 0.toShort()
        set(value) {
            lvDrawArcDscPtr?.pointed?.width = value
        }
    public actual var rounded: UByte
        get() = lvDrawArcDscPtr?.pointed?.rounded ?: 0u.toUByte()
        set(value) {
            lvDrawArcDscPtr?.pointed?.rounded = value
        }

    private fun createPointer(): CPointer<lv_draw_arc_dsc_t>? {
        val result = arena?.alloc<lv_draw_arc_dsc_t>()?.ptr
        lv_draw_arc_dsc_init(result)
        return result
    }

    public actual companion object {
        public actual fun create(): ArcDescriptor = ArcDescriptor()

        public fun fromPointer(ptr: CPointer<lv_draw_arc_dsc_t>?): ArcDescriptor = ArcDescriptor(ptr)
    }

    override fun close() {
        arena?.clear()
    }

    public actual fun setImageSource(imageSrc: String): StringReference {
        val result = StringReference.create(imageSrc)
        lvDrawArcDscPtr?.pointed?.img_src = result.ref
        return result
    }

    public actual fun setImageSource(imageSrc: StringReference) {
        lvDrawArcDscPtr?.pointed?.img_src = imageSrc.ref
    }

    public actual fun getImageSourceAsString(): String =
        lvDrawArcDscPtr?.pointed?.img_src?.reinterpret<ByteVar>()?.toKString() ?: ""
}

public fun CPointer<lv_draw_arc_dsc_t>?.toArcDescriptor(): ArcDescriptor = ArcDescriptor.fromPointer(this)
