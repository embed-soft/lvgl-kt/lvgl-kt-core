package io.gitlab.embedSoft.lvglKt.core.lvglObject

import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.*

public actual val LvglPart.value: UInt
    get() = when(this) {
        LvglPart.ITEMS -> LV_PART_ITEMS
        LvglPart.KNOB -> LV_PART_KNOB
        LvglPart.CUSTOM_FIRST -> LV_PART_CUSTOM_FIRST
        LvglPart.CURSOR -> LV_PART_CURSOR
        LvglPart.MAIN -> LV_PART_MAIN
        LvglPart.INDICATOR -> LV_PART_INDICATOR
        LvglPart.SCROLLBAR -> LV_PART_SCROLLBAR
        LvglPart.TICKS -> LV_PART_TICKS
        LvglPart.SELECTED -> LV_PART_SELECTED
    }

public actual fun UInt.toLvglPart(): LvglPart = when(this) {
    LV_PART_ITEMS -> LvglPart.ITEMS
    LV_PART_KNOB -> LvglPart.KNOB
    LV_PART_CUSTOM_FIRST -> LvglPart.CUSTOM_FIRST
    LV_PART_CURSOR -> LvglPart.CURSOR
    LV_PART_MAIN -> LvglPart.MAIN
    LV_PART_INDICATOR -> LvglPart.INDICATOR
    LV_PART_SCROLLBAR -> LvglPart.SCROLLBAR
    LV_PART_TICKS -> LvglPart.TICKS
    LV_PART_SELECTED -> LvglPart.SELECTED
    else -> throw IllegalStateException("Unrecognised value")
}
