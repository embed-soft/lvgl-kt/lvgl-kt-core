package io.gitlab.embedSoft.lvglKt.core.layout

import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.lv_obj_set_flex_align
import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.lv_obj_set_flex_flow
import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.lv_obj_set_flex_grow
import io.gitlab.embedSoft.lvglKt.core.lvglObject.LvglObjectBase

public actual fun LvglObjectBase.setFlexAlign(
    mainPlace: FlexAlignment,
    crossPlace: FlexAlignment,
    trackCrossPlace: FlexAlignment
) {
    lv_obj_set_flex_align(
        obj = lvObjPtr,
        main_place = mainPlace.toLvFlexAlign(),
        cross_place = crossPlace.toLvFlexAlign(),
        track_cross_place = trackCrossPlace.toLvFlexAlign()
    )
}

public actual fun LvglObjectBase.setFlexFlow(flow: FlexFlow) {
    lv_obj_set_flex_flow(lvObjPtr, flow.value)
}

public actual fun LvglObjectBase.setFlexGrow(grow: UByte) {
    lv_obj_set_flex_grow(lvObjPtr, grow)
}
