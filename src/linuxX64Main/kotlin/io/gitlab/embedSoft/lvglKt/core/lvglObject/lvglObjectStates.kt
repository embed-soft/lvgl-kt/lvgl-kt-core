package io.gitlab.embedSoft.lvglKt.core.lvglObject

import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.*

public actual val LvglState.value: UInt
    get() = when(this) {
        LvglState.USER_1 -> LV_STATE_USER_1
        LvglState.USER_2 -> LV_STATE_USER_2
        LvglState.USER_3 -> LV_STATE_USER_3
        LvglState.USER_4 -> LV_STATE_USER_4
        LvglState.CHECKED -> LV_STATE_CHECKED
        LvglState.DEFAULT -> LV_STATE_DEFAULT
        LvglState.DISABLED -> LV_STATE_DISABLED
        LvglState.EDITED -> LV_STATE_EDITED
        LvglState.FOCUSED -> LV_STATE_FOCUSED
        LvglState.HOVERED -> LV_STATE_HOVERED
        LvglState.PRESSED -> LV_STATE_PRESSED
        LvglState.FOCUS_KEY -> LV_STATE_FOCUS_KEY
        LvglState.SCROLLED -> LV_STATE_SCROLLED
    }

public actual fun UInt.toLvglState(): LvglState = when(this) {
    LV_STATE_USER_1 -> LvglState.USER_1
    LV_STATE_USER_2 -> LvglState.USER_2
    LV_STATE_USER_3 -> LvglState.USER_3
    LV_STATE_USER_4 -> LvglState.USER_4
    LV_STATE_CHECKED -> LvglState.CHECKED
    LV_STATE_DEFAULT -> LvglState.DEFAULT
    LV_STATE_DISABLED -> LvglState.DISABLED
    LV_STATE_EDITED -> LvglState.EDITED
    LV_STATE_FOCUSED -> LvglState.FOCUSED
    LV_STATE_HOVERED -> LvglState.HOVERED
    LV_STATE_PRESSED -> LvglState.PRESSED
    LV_STATE_FOCUS_KEY -> LvglState.FOCUS_KEY
    LV_STATE_SCROLLED -> LvglState.SCROLLED
    else -> throw IllegalStateException("Unrecognised value")
}
