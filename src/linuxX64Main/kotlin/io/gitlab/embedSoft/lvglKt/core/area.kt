package io.gitlab.embedSoft.lvglKt.core

import kotlinx.cinterop.Arena
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.alloc
import kotlinx.cinterop.ptr
import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.*

public actual class Area private constructor(ptr: CPointer<lv_area_t>? = null) : Closable {
    private val arena = if (ptr == null) Arena() else null
    public val lvAreaPtr: CPointer<lv_area_t>? = ptr ?: arena?.alloc<lv_area_t>()?.ptr
    public actual var height: Short
        get() = lv_area_get_height(lvAreaPtr)
        set(value) {
            lv_area_set_height(lvAreaPtr, value)
        }

    public actual var width: Short
        get() = lv_area_get_width(lvAreaPtr)
        set(value) {
            lv_area_set_width(lvAreaPtr, value)
        }
    public actual val size: UInt
        get() = lv_area_get_size(lvAreaPtr)

    public actual companion object {
        public fun fromPointer(ptr: CPointer<lv_area_t>?): Area = Area(ptr)

        public actual fun create(): Area = Area()
    }

    public actual fun move(xOffset: Short, yOffset: Short) {
        lv_area_move(area = lvAreaPtr, x_ofs = xOffset, y_ofs = yOffset)
    }

    public actual fun setPositionAndSize(left: Short, top: Short, right: Short, bottom: Short) {
        lv_area_set(area_p = lvAreaPtr, x1 = left, x2 = right, y1 = top, y2 = bottom)
    }

    override fun close() {
        arena?.clear()
    }
}

public fun CPointer<lv_area_t>?.toArea(): Area = Area.fromPointer(this)
