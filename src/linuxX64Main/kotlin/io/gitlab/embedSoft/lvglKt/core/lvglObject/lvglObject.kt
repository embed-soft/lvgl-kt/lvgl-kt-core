package io.gitlab.embedSoft.lvglKt.core.lvglObject

import kotlinx.cinterop.CPointer
import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.lv_obj_create
import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.lv_obj_t

public actual class LvglObject private constructor(ptr: CPointer<lv_obj_t>?) : LvglObjectBase {
    override val lvObjPtr: CPointer<lv_obj_t>? = ptr

    public actual companion object {
        public fun fromPointer(ptr: CPointer<lv_obj_t>?): LvglObject =
            LvglObject(ptr)

        public actual fun create(parent: LvglObjectBase?): LvglObject =
            LvglObject(lv_obj_create(parent?.lvObjPtr))
    }
}

public fun CPointer<lv_obj_t>?.toLvglObject(): LvglObject = LvglObject.fromPointer(this)
