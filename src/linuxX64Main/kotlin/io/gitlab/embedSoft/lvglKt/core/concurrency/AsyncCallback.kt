package io.gitlab.embedSoft.lvglKt.core.concurrency

import io.gitlab.embedSoft.lvglKt.core.Closable
import kotlinx.cinterop.COpaquePointer
import kotlinx.cinterop.StableRef
import kotlinx.cinterop.asStableRef
import kotlinx.cinterop.staticCFunction
import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.lv_async_cb_t

public actual class AsyncCallback private constructor(
    public val callback: (userData: Any?) -> Unit,
    public val userData: Any? = null
): Closable {
    internal val stableRef = StableRef.create(this)
    public val lvAsyncCbPtr: lv_async_cb_t = staticCFunction { userData: COpaquePointer? ->
        val tmpObj = userData?.asStableRef<AsyncCallback>()?.get()
        tmpObj?.callback?.invoke(tmpObj.userData)
    }

    public actual companion object {
        public actual fun create(
            userData: Any?,
            callback: (userData: Any?) -> Unit
        ): AsyncCallback = AsyncCallback(callback, userData)
    }

    override fun close() {
        stableRef.dispose()
    }
}
