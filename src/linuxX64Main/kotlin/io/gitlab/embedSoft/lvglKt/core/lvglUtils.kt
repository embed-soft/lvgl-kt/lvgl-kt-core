package io.gitlab.embedSoft.lvglKt.core

import io.gitlab.embedSoft.lvglKt.core.lvglObject.LvglObjectBase
import io.gitlab.embedSoft.lvglKt.core.lvglObject.toLvglObject
import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.*
import platform.posix.usleep

public actual fun initLvgl() {
    lv_init()
}

public actual fun deinitLvgl() {
    lv_deinit()
}

public actual fun runEventLoop() {
    // Handle LVGL tasks (tickless mode).
    while (true) {
        lv_tick_inc(5)
        lv_task_handler()
        usleep(5000)
    }
}

public actual val Short.percent: Short
    get() = lv_pct(this)

public actual val topLayer: LvglObjectBase
    get() = lv_layer_top().toLvglObject()

public actual val sysLayer: LvglObjectBase
    get() = lv_layer_sys().toLvglObject()

public actual fun initGrid() {
    lv_grid_init()
}
