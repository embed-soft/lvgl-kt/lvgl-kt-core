package io.gitlab.embedSoft.lvglKt.core

import kotlinx.cinterop.*
import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.lv_point_t

public actual class Point private constructor(ptr: CPointer<lv_point_t>? = null) : Closable {
    private val arena = if (ptr == null) Arena() else null
    public val lvPointPtr: CPointer<lv_point_t>? = ptr ?: arena?.alloc<lv_point_t>()?.ptr
    public actual var xPos: Short
        get() = lvPointPtr?.pointed?.x ?: 0.toShort()
        set(value) {
            lvPointPtr?.pointed?.x = value
        }
    public actual var yPos: Short
        get() = lvPointPtr?.pointed?.y ?: 0.toShort()
        set(value) {
            lvPointPtr?.pointed?.y = value
        }

    override fun close() {
        arena?.clear()
    }

    public actual companion object {
        public actual fun create(): Point = Point()

        public fun fromPointer(ptr: CPointer<lv_point_t>?): Point = Point(ptr)
    }
}

public fun CPointer<lv_point_t>?.toPoint(): Point = Point.fromPointer(this)
