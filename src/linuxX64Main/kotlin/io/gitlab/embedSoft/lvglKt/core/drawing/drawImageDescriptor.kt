package io.gitlab.embedSoft.lvglKt.core.drawing

import io.gitlab.embedSoft.lvglKt.core.Closable
import io.gitlab.embedSoft.lvglKt.core.Point
import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.lv_draw_img_dsc_init
import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.lv_draw_img_dsc_t
import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.lvglkt_draw_img_dsc_get_recolor
import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.lvglkt_draw_img_dsc_set_recolor
import io.gitlab.embedSoft.lvglKt.core.styling.Color
import io.gitlab.embedSoft.lvglKt.core.styling.toCValue
import io.gitlab.embedSoft.lvglKt.core.styling.toColor
import io.gitlab.embedSoft.lvglKt.core.toPoint
import kotlinx.cinterop.*

public actual class DrawImageDescriptor private constructor(ptr: CPointer<lv_draw_img_dsc_t>? = null): Closable {
    private val arena = if (ptr == null) Arena() else null
    public val lvDrawImgDscPtr: CPointer<lv_draw_img_dsc_t>? = createPointer()
    public actual var angle: Short
        get() = lvDrawImgDscPtr?.pointed?.angle ?: 0
        set(value) {
            lvDrawImgDscPtr?.pointed?.angle = value
        }
    public actual var opacity: UByte
        get() = lvDrawImgDscPtr?.pointed?.opa ?: 0.toUByte()
        set(value) {
            lvDrawImgDscPtr?.pointed?.opa = value
        }
    public actual var blendMode: UByte
        get() = lvDrawImgDscPtr?.pointed?.blend_mode ?: 0.toUByte()
        set(value) {
            lvDrawImgDscPtr?.pointed?.blend_mode = value
        }
    public actual var antiAlias: UByte
        get() = lvDrawImgDscPtr?.pointed?.antialias ?: 0.toUByte()
        set(value) {
            lvDrawImgDscPtr?.pointed?.antialias = value
        }
    public actual var frameId: Int
        get() = lvDrawImgDscPtr?.pointed?.frame_id ?: 0
        set(value) {
            lvDrawImgDscPtr?.pointed?.frame_id = value
        }
    public actual var recolor: Color
        get() = lvglkt_draw_img_dsc_get_recolor(lvDrawImgDscPtr).toColor()
        set(value) {
            lvglkt_draw_img_dsc_set_recolor(lvDrawImgDscPtr, value.toCValue())
        }
    public actual var recolorOpacity: UByte
        get() = lvDrawImgDscPtr?.pointed?.recolor_opa ?: 0.toUByte()
        set(value) {
            lvDrawImgDscPtr?.pointed?.recolor_opa = value
        }
    public actual var zoom: UShort
        get() = lvDrawImgDscPtr?.pointed?.zoom ?: 0.toUShort()
        set(value) {
            lvDrawImgDscPtr?.pointed?.zoom = value
        }
    public actual var pivot: Point
        get() = lvDrawImgDscPtr?.pointed?.pivot?.ptr.toPoint()
        set(value) {
            lvDrawImgDscPtr?.pointed?.pivot?.x = value.xPos
            lvDrawImgDscPtr?.pointed?.pivot?.y = value.yPos
        }

    private fun createPointer(): CPointer<lv_draw_img_dsc_t>? {
        val result = arena?.alloc<lv_draw_img_dsc_t>()?.ptr
        lv_draw_img_dsc_init(result)
        return result
    }

    public actual companion object {
        public actual fun create(): DrawImageDescriptor = DrawImageDescriptor()

        public fun fromPointer(ptr: CPointer<lv_draw_img_dsc_t>?): DrawImageDescriptor = DrawImageDescriptor(ptr)
    }

    override fun close() {
        arena?.clear()
    }
}

public fun CPointer<lv_draw_img_dsc_t>?.toDrawImageDescriptor(): DrawImageDescriptor =
    DrawImageDescriptor.fromPointer(this)
