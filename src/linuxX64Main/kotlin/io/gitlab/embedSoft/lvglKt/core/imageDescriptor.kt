package io.gitlab.embedSoft.lvglKt.core

import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.lv_img_dsc_t
import kotlinx.cinterop.*

public actual class ImageDescriptor private constructor(ptr: CPointer<lv_img_dsc_t>? = null): Closable {
    private val arena = if (ptr == null) Arena() else null
    public val lvImgDscPtr: CPointer<lv_img_dsc_t>? = ptr ?: arena?.alloc<lv_img_dsc_t>()?.ptr
    public actual var dataSize: UInt
        get() = lvImgDscPtr?.pointed?.data_size ?: 0u
        set(value) {
            lvImgDscPtr?.pointed?.data_size = value
        }

    override fun close() {
        arena?.clear()
    }

    public actual companion object {
        public actual fun create(): ImageDescriptor = ImageDescriptor()

        public fun fromPointer(ptr: CPointer<lv_img_dsc_t>?): ImageDescriptor = ImageDescriptor(ptr)
    }
}

public fun CPointer<lv_img_dsc_t>?.toImageDescriptor(): ImageDescriptor = ImageDescriptor.fromPointer(this)
