package io.gitlab.embedSoft.lvglKt.core.styling

import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.lvglkt_palette_darken
import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.lvglkt_palette_lighten
import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.lvglkt_palette_main

public actual fun mainPaletteColor(paletteValue: UInt): Color = lvglkt_palette_main(paletteValue).toColor()

public actual fun mainPaletteColor(paletteValue: Palette): Color = lvglkt_palette_main(paletteValue.value).toColor()

public actual fun lighterPaletteColor(paletteValue: UInt, level: UByte): Color =
    lvglkt_palette_lighten(paletteValue, level).toColor()

public actual fun lighterPaletteColor(paletteValue: Palette, level: UByte): Color =
    lvglkt_palette_lighten(paletteValue.value, level).toColor()

public actual fun darkerPaletteColor(paletteValue: UInt, level: UByte): Color =
    lvglkt_palette_darken(paletteValue, level).toColor()

public actual fun darkerPaletteColor(paletteValue: Palette, level: UByte): Color =
    lvglkt_palette_darken(paletteValue.value, level).toColor()
