package io.gitlab.embedSoft.lvglKt.core.drawing

import io.gitlab.embedSoft.lvglKt.core.Closable
import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.*
import io.gitlab.embedSoft.lvglKt.core.styling.Color
import io.gitlab.embedSoft.lvglKt.core.styling.toCValue
import io.gitlab.embedSoft.lvglKt.core.styling.toColor
import kotlinx.cinterop.*

public actual class LabelDescriptor private constructor(ptr: CPointer<lv_draw_label_dsc_t>? = null): Closable {
    private val arena = if (ptr == null) Arena() else null
    public val lvDrawLabelDscPtr: CPointer<lv_draw_label_dsc_t>? = ptr ?: createPointer()
    public actual var color: Color
        get() = lvglkt_draw_label_dsc_get_color(lvDrawLabelDscPtr).toColor()
        set(value) {
            lvglkt_draw_label_dsc_set_color(lvDrawLabelDscPtr, value.toCValue())
        }
    public actual var selectedStart: UInt
        get() = lvDrawLabelDscPtr?.pointed?.sel_start ?: 0u
        set(value) {
            lvDrawLabelDscPtr?.pointed?.sel_start = value
        }
    public actual var selectedEnd: UInt
        get() = lvDrawLabelDscPtr?.pointed?.sel_end ?: 0u
        set(value) {
            lvDrawLabelDscPtr?.pointed?.sel_end = value
        }
    public actual var selectedColor: Color
        get() = lvglkt_draw_label_dsc_get_sel_color(lvDrawLabelDscPtr).toColor()
        set(value) {
            lvglkt_draw_label_dsc_set_sel_color(lvDrawLabelDscPtr, value.toCValue())
        }
    public actual var selectedBackgroundColor: Color
        get() = lvglkt_draw_label_dsc_get_sel_bg_color(lvDrawLabelDscPtr).toColor()
        set(value) {
            lvglkt_draw_label_dsc_set_sel_bg_color(lvDrawLabelDscPtr, value.toCValue())
        }
    public actual var blendMode: UByte
        get() = lvDrawLabelDscPtr?.pointed?.blend_mode ?: 0u.toUByte()
        set(value) {
            lvDrawLabelDscPtr?.pointed?.blend_mode = value
        }
    public actual var decor: UByte
        get() = lvDrawLabelDscPtr?.pointed?.decor ?: 0u.toUByte()
        set(value) {
            lvDrawLabelDscPtr?.pointed?.decor = value
        }
    public actual var align: UByte
        get() = lvDrawLabelDscPtr?.pointed?.align ?: 0u.toUByte()
        set(value) {
            lvDrawLabelDscPtr?.pointed?.align = value
        }
    public actual var bidiDir: UByte
        get() = lvDrawLabelDscPtr?.pointed?.bidi_dir ?: 0u.toUByte()
        set(value) {
            lvDrawLabelDscPtr?.pointed?.bidi_dir = value
        }
    public actual var flag: UByte
        get() = lvDrawLabelDscPtr?.pointed?.flag ?: 0u.toUByte()
        set(value) {
            lvDrawLabelDscPtr?.pointed?.flag = value
        }
    public actual var letterSpace: Short
        get() = lvDrawLabelDscPtr?.pointed?.letter_space ?: 0.toShort()
        set(value) {
            lvDrawLabelDscPtr?.pointed?.letter_space = value
        }
    public actual var lineSpace: Short
        get() = lvDrawLabelDscPtr?.pointed?.line_space ?: 0.toShort()
        set(value) {
            lvDrawLabelDscPtr?.pointed?.line_space = value
        }
    public actual var xOffset: Short
        get() = lvDrawLabelDscPtr?.pointed?.ofs_x ?: 0.toShort()
        set(value) {
            lvDrawLabelDscPtr?.pointed?.ofs_x = value
        }
    public actual var yOffset: Short
        get() = lvDrawLabelDscPtr?.pointed?.ofs_y ?: 0.toShort()
        set(value) {
            lvDrawLabelDscPtr?.pointed?.ofs_y = value
        }
    public actual var opacity: UByte
        get() = lvDrawLabelDscPtr?.pointed?.opa ?: 0u.toUByte()
        set(value) {
            lvDrawLabelDscPtr?.pointed?.opa = value
        }

    private fun createPointer(): CPointer<lv_draw_label_dsc_t>? {
        val result = arena?.alloc<lv_draw_label_dsc_t>()?.ptr
        lv_draw_label_dsc_init(result)
        return result
    }

    public actual companion object {
        public actual fun create(): LabelDescriptor = LabelDescriptor()

        public fun fromPointer(ptr: CPointer<lv_draw_label_dsc_t>?): LabelDescriptor = LabelDescriptor(ptr)
    }

    override fun close() {
        arena?.clear()
    }
}

public fun CPointer<lv_draw_label_dsc_t>?.toLabelDescriptor(): LabelDescriptor = LabelDescriptor.fromPointer(this)
