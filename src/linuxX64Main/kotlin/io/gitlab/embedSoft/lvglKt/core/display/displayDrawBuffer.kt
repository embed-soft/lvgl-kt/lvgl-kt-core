package io.gitlab.embedSoft.lvglKt.core.display

import io.gitlab.embedSoft.lvglKt.core.Closable
import kotlinx.cinterop.*
import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.lv_color_t
import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.lv_disp_draw_buf_init
import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.lv_disp_draw_buf_t

public actual class DisplayDrawBuffer private constructor(
    ptr: CPointer<lv_disp_draw_buf_t>?,
    bufSize: UInt,
    dblBuffer: Boolean
): Closable {
    private val arena = Arena()
    private val primaryBufPtr: CPointer<lv_color_t>? =
        if (ptr == null) arena.allocArray(bufSize.toInt()) else null
    private val secondaryBufPtr: CPointer<lv_color_t>? =
        if (ptr == null && dblBuffer) arena.allocArray(bufSize.toInt()) else null
    public val lvDispDrawBufPtr: CPointer<lv_disp_draw_buf_t> = ptr ?: createDrawBuffer(bufSize).ptr

    private fun createDrawBuffer(bufSize: UInt): lv_disp_draw_buf_t {
        val result = arena.alloc<lv_disp_draw_buf_t>()
        lv_disp_draw_buf_init(draw_buf = result.ptr, buf1 = primaryBufPtr, buf2 = secondaryBufPtr,
            size_in_px_cnt = bufSize)
        return result
    }

    public actual companion object {
        public actual fun create(bufSize: UInt, dblBuffer: Boolean): DisplayDrawBuffer =
            DisplayDrawBuffer(bufSize = bufSize, dblBuffer = dblBuffer, ptr = null)

        public fun fromPointer(ptr: CPointer<lv_disp_draw_buf_t>?): DisplayDrawBuffer =
            DisplayDrawBuffer(ptr = ptr, bufSize = 0u, dblBuffer = false)
    }

    override fun close() {
        if (primaryBufPtr != null) arena.clear()
    }
}

public fun CPointer<lv_disp_draw_buf_t>?.toDisplayDrawBuffer(): DisplayDrawBuffer =
    DisplayDrawBuffer.fromPointer(this)
