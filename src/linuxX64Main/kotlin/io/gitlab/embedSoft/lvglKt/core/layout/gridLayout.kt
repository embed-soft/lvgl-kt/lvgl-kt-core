package io.gitlab.embedSoft.lvglKt.core.layout

import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.*
import io.gitlab.embedSoft.lvglKt.core.lvglObject.LvglObjectBase
import io.gitlab.embedSoft.lvglKt.core.lvglObject.LvglPart
import io.gitlab.embedSoft.lvglKt.core.lvglObject.value
import io.gitlab.embedSoft.lvglKt.core.styling.Style
import kotlinx.cinterop.*

public actual fun LvglObjectBase.setGridAlignment(
    columnAlign: GridAlignment,
    rowAlign: GridAlignment
) {
    lv_obj_set_grid_align(obj = lvObjPtr, column_align = columnAlign.toLvGridAlign(),
        row_align = rowAlign.toLvGridAlign())
}

public actual fun LvglObjectBase.setGridCell(
    columnAlign: GridAlignment,
    colPos: UByte,
    colSpan: UByte,
    rowAlign: GridAlignment,
    rowPos: UByte,
    rowSpan: UByte
) {
    lv_obj_set_grid_cell(
        obj = lvObjPtr,
        column_align = columnAlign.toLvGridAlign(),
        col_pos = colPos,
        col_span = colSpan,
        row_align = rowAlign.toLvGridAlign(),
        row_pos = rowPos,
        row_span = rowSpan
    )
}

public actual fun Style.setGridRowDescriptor(vararg values: Short) {
    lv_style_set_grid_row_dsc_array(lvStylePtr, cValuesOf(*values))
}

public actual fun Style.setGridColumnDescriptor(vararg values: Short) {
    lv_style_set_grid_column_dsc_array(lvStylePtr, cValuesOf(*values))
}

public actual fun Style.setGridRowAlignment(value: GridAlignment) {
    lv_style_set_grid_row_align(lvStylePtr, value.toLvGridAlign())
}

public actual fun Style.setGridColumnAlignment(value: GridAlignment) {
    lv_style_set_grid_column_align(lvStylePtr, value.toLvGridAlign())
}

public actual fun Style.setGridCellColumnPosition(value: Short) {
    lv_style_set_grid_cell_column_pos(lvStylePtr, value)
}

public actual fun Style.setGridCellColumnSpan(value: Short) {
    lv_style_set_grid_cell_column_span(lvStylePtr, value)
}

public actual fun Style.setGridCellRowPosition(value: Short) {
    lv_style_set_grid_cell_row_pos(lvStylePtr, value)
}

public actual fun Style.setGridCellRowSpan(value: Short) {
    lv_style_set_grid_cell_row_span(lvStylePtr, value)
}

public actual fun Style.setGridCellXAlignment(value: Short) {
    lv_style_set_grid_cell_x_align(lvStylePtr, value)
}

public actual fun Style.setGridCellYAlignment(value: Short) {
    lv_style_set_grid_cell_y_align(lvStylePtr, value)
}

public actual fun LvglObjectBase.getStyleGridRowDescriptor(part: LvglPart): Array<Short> {
    var pos = 0
    val tmpList = mutableListOf<Short>()
    val ptr = lv_obj_get_style_grid_row_dsc_array(lvObjPtr, part.value)
    var value: Short?
    do {
        value = ptr?.get(pos)
        if (value != null) tmpList += value
        pos++
    } while (value != null)
    return tmpList.toTypedArray()
}

public actual fun LvglObjectBase.getStyleGridColumnDescriptor(part: LvglPart): Array<Short> {
    var pos = 0
    val tmpList = mutableListOf<Short>()
    val ptr = lv_obj_get_style_grid_column_dsc_array(lvObjPtr, part.value)
    var value: Short?
    do {
        value = ptr?.get(pos)
        if (value != null) tmpList += value
        pos++
    } while (value != null)
    return tmpList.toTypedArray()
}

public actual fun LvglObjectBase.getStyleGridRowAlignment(part: LvglPart): GridAlignment =
    lv_obj_get_style_grid_row_align(lvObjPtr, part.value).toGridAlignment()

public actual fun LvglObjectBase.getStyleGridColumnAlignment(part: LvglPart): GridAlignment =
    lv_obj_get_style_grid_column_align(lvObjPtr, part.value).toGridAlignment()

public actual fun LvglObjectBase.getStyleGridCellColumnPosition(part: LvglPart): Short =
    lv_obj_get_style_grid_cell_column_pos(lvObjPtr, part.value)

public actual fun LvglObjectBase.getStyleGridCellColumnSpan(part: LvglPart): Short =
    lv_obj_get_style_grid_cell_column_span(lvObjPtr, part.value)

public actual fun LvglObjectBase.getStyleGridCellRowPosition(part: LvglPart): Short =
    lv_obj_get_style_grid_cell_row_pos(lvObjPtr, part.value)

public actual fun LvglObjectBase.getStyleGridCellRowSpan(part: LvglPart): Short =
    lv_obj_get_style_grid_cell_row_span(lvObjPtr, part.value)

public actual fun LvglObjectBase.getStyleGridCellXAlignment(part: LvglPart): Short =
    lv_obj_get_style_grid_cell_x_align(lvObjPtr, part.value)

public actual fun LvglObjectBase.getStyleGridCellYAlignment(part: LvglPart): Short =
    lv_obj_get_style_grid_cell_y_align(lvObjPtr, part.value)

public actual fun LvglObjectBase.setStyleGridRowDescriptor(selector: UInt, vararg values: Short) {
    lv_obj_set_style_grid_row_dsc_array(obj = lvObjPtr, selector = selector, value = cValuesOf(*values))
}

public actual fun LvglObjectBase.setStyleGridColumnDescriptor(selector: UInt, vararg values: Short) {
    lv_obj_set_style_grid_column_dsc_array(obj = lvObjPtr, selector = selector, value = cValuesOf(*values))
}

public actual fun LvglObjectBase.setStyleGridRowAlignment(selector: UInt, value: GridAlignment) {
    lv_obj_set_style_grid_row_align(obj = lvObjPtr, value = value.toLvGridAlign(), selector = selector)
}

public actual fun LvglObjectBase.setStyleGridColumnAlignment(selector: UInt, value: GridAlignment) {
    lv_obj_set_style_grid_column_align(obj = lvObjPtr, selector = selector, value = value.toLvGridAlign())
}

public actual fun LvglObjectBase.setStyleGridCellColumnPosition(selector: UInt, value: Short) {
    lv_obj_set_style_grid_cell_column_pos(obj = lvObjPtr, value = value, selector = selector)
}

public actual fun LvglObjectBase.setStyleGridCellColumnSpan(selector: UInt, value: Short) {
    lv_obj_set_style_grid_cell_column_span(obj = lvObjPtr, selector = selector, value = value)
}

public actual fun LvglObjectBase.setStyleGridCellRowPosition(selector: UInt, value: Short) {
    lv_obj_set_style_grid_cell_row_pos(obj = lvObjPtr, value = value, selector = selector)
}

public actual fun LvglObjectBase.setStyleGridCellRowSpan(selector: UInt, value: Short) {
    lv_obj_set_style_grid_cell_row_span(obj = lvObjPtr, selector = selector, value = value)
}

public actual fun LvglObjectBase.setStyleGridCellXAlignment(selector: UInt, value: Short) {
    lv_obj_set_style_grid_cell_x_align(obj = lvObjPtr, selector = selector, value = value)
}

public actual fun LvglObjectBase.setStyleGridCellYAlignment(selector: UInt, value: Short) {
    lv_obj_set_style_grid_cell_y_align(obj = lvObjPtr, selector = selector, value = value)
}
