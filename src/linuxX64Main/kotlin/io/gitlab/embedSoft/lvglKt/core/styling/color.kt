package io.gitlab.embedSoft.lvglKt.core.styling

import io.gitlab.embedSoft.lvglKt.core.Closable
import kotlinx.cinterop.*
import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.*

public actual class Color private constructor(ptr: CPointer<lv_color_t>? = null) : Closable {
    private val arena = if (ptr == null) Arena() else null
    public val lvColorPtr: CPointer<lv_color_t>? = arena?.alloc<lv_color_t>()?.ptr ?: ptr
    public actual var full: UShort
        get() = lvColorPtr?.pointed?.full ?: 0u
        set(value) {
            lvColorPtr?.pointed?.full = value
        }
    public actual var red: UShort
        get() = lvColorPtr?.pointed?.ch?.red ?: 255u
        set(value) {
            lvColorPtr?.pointed?.ch?.red = value
        }
    public actual var green: UShort
        get() = lvColorPtr?.pointed?.ch?.green ?: 255u
        set(value) {
            lvColorPtr?.pointed?.ch?.green = value
        }
    public actual var blue: UShort
        get() = lvColorPtr?.pointed?.ch?.blue ?: 255u
        set(value) {
            lvColorPtr?.pointed?.ch?.blue = value
        }

    public actual companion object {
        public fun fromPointer(ptr: CPointer<lv_color_t>?): Color = Color(ptr)

        public actual fun create(): Color = Color()
    }

    public actual fun lighten(level: UByte): Color = lvglkt_color_lighten(toCValue(), level).toColor()

    public actual fun darken(level: UByte): Color = lvglkt_color_darken(toCValue(), level).toColor()

    public actual fun mix(otherColor: Color, ratio: UByte): Color =
        lvglkt_color_mix(color1 = toCValue(), color2 = otherColor.toCValue(), mix = ratio).toColor()

    public actual fun mix(otherColor: Color, ratio: Opacity): Color =
        lvglkt_color_mix(color1 = toCValue(), color2 = otherColor.toCValue(), mix = ratio.value).toColor()

    override fun close() {
        arena?.clear()
    }
}

public fun CValue<lvglkt_color_t>.toColor(): Color = Color.create().apply {
    var newFull = 255u.toUShort()
    var newRed = 255u.toUShort()
    var newGreen = 255u.toUShort()
    var newBlue = 255u.toUShort()
    this@toColor.useContents {
        newFull = full
        newRed = red
        newGreen = green
        newBlue = blue
    }
    full = newFull
    red = newRed
    green = newGreen
    blue = newBlue
}

public fun lvglkt_color_t.toColor(): Color {
    val tmpFull = full
    val tmpRed = red
    val tmpGreen = green
    val tmpBlue = blue
    return Color.create().apply {
        full = tmpFull
        red = tmpRed
        green = tmpGreen
        blue = tmpBlue
    }
}

public fun Color.toCValue(): CValue<lvglkt_color_t> {
    val tmpFull = full
    val tmpRed = red
    val tmpGreen = green
    val tmpBlue = blue
    return cValue {
        full = tmpFull
        red = tmpRed
        green = tmpGreen
        blue = tmpBlue
    }
}

public fun CPointer<lv_color_t>?.toColor(): Color = Color.fromPointer(this)

public actual fun colorFromRgb(red: UByte, green: UByte, blue: UByte): Color =
    lvglkt_color_make(red = red.toUShort(), green = green.toUShort(), blue = blue.toUShort()).toColor()

public actual fun rgbColorFromHsv(hue: UShort, saturation: UByte, value: UByte): Color =
    lvglkt_color_hsv_to_rgb(hue = hue, saturation = saturation.toUShort(), value = value.toUShort()).toColor()

public actual fun whiteColor(): Color = lvglkt_color_white().toColor()

public actual fun blackColor(): Color = lvglkt_color_black().toColor()
