package io.gitlab.embedSoft.lvglKt.core.display

import io.gitlab.embedSoft.lvglKt.core.Area
import io.gitlab.embedSoft.lvglKt.core.Closable
import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.lv_disp_drv_init
import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.lv_disp_drv_t
import io.gitlab.embedSoft.lvglKt.core.styling.Color
import io.gitlab.embedSoft.lvglKt.core.toArea
import kotlinx.cinterop.*

public actual class DisplayDriver private constructor(
    ptr: CPointer<lv_disp_drv_t>? = null,
    drawBuf: DisplayDrawBuffer? = null
) : Closable {
    private val arena = Arena()
    private val stableRef = if (ptr == null) StableRef.create(this) else null
    public val lvDispDrvPtr: CPointer<lv_disp_drv_t>? = if (drawBuf != null) createDisplayDriver(drawBuf).ptr else ptr
    public actual var horRes: Short
        get() = lvDispDrvPtr?.pointed?.hor_res ?: 0.toShort()
        set(value) {
            lvDispDrvPtr?.pointed?.hor_res = value
        }
    public actual var vertRes: Short
        get() = lvDispDrvPtr?.pointed?.ver_res ?: 0.toShort()
        set(value) {
            lvDispDrvPtr?.pointed?.ver_res = value
        }
    public actual var drawBuf: DisplayDrawBuffer?
        get() {
            val ptr = lvDispDrvPtr?.pointed?.draw_buf
            return if (ptr != null) DisplayDrawBuffer.fromPointer(ptr) else null
        }
        set(value) {
            lvDispDrvPtr?.pointed?.draw_buf = value?.lvDispDrawBufPtr
        }
    public actual var antiAliasing: Boolean
        get() = lvDispDrvPtr?.pointed?.antialiasing == 0u
        set(value) {
            lvDispDrvPtr?.pointed?.antialiasing = if (value) 1u else 0u
        }
    public actual var onFlush: (driver: DisplayDriver, area: Area, buf: Color) -> Unit = { _, _, _ -> }

    private fun createDisplayDriver(drawBuf: DisplayDrawBuffer): lv_disp_drv_t {
        val result = arena.alloc<lv_disp_drv_t>()
        lv_disp_drv_init(result.ptr)
        result.draw_buf = drawBuf.lvDispDrawBufPtr
        return result
    }

    private fun setupCallbacks() {
        lvDispDrvPtr?.pointed?.user_data = stableRef?.asCPointer()
        lvDispDrvPtr?.pointed?.flush_cb = staticCFunction { driver, area, buf ->
            val userData = driver?.pointed?.user_data?.asStableRef<DisplayDriver>()?.get()
            val color = Color.fromPointer(buf)
            userData?.onFlush?.invoke(driver.toDisplayDriver(), area.toArea(), color)
        }
    }

    override fun close() {
        arena.clear()
        stableRef?.dispose()
    }

    public actual companion object {
        public actual fun create(drawBuf: DisplayDrawBuffer): DisplayDriver {
            val result = DisplayDriver(drawBuf = drawBuf)
            result.setupCallbacks()
            return result
        }

        public fun fromPointer(ptr: CPointer<lv_disp_drv_t>?): DisplayDriver = DisplayDriver(ptr = ptr)
    }
}

public fun CPointer<lv_disp_drv_t>?.toDisplayDriver(): DisplayDriver = DisplayDriver.fromPointer(ptr = this)
