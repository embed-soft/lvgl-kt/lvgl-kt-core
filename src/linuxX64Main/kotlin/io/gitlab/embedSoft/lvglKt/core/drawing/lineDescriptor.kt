package io.gitlab.embedSoft.lvglKt.core.drawing

import io.gitlab.embedSoft.lvglKt.core.Closable
import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.lv_draw_line_dsc_init
import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.lv_draw_line_dsc_t
import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.lvglkt_draw_line_dsc_get_color
import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.lvglkt_draw_line_dsc_set_color
import io.gitlab.embedSoft.lvglKt.core.styling.Color
import io.gitlab.embedSoft.lvglKt.core.styling.toCValue
import io.gitlab.embedSoft.lvglKt.core.styling.toColor
import kotlinx.cinterop.*

public actual class LineDescriptor private constructor(ptr: CPointer<lv_draw_line_dsc_t>? = null) : Closable {
    private val arena = if (ptr == null) Arena() else null
    public val lvDrawLineDscPtr: CPointer<lv_draw_line_dsc_t>? = ptr ?: createPointer()
    public actual var blendMode: UByte
        get() = lvDrawLineDscPtr?.pointed?.blend_mode ?: 0u.toUByte()
        set(value) {
            lvDrawLineDscPtr?.pointed?.blend_mode = value
        }
    public actual var color: Color
        get() = lvglkt_draw_line_dsc_get_color(lvDrawLineDscPtr).toColor()
        set(value) {
            lvglkt_draw_line_dsc_set_color(lvDrawLineDscPtr, value.toCValue())
        }
    public actual var opacity: UByte
        get() = lvDrawLineDscPtr?.pointed?.opa ?: 0u.toUByte()
        set(value) {
            lvDrawLineDscPtr?.pointed?.opa = value
        }
    public actual var dashGap: Short
        get() = lvDrawLineDscPtr?.pointed?.dash_gap ?: 0.toShort()
        set(value) {
            lvDrawLineDscPtr?.pointed?.dash_gap = value
        }
    public actual var dashWidth: Short
        get() = lvDrawLineDscPtr?.pointed?.dash_width ?: 0.toShort()
        set(value) {
            lvDrawLineDscPtr?.pointed?.dash_width = value
        }
    public actual var rawEnd: UByte
        get() = lvDrawLineDscPtr?.pointed?.raw_end ?: 0u.toUByte()
        set(value) {
            lvDrawLineDscPtr?.pointed?.raw_end = value
        }
    public actual var roundEnd: UByte
        get() = lvDrawLineDscPtr?.pointed?.round_end ?: 0u.toUByte()
        set(value) {
            lvDrawLineDscPtr?.pointed?.round_end = value
        }
    public actual var roundStart: UByte
        get() = lvDrawLineDscPtr?.pointed?.round_start ?: 0u.toUByte()
        set(value) {
            lvDrawLineDscPtr?.pointed?.round_start = value
        }
    public actual var width: Short
        get() = lvDrawLineDscPtr?.pointed?.width ?: 0.toShort()
        set(value) {
            lvDrawLineDscPtr?.pointed?.width = value
        }

    private fun createPointer(): CPointer<lv_draw_line_dsc_t>? {
        val result = arena?.alloc<lv_draw_line_dsc_t>()?.ptr
        lv_draw_line_dsc_init(result)
        return result
    }

    public actual companion object {
        public actual fun create(): LineDescriptor = LineDescriptor()

        public fun fromPointer(ptr: CPointer<lv_draw_line_dsc_t>?): LineDescriptor = LineDescriptor(ptr)
    }

    override fun close() {
        arena?.clear()
    }
}

public fun CPointer<lv_draw_line_dsc_t>?.toLineDescriptor(): LineDescriptor = LineDescriptor.fromPointer(this)
