package io.gitlab.embedSoft.lvglKt.core.concurrency

import io.gitlab.embedSoft.lvglKt.core.Closable
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.pointed
import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.*

public actual class Timer private constructor(
    ptr: CPointer<lv_timer_t>? = null,
    period: UInt = 0u,
    callback: TimerCallback? = null
) : Closable {
    public val lvTimerPtr: CPointer<lv_timer_t>? =
        ptr ?: lv_timer_create(period = period, user_data = callback?.stableRef?.asCPointer(),
            timer_xcb = callback?.lvTimerCbPtr)
    public actual val next: Timer
        get() = lv_timer_get_next(lvTimerPtr).toTimer()
    public actual var period: UInt
        get() = lvTimerPtr?.pointed?.period ?: 0u
        set(value) {
            lv_timer_set_period(lvTimerPtr, value)
        }
    public actual var repeatCount: Int
        get() = lvTimerPtr?.pointed?.repeat_count ?: -1
        set(value) {
            lv_timer_set_repeat_count(lvTimerPtr, value)
        }

    public actual companion object {
        public actual val idle: UByte
            get() = lv_timer_get_idle()

        public actual fun create(callback: TimerCallback, period: UInt): Timer =
            Timer(period = period, callback = callback)

        public fun fromPointer(ptr: CPointer<lv_timer_t>?): Timer = Timer(ptr = ptr)

        public actual fun handler(): UInt = lv_timer_handler()
    }

    override fun close() {
        lv_timer_del(lvTimerPtr)
    }

    public actual fun setEnable(enable: Boolean) {
        lv_timer_enable(enable)
    }

    public actual fun pause() {
        lv_timer_pause(lvTimerPtr)
    }

    public actual fun ready() {
        lv_timer_ready(lvTimerPtr)
    }

    public actual fun reset() {
        lv_timer_reset(lvTimerPtr)
    }

    public actual fun resume() {
        lv_timer_resume(lvTimerPtr)
    }
}

public fun CPointer<lv_timer_t>?.toTimer(): Timer = Timer.fromPointer(this)
