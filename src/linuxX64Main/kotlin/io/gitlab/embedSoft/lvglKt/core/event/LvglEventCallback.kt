package io.gitlab.embedSoft.lvglKt.core.event

import io.gitlab.embedSoft.lvglKt.core.Closable
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.StableRef
import kotlinx.cinterop.asStableRef
import kotlinx.cinterop.staticCFunction
import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.lv_event_cb_t
import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.lv_event_get_user_data
import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.lv_event_t

public actual class LvglEventCallback private constructor(
    public val callback: (evt: LvglEvent, userData: Any?) -> Unit,
    public val userData: Any? = null
): Closable {
    internal val stableRef = StableRef.create(this)
    public val lvEventCbPtr: lv_event_cb_t = staticCFunction { evt: CPointer<lv_event_t>? ->
        val tmpEvt = evt.toLvglEvent()
        val tmpObj = lv_event_get_user_data(tmpEvt.lvEventPtr)?.asStableRef<LvglEventCallback>()?.get()
        tmpObj?.callback?.invoke(tmpEvt, tmpObj.userData)
    }

    public actual companion object {
        public actual fun create(
            userData: Any?,
            callback: (evt: LvglEvent, userData: Any?) -> Unit
        ): LvglEventCallback = LvglEventCallback(callback, userData)
    }

    override fun close() {
        stableRef.dispose()
    }
}
