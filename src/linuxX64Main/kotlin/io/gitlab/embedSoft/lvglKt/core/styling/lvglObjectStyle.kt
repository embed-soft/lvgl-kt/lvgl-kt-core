package io.gitlab.embedSoft.lvglKt.core.styling

import io.gitlab.embedSoft.lvglKt.core.StringReference
import io.gitlab.embedSoft.lvglKt.core.lvglObject.LvglObjectBase
import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.*
import io.gitlab.embedSoft.lvglKt.core.layout.FlexAlignment
import io.gitlab.embedSoft.lvglKt.core.layout.toFlexAlignment
import io.gitlab.embedSoft.lvglKt.core.layout.toLvFlexAlign

public actual fun LvglObjectBase.getRadiusStyle(part: UInt): Short = lv_obj_get_style_radius(lvObjPtr, part)

public actual fun LvglObjectBase.setRadiusStyle(value: Short, selector: UInt) {
    lv_obj_set_style_radius(obj = lvObjPtr, value = value, selector = selector)
}

public actual fun LvglObjectBase.getBorderWidthStyle(part: UInt): Short = lv_obj_get_style_border_width(lvObjPtr, part)

public actual fun LvglObjectBase.setBorderWidthStyle(value: Short, selector: UInt) {
    lv_obj_set_style_border_width(obj = lvObjPtr, value = value, selector = selector)
}

public actual fun LvglObjectBase.setArcRoundedStyle(value: Boolean, selector: UInt) {
    lv_obj_set_style_arc_rounded(obj = lvObjPtr, value = value, selector = selector)
}

public actual fun LvglObjectBase.getArcRoundedStyle(part: UInt): Boolean = lv_obj_get_style_arc_rounded(lvObjPtr, part)

public actual fun LvglObjectBase.setArcColorStyle(value: Color, selector: UInt) {
    lvglkt_obj_set_style_arc_color(obj = lvObjPtr, value = value.toCValue(), selector = selector)
}

public actual fun LvglObjectBase.getArcColorStyle(part: UInt): Color =
    lvglkt_obj_get_style_arc_color(lvObjPtr, part).toColor()

public actual fun LvglObjectBase.setArcWidthStyle(value: Short, selector: UInt) {
    lv_obj_set_style_arc_width(obj = lvObjPtr, selector = selector, value = value)
}

public actual fun LvglObjectBase.getArcWidthStyle(part: UInt): Short = lv_obj_get_style_arc_width(lvObjPtr, part)

public actual fun LvglObjectBase.setArcOpacityStyle(value: UByte, selector: UInt) {
    lv_obj_set_style_arc_opa(obj = lvObjPtr, selector = selector, value = value)
}

public actual fun LvglObjectBase.getArcOpacityStyle(part: UInt): UByte = lv_obj_get_style_arc_opa(lvObjPtr, part)

public actual fun LvglObjectBase.getArcColorFilteredStyle(part: UInt): Color =
    lvglkt_obj_get_style_arc_color_filtered(lvObjPtr, part).toColor()

public actual fun LvglObjectBase.setBorderColorStyle(value: Color, selector: UInt) {
    lvglkt_obj_set_style_border_color(obj = lvObjPtr, value = value.toCValue(), selector = selector)
}

public actual fun LvglObjectBase.getBorderColorStyle(part: UInt): Color =
    lvglkt_obj_get_style_border_color(lvObjPtr, part).toColor()

public actual fun LvglObjectBase.setBorderPostStyle(value: Boolean, selector: UInt) {
    lv_obj_set_style_border_post(obj = lvObjPtr, selector = selector, value = value)
}

public actual fun LvglObjectBase.getBorderPostStyle(part: UInt): Boolean = lv_obj_get_style_border_post(lvObjPtr, part)

public actual fun LvglObjectBase.getBorderColorFilteredStyle(part: UInt): Color =
    lvglkt_obj_get_style_border_color_filtered(lvObjPtr, part).toColor()

public actual fun LvglObjectBase.setAlignStyle(value: UByte, selector: UInt) {
    lv_obj_set_style_align(obj = lvObjPtr, value = value, selector = selector)
}

public actual fun LvglObjectBase.getAlignStyle(part: UInt): UByte = lv_obj_get_style_align(lvObjPtr, part)

public actual fun LvglObjectBase.setWidthStyle(value: Short, selector: UInt) {
    lv_obj_set_style_width(obj = lvObjPtr, selector = selector, value = value)
}

public actual fun LvglObjectBase.getWidthStyle(part: UInt): Short = lv_obj_get_style_width(lvObjPtr, part)

public actual fun LvglObjectBase.setMinWidthStyle(value: Short, selector: UInt) {
    lv_obj_set_style_min_width(obj = lvObjPtr, value = value, selector = selector)
}

public actual fun LvglObjectBase.getMinWidthStyle(part: UInt): Short = lv_obj_get_style_min_width(lvObjPtr, part)

public actual fun LvglObjectBase.setMaxWidthStyle(value: Short, selector: UInt) {
    lv_obj_set_style_max_width(obj = lvObjPtr, selector = selector, value = value)
}

public actual fun LvglObjectBase.getMaxWidthStyle(part: UInt): Short = lv_obj_get_style_max_width(lvObjPtr, part)

public actual fun LvglObjectBase.setHeightStyle(value: Short, selector: UInt) {
    lv_obj_set_style_height(obj = lvObjPtr, value = value, selector = selector)
}

public actual fun LvglObjectBase.getHeightStyle(part: UInt): Short = lv_obj_get_style_height(lvObjPtr, part)

public actual fun LvglObjectBase.setMinHeightStyle(value: Short, selector: UInt) {
    lv_obj_set_style_min_height(obj = lvObjPtr, value = value, selector = selector)
}

public actual fun LvglObjectBase.getMinHeightStyle(part: UInt): Short = lv_obj_get_style_min_height(lvObjPtr, part)

public actual fun LvglObjectBase.setMaxHeightStyle(value: Short, selector: UInt) {
    lv_obj_set_style_max_height(obj = lvObjPtr, value = value, selector = selector)
}

public actual fun LvglObjectBase.getMaxHeightStyle(part: UInt): Short = lv_obj_get_style_max_height(lvObjPtr, part)

public actual fun LvglObjectBase.setXPosStyle(value: Short, selector: UInt) {
    lv_obj_set_style_x(obj = lvObjPtr, selector = selector, value = value)
}

public actual fun LvglObjectBase.getXPosStyle(part: UInt): Short = lv_obj_get_style_x(lvObjPtr, part)

public actual fun LvglObjectBase.setYPosStyle(value: Short, selector: UInt) {
    lv_obj_set_style_y(obj = lvObjPtr, value = value, selector = selector)
}

public actual fun LvglObjectBase.getYPosStyle(part: UInt): Short = lv_obj_get_style_y(lvObjPtr, part)

public actual fun LvglObjectBase.setTransformWidthStyle(value: Short, selector: UInt) {
    lv_obj_set_style_transform_width(obj = lvObjPtr, value = value, selector = selector)
}

public actual fun LvglObjectBase.getTransformWidthStyle(part: UInt): Short =
    lv_obj_get_style_transform_width(lvObjPtr, part)

public actual fun LvglObjectBase.setTransformHeightStyle(value: Short, selector: UInt) {
    lv_obj_set_style_transform_height(obj = lvObjPtr, selector = selector, value = value)
}

public actual fun LvglObjectBase.getTransformHeightStyle(part: UInt): Short =
    lv_obj_get_style_transform_height(lvObjPtr, part)

public actual fun LvglObjectBase.setTranslateXStyle(value: Short, selector: UInt) {
    lv_obj_set_style_translate_x(obj = lvObjPtr, selector = selector, value = value)
}

public actual fun LvglObjectBase.getTranslateXStyle(part: UInt): Short = lv_obj_get_style_translate_x(lvObjPtr, part)

public actual fun LvglObjectBase.setTranslateYStyle(value: Short, selector: UInt) {
    lv_obj_set_style_translate_y(obj = lvObjPtr, value = value, selector = selector)
}

public actual fun LvglObjectBase.getTranslateYStyle(part: UInt): Short = lv_obj_get_style_translate_y(lvObjPtr, part)

public actual fun LvglObjectBase.setTransformZoomStyle(value: Short, selector: UInt) {
    lv_obj_set_style_transform_zoom(obj = lvObjPtr, value = value, selector = selector)
}

public actual fun LvglObjectBase.getTransformZoomStyle(part: UInt): Short =
    lv_obj_get_style_transform_zoom(lvObjPtr, part)

public actual fun LvglObjectBase.setTransformAngleStyle(value: Short, selector: UInt) {
    lv_obj_set_style_transform_angle(obj = lvObjPtr, selector = selector, value = value)
}

public actual fun LvglObjectBase.getTransformAngleStyle(part: UInt): Short =
    lv_obj_get_style_transform_angle(lvObjPtr, part)

public actual fun LvglObjectBase.setPadAllStyle(value: Short, selector: UInt) {
    lv_obj_set_style_pad_all(obj = lvObjPtr, selector = selector, value = value)
}

public actual fun LvglObjectBase.setPadTopStyle(value: Short, selector: UInt) {
    lv_obj_set_style_pad_top(obj = lvObjPtr, value = value, selector = selector)
}

public actual fun LvglObjectBase.getPadTopStyle(part: UInt): Short = lv_obj_get_style_pad_top(lvObjPtr, part)

public actual fun LvglObjectBase.setPadBottomStyle(value: Short, selector: UInt) {
    lv_obj_set_style_pad_bottom(obj = lvObjPtr, selector = selector, value = value)
}

public actual fun LvglObjectBase.getPadBottomStyle(part: UInt): Short = lv_obj_get_style_pad_bottom(lvObjPtr, part)

public actual fun LvglObjectBase.setPadLeftStyle(value: Short, selector: UInt) {
    lv_obj_set_style_pad_left(obj = lvObjPtr, selector = selector, value = value)
}

public actual fun LvglObjectBase.getPadLeftStyle(part: UInt): Short = lv_obj_get_style_pad_left(lvObjPtr, part)

public actual fun LvglObjectBase.setPadRightStyle(value: Short, selector: UInt) {
    lv_obj_set_style_pad_right(obj = lvObjPtr, selector = selector, value = value)
}

public actual fun LvglObjectBase.getPadRightStyle(part: UInt): Short = lv_obj_get_style_pad_right(lvObjPtr, part)

public actual fun LvglObjectBase.setPadRowStyle(value: Short, selector: UInt) {
    lv_obj_set_style_pad_row(obj = lvObjPtr, value = value, selector = selector)
}

public actual fun LvglObjectBase.getPadRowStyle(part: UInt): Short = lv_obj_get_style_pad_row(lvObjPtr, part)

public actual fun LvglObjectBase.setPadColumnStyle(value: Short, selector: UInt) {
    lv_obj_set_style_pad_column(obj = lvObjPtr, selector = selector, value = value)
}

public actual fun LvglObjectBase.getPadColumnStyle(part: UInt): Short = lv_obj_get_style_pad_column(lvObjPtr, part)

public actual fun LvglObjectBase.setBackgroundColorStyle(value: Color, selector: UInt) {
    lvglkt_obj_set_style_bg_color(obj = lvObjPtr, value = value.toCValue(), selector = selector)
}

public actual fun LvglObjectBase.getBackgroundColorStyle(part: UInt): Color =
    lvglkt_obj_get_style_bg_color(lvObjPtr, part).toColor()

public actual fun LvglObjectBase.setBackgroundGradientColorStyle(value: Color, selector: UInt) {
    lvglkt_obj_set_style_bg_grad_color(obj = lvObjPtr, value = value.toCValue(), selector = selector)
}

public actual fun LvglObjectBase.getBackgroundGradientColorStyle(part: UInt): Color =
    lvglkt_obj_get_style_bg_grad_color(lvObjPtr, part).toColor()

public actual fun LvglObjectBase.setBackgroundGradientDirectionStyle(value: UByte, selector: UInt) {
    lv_obj_set_style_bg_grad_dir(obj = lvObjPtr, value = value, selector = selector)
}

public actual fun LvglObjectBase.getBackgroundGradientDirectionStyle(part: UInt): UByte =
    lv_obj_get_style_bg_grad_dir(lvObjPtr, part)

public actual fun LvglObjectBase.setBackgroundMainStopStyle(value: Short, selector: UInt) {
    lv_obj_set_style_bg_main_stop(obj = lvObjPtr, selector = selector, value = value)
}

public actual fun LvglObjectBase.getBackgroundMainStopStyle(part: UInt): Short =
    lv_obj_get_style_bg_main_stop(lvObjPtr, part)

public actual fun LvglObjectBase.setBackgroundGradientStopStyle(value: Short, selector: UInt) {
    lv_obj_set_style_bg_grad_stop(obj = lvObjPtr, value = value, selector = selector)
}

public actual fun LvglObjectBase.getBackgroundGradientStopStyle(part: UInt): Short =
    lv_obj_get_style_bg_grad_stop(lvObjPtr, part)

public actual fun LvglObjectBase.setBackgroundImageRecolorStyle(value: Color, selector: UInt) {
    lvglkt_obj_set_style_bg_img_recolor(obj = lvObjPtr, selector = selector, value = value.toCValue())
}

public actual fun LvglObjectBase.getBackgroundImageRecolorStyle(part: UInt): Color =
    lvglkt_obj_get_style_bg_img_recolor(lvObjPtr, part).toColor()

public actual fun LvglObjectBase.setBackgroundImageTiledStyle(value: Boolean, selector: UInt) {
    lv_obj_set_style_bg_img_tiled(obj = lvObjPtr, value = value, selector = selector)
}

public actual fun LvglObjectBase.getBackgroundImageTiledStyle(part: UInt): Boolean =
    lv_obj_get_style_bg_img_tiled(lvObjPtr, part)

public actual fun LvglObjectBase.setBorderSideStyle(value: UByte, selector: UInt) {
    lv_obj_set_style_border_side(obj = lvObjPtr, selector = selector, value = value)
}

public actual fun LvglObjectBase.getBorderSideStyle(part: UInt): UByte = lv_obj_get_style_border_side(lvObjPtr, part)

public actual fun LvglObjectBase.setTextColorStyle(value: Color, selector: UInt) {
    lvglkt_obj_set_style_text_color(obj = lvObjPtr, value = value.toCValue(), selector = selector)
}

public actual fun LvglObjectBase.getTextColorStyle(part: UInt): Color =
    lvglkt_obj_get_style_text_color(lvObjPtr, part).toColor()

public actual fun LvglObjectBase.setTextLetterSpaceStyle(value: Short, selector: UInt) {
    lv_obj_set_style_text_letter_space(obj = lvObjPtr, selector = selector, value = value)
}

public actual fun LvglObjectBase.getTextLetterSpaceStyle(part: UInt): Short =
    lv_obj_get_style_text_letter_space(lvObjPtr, part)

public actual fun LvglObjectBase.setTextLineSpaceStyle(value: Short, selector: UInt) {
    lv_obj_set_style_text_line_space(obj = lvObjPtr, value = value, selector = selector)
}

public actual fun LvglObjectBase.getTextLineSpaceStyle(part: UInt): Short =
    lv_obj_get_style_text_line_space(lvObjPtr, part)

public actual fun LvglObjectBase.setTextDecorStyle(value: UByte, selector: UInt) {
    lv_obj_set_style_text_decor(obj = lvObjPtr, selector = selector, value = value)
}

public actual fun LvglObjectBase.getTextDecorStyle(part: UInt): UByte = lv_obj_get_style_text_decor(lvObjPtr, part)

public actual fun LvglObjectBase.setTextAlignStyle(value: TextAlignment, selector: UInt) {
    lv_obj_set_style_text_align(obj = lvObjPtr, value = value.value, selector = selector)
}

public actual fun LvglObjectBase.getTextAlignStyle(part: UInt): TextAlignment =
    lv_obj_get_style_text_align(lvObjPtr, part).toTextAlignment()

public actual fun LvglObjectBase.setImageOpacityStyle(value: UByte, selector: UInt) {
    lv_obj_set_style_img_opa(obj = lvObjPtr, selector = selector, value = value)
}

public actual fun LvglObjectBase.getImageOpacityStyle(part: UInt): UByte = lv_obj_get_style_img_opa(lvObjPtr, part)

public actual fun LvglObjectBase.setImageRecolorStyle(value: Color, selector: UInt) {
    lvglkt_obj_set_style_img_recolor(obj = lvObjPtr, selector = selector, value = value.toCValue())
}

public actual fun LvglObjectBase.getImageRecolorStyle(part: UInt): Color =
    lvglkt_obj_get_style_img_recolor(lvObjPtr, part).toColor()

public actual fun LvglObjectBase.setImageRecolorOpaStyle(value: UByte, selector: UInt) {
    lv_obj_set_style_img_recolor_opa(obj = lvObjPtr, value = value, selector = selector)
}

public actual fun LvglObjectBase.getImageRecolorOpaStyle(part: UInt): UByte =
    lv_obj_get_style_img_recolor_opa(lvObjPtr, part)

public actual fun LvglObjectBase.setOutlineWidthStyle(value: Short, selector: UInt) {
    lv_obj_set_style_outline_width(obj = lvObjPtr, selector = selector, value = value)
}

public actual fun LvglObjectBase.getOutlineWidthStyle(part: UInt): Short =
    lv_obj_get_style_outline_width(lvObjPtr, part)

public actual fun LvglObjectBase.setOutlineColorStyle(value: Color, selector: UInt) {
    lvglkt_obj_set_style_outline_color(obj = lvObjPtr, value = value.toCValue(), selector = selector)
}

public actual fun LvglObjectBase.getOutlineColorStyle(part: UInt): Color =
    lvglkt_obj_get_style_outline_color(lvObjPtr, part).toColor()

public actual fun LvglObjectBase.setOutlineOpacityStyle(value: UByte, selector: UInt) {
    lv_obj_set_style_outline_opa(obj = lvObjPtr, selector = selector, value = value)
}

public actual fun LvglObjectBase.getOutlineOpacityStyle(part: UInt): UByte =
    lv_obj_get_style_outline_opa(lvObjPtr, part)

public actual fun LvglObjectBase.setOutlinePadStyle(value: Short, selector: UInt) {
    lv_obj_set_style_outline_pad(obj = lvObjPtr, selector = selector, value = value)
}

public actual fun LvglObjectBase.getOutlinePadStyle(part: UInt): Short = lv_obj_get_style_outline_pad(lvObjPtr, part)

public actual fun LvglObjectBase.setShadowWidthStyle(value: Short, selector: UInt) {
    lv_obj_set_style_shadow_width(obj = lvObjPtr, value = value, selector = selector)
}

public actual fun LvglObjectBase.getShadowWidthStyle(part: UInt): Short = lv_obj_get_style_shadow_width(lvObjPtr, part)

public actual fun LvglObjectBase.setShadowXOffsetStyle(value: Short, selector: UInt) {
    lv_obj_set_style_shadow_ofs_x(obj = lvObjPtr, value = value, selector = selector)
}

public actual fun LvglObjectBase.getShadowXOffsetStyle(part: UInt): Short =
    lv_obj_get_style_shadow_ofs_x(lvObjPtr, part)

public actual fun LvglObjectBase.setShadowYOffsetStyle(value: Short, selector: UInt) {
    lv_obj_set_style_shadow_ofs_y(obj = lvObjPtr, value = value, selector = selector)
}

public actual fun LvglObjectBase.getShadowYOffsetStyle(part: UInt): Short =
    lv_obj_get_style_shadow_ofs_y(lvObjPtr, part)

public actual fun LvglObjectBase.setShadowSpreadStyle(value: Short, selector: UInt) {
    lv_obj_set_style_shadow_spread(obj = lvObjPtr, selector = selector, value = value)
}

public actual fun LvglObjectBase.getShadowSpreadStyle(part: UInt): Short =
    lv_obj_get_style_shadow_spread(lvObjPtr, part)

public actual fun LvglObjectBase.setShadowColorStyle(value: Color, selector: UInt) {
    lvglkt_obj_set_style_shadow_color(obj = lvObjPtr, selector = selector, value = value.toCValue())
}

public actual fun LvglObjectBase.getShadowColorStyle(part: UInt): Color =
    lvglkt_obj_get_style_shadow_color(lvObjPtr, part).toColor()

public actual fun LvglObjectBase.setShadowOpacityStyle(value: UByte, selector: UInt) {
    lv_obj_set_style_shadow_opa(obj = lvObjPtr, value = value, selector = selector)
}

public actual fun LvglObjectBase.getShadowOpacityStyle(part: UInt): UByte =
    lv_obj_get_style_shadow_opa(lvObjPtr, part)

public actual fun LvglObjectBase.setLineWidthStyle(value: Short, selector: UInt) {
    lv_obj_set_style_line_width(obj = lvObjPtr, selector = selector, value = value)
}

public actual fun LvglObjectBase.getLineWidthStyle(part: UInt): Short = lv_obj_get_style_line_width(lvObjPtr, part)

public actual fun LvglObjectBase.setLineDashWidthStyle(value: Short, selector: UInt) {
    lv_obj_set_style_line_dash_width(obj = lvObjPtr, value = value, selector = selector)
}

public actual fun LvglObjectBase.getLineDashWidthStyle(part: UInt): Short =
    lv_obj_get_style_line_dash_width(lvObjPtr, part)

public actual fun LvglObjectBase.setLineDashGapStyle(value: Short, selector: UInt) {
    lv_obj_set_style_line_dash_gap(obj = lvObjPtr, selector = selector, value = value)
}

public actual fun LvglObjectBase.getLineDashGapStyle(part: UInt): Short =
    lv_obj_get_style_line_dash_gap(lvObjPtr, part)

public actual fun LvglObjectBase.setLineRoundedStyle(value: Boolean, selector: UInt) {
    lv_obj_set_style_line_rounded(obj = lvObjPtr, value = value, selector = selector)
}

public actual fun LvglObjectBase.getLineRoundedStyle(part: UInt): Boolean =
    lv_obj_get_style_line_rounded(lvObjPtr, part)

public actual fun LvglObjectBase.setLineColorStyle(value: Color, selector: UInt) {
    lvglkt_obj_set_style_line_color(obj = lvObjPtr, value = value.toCValue(), selector = selector)
}

public actual fun LvglObjectBase.getLineColorStyle(part: UInt): Color =
    lvglkt_obj_get_style_line_color(lvObjPtr, part).toColor()

public actual fun LvglObjectBase.setLineOpacityStyle(value: UByte, selector: UInt) {
    lv_obj_set_style_line_opa(obj = lvObjPtr, selector = selector, value = value)
}

public actual fun LvglObjectBase.getLineOpacityStyle(part: UInt): UByte = lv_obj_get_style_line_opa(lvObjPtr, part)

public actual fun LvglObjectBase.setOpacityStyle(value: UByte, selector: UInt) {
    lv_obj_set_style_opa(obj = lvObjPtr, selector = selector, value = value)
}

public actual fun LvglObjectBase.getOpacityStyle(part: UInt): UByte = lv_obj_get_style_opa(lvObjPtr, part)

public actual fun LvglObjectBase.setColorFilterOpacityStyle(value: UByte, selector: UInt) {
    lv_obj_set_style_color_filter_opa(obj = lvObjPtr, value = value, selector = selector)
}

public actual fun LvglObjectBase.getColorFilterOpacityStyle(part: UInt): UByte =
    lv_obj_get_style_color_filter_opa(lvObjPtr, part)

public actual fun LvglObjectBase.setBackgroundOpacityStyle(value: UByte, selector: UInt) {
    lv_obj_set_style_bg_opa(obj = lvObjPtr, value = value, selector = selector)
}

public actual fun LvglObjectBase.getBackgroundOpacityStyle(part: UInt): UByte = lv_obj_get_style_bg_opa(lvObjPtr, part)

public actual fun LvglObjectBase.setBackgroundImageOpacityStyle(value: UByte, selector: UInt) {
    lv_obj_set_style_bg_img_opa(obj = lvObjPtr, selector = selector, value = value)
}

public actual fun LvglObjectBase.getBackgroundImageOpacityStyle(part: UInt): UByte =
    lv_obj_get_style_bg_img_opa(lvObjPtr, part)

public actual fun LvglObjectBase.setBackgroundImageRecolorOpacityStyle(value: UByte, selector: UInt) {
    lv_obj_set_style_bg_img_opa(obj = lvObjPtr, selector = selector, value = value)
}

public actual fun LvglObjectBase.getBackgroundImageRecolorOpacityStyle(part: UInt): UByte =
    lv_obj_get_style_bg_img_opa(lvObjPtr, part)

public actual fun LvglObjectBase.setBorderOpacityStyle(value: UByte, selector: UInt) {
    lv_obj_set_style_border_opa(obj = lvObjPtr, value = value, selector = selector)
}

public actual fun LvglObjectBase.getBorderOpacityStyle(part: UInt): UByte = lv_obj_get_style_border_opa(lvObjPtr, part)

public actual fun LvglObjectBase.setTextOpacityStyle(value: UByte, selector: UInt) {
    lv_obj_set_style_text_opa(obj = lvObjPtr, selector = selector, value = value)
}

public actual fun LvglObjectBase.getTextOpacityStyle(part: UInt): UByte = lv_obj_get_style_text_opa(lvObjPtr, part)

public actual fun LvglObjectBase.setLayoutStyle(value: UShort, selector: UInt) {
    lv_obj_set_style_layout(obj = lvObjPtr, value = value, selector = selector)
}

public actual fun LvglObjectBase.getLayoutStyle(part: UInt): UShort = lv_obj_get_style_layout(lvObjPtr, part)

public actual fun LvglObjectBase.setFlexFlowStyle(value: UInt, selector: UInt) {
    lv_obj_set_style_flex_flow(obj = lvObjPtr, value = value, selector = selector)
}

public actual fun LvglObjectBase.getFlexFlowStyle(part: UInt): UInt = lv_obj_get_style_flex_flow(lvObjPtr, part)

public actual fun LvglObjectBase.setFlexGrowStyle(value: UByte, selector: UInt) {
    lv_obj_set_style_flex_grow(obj = lvObjPtr, selector = selector, value = value)
}

public actual fun LvglObjectBase.getFlexGrowStyle(part: UInt): UByte = lv_obj_get_style_flex_grow(lvObjPtr, part)

public actual fun LvglObjectBase.setFlexCrossPlaceStyle(value: FlexAlignment, selector: UInt) {
    lv_obj_set_style_flex_cross_place(obj = lvObjPtr, value = value.toLvFlexAlign(), selector = selector)
}

public actual fun LvglObjectBase.getFlexCrossPlaceStyle(part: UInt): FlexAlignment =
    lv_obj_get_style_flex_cross_place(lvObjPtr, part).value.toFlexAlignment()

public actual fun LvglObjectBase.setFlexTrackPlaceStyle(value: FlexAlignment, selector: UInt) {
    lv_obj_set_style_flex_track_place(obj = lvObjPtr, value = value.toLvFlexAlign(), selector = selector)
}

public actual fun LvglObjectBase.getFlexTrackPlaceStyle(part: UInt): FlexAlignment =
    lv_obj_get_style_flex_track_place(lvObjPtr, part).value.toFlexAlignment()

public actual fun LvglObjectBase.setFlexMainPlaceStyle(value: FlexAlignment, selector: UInt) {
    lv_obj_set_style_flex_main_place(obj = lvObjPtr, value = value.toLvFlexAlign(), selector = selector)
}

public actual fun LvglObjectBase.getFlexMainPlaceStyle(part: UInt): FlexAlignment =
    lv_obj_get_style_flex_main_place(lvObjPtr, part).value.toFlexAlignment()

public actual fun LvglObjectBase.setBackgroundImageSourceStyle(value: String, selector: UInt): StringReference {
    val result = StringReference.create(value)
    lv_obj_set_style_bg_img_src(obj = lvObjPtr, selector = selector, value = result.ref)
    return result
}

public actual fun LvglObjectBase.setBackgroundImageSourceStyle(value: StringReference, selector: UInt) {
    lv_obj_set_style_bg_img_src(obj = lvObjPtr, selector = selector, value = value.ref)
}

public actual fun LvglObjectBase.setArcImageSourceStyle(value: StringReference, selector: UInt) {
    lv_obj_set_style_arc_img_src(obj = lvObjPtr, value = value.ref, selector = selector)
}

public actual fun LvglObjectBase.setArcImageSourceStyle(value: String, selector: UInt): StringReference {
    val result = StringReference.create(value)
    lv_obj_set_style_arc_img_src(obj = lvObjPtr, selector = selector, value = result.ref)
    return result
}

public actual fun LvglObjectBase.setTextFont(value: Font, selector: UInt) {
    lv_obj_set_style_text_font(obj = lvObjPtr, value = value.lvFontPtr, selector = selector)
}

public actual fun LvglObjectBase.getTextFont(part: UInt): Font =
    lv_obj_get_style_text_font(lvObjPtr, part).toFont()
