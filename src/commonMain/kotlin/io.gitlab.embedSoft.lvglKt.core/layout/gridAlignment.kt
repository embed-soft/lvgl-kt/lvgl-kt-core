package io.gitlab.embedSoft.lvglKt.core.layout

/** Provides options used to align children in a Flex layout. */
public enum class GridAlignment {
    START,
    CENTER,
    END,
    STRETCH,
    SPACE_EVENLY,
    SPACE_AROUND,
    SPACE_BETWEEN
}
