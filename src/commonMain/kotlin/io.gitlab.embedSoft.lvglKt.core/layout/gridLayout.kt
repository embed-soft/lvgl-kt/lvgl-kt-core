package io.gitlab.embedSoft.lvglKt.core.layout

import io.gitlab.embedSoft.lvglKt.core.lvglObject.LvglObjectBase
import io.gitlab.embedSoft.lvglKt.core.lvglObject.LvglPart
import io.gitlab.embedSoft.lvglKt.core.styling.Style

/**
 * Changes the grid alignment.
 * @param columnAlign The column's alignment.
 * @param rowAlign The row's alignment.
 */
public expect fun LvglObjectBase.setGridAlignment(columnAlign: GridAlignment, rowAlign: GridAlignment)

/**
 * Set the cell of an object. The object's parent needs to have grid layout, else nothing will happen.
 * @param columnAlign The vertical alignment in the cell.
 * @param colPos Column position.
 * @param colSpan The number of columns to take (>= 1).
 * @param rowAlign The horizontal alignment in the cell.
 * @param rowPos Row position.
 * @param rowSpan The number of rows to take (>= 1).
 */
public expect fun LvglObjectBase.setGridCell(
    columnAlign: GridAlignment,
    colPos: UByte,
    colSpan: UByte,
    rowAlign: GridAlignment,
    rowPos: UByte,
    rowSpan: UByte
)

public expect fun Style.setGridRowDescriptor(vararg values: Short)

public expect fun Style.setGridColumnDescriptor(vararg values: Short)

public expect fun Style.setGridRowAlignment(value: GridAlignment)

public expect fun Style.setGridColumnAlignment(value: GridAlignment)

public expect fun Style.setGridCellColumnPosition(value: Short)

public expect fun Style.setGridCellColumnSpan(value: Short)

public expect fun Style.setGridCellRowPosition(value: Short)

public expect fun Style.setGridCellRowSpan(value: Short)

public expect fun Style.setGridCellXAlignment(value: Short)

public expect fun Style.setGridCellYAlignment(value: Short)

public expect fun LvglObjectBase.getStyleGridRowDescriptor(part: LvglPart = LvglPart.MAIN): Array<Short>

public expect fun LvglObjectBase.getStyleGridColumnDescriptor(part: LvglPart = LvglPart.MAIN): Array<Short>

public expect fun LvglObjectBase.getStyleGridRowAlignment(part: LvglPart = LvglPart.MAIN): GridAlignment

public expect fun LvglObjectBase.getStyleGridColumnAlignment(part: LvglPart = LvglPart.MAIN): GridAlignment

public expect fun LvglObjectBase.getStyleGridCellColumnPosition(part: LvglPart = LvglPart.MAIN): Short

public expect fun LvglObjectBase.getStyleGridCellColumnSpan(part: LvglPart = LvglPart.MAIN): Short

public expect fun LvglObjectBase.getStyleGridCellRowPosition(part: LvglPart = LvglPart.MAIN): Short

public expect fun LvglObjectBase.getStyleGridCellRowSpan(part: LvglPart = LvglPart.MAIN): Short

public expect fun LvglObjectBase.getStyleGridCellXAlignment(part: LvglPart = LvglPart.MAIN): Short

public expect fun LvglObjectBase.getStyleGridCellYAlignment(part: LvglPart = LvglPart.MAIN): Short

public expect fun LvglObjectBase.setStyleGridRowDescriptor(selector: UInt, vararg values: Short)

public expect fun LvglObjectBase.setStyleGridColumnDescriptor(selector: UInt, vararg values: Short)

public expect fun LvglObjectBase.setStyleGridRowAlignment(selector: UInt, value: GridAlignment)

public expect fun LvglObjectBase.setStyleGridColumnAlignment(selector: UInt, value: GridAlignment)

public expect fun LvglObjectBase.setStyleGridCellColumnPosition(selector: UInt, value: Short)

public expect fun LvglObjectBase.setStyleGridCellColumnSpan(selector: UInt, value: Short)

public expect fun LvglObjectBase.setStyleGridCellRowPosition(selector: UInt, value: Short)

public expect fun LvglObjectBase.setStyleGridCellRowSpan(selector: UInt, value: Short)

public expect fun LvglObjectBase.setStyleGridCellXAlignment(selector: UInt, value: Short)

public expect fun LvglObjectBase.setStyleGridCellYAlignment(selector: UInt, value: Short)
