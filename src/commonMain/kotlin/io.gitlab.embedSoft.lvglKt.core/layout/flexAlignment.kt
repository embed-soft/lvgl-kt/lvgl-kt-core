package io.gitlab.embedSoft.lvglKt.core.layout

/** Provides options used to align children in a Flex layout. */
public enum class FlexAlignment {
    START,
    END,
    CENTER,
    SPACE_EVENLY,
    SPACE_AROUND,
    SPACE_BETWEEN
}

/** A unique value for [FlexAlignment]. */
public expect val FlexAlignment.value: UInt

/**
 * Converts a value to a [FlexAlignment].
 * @return The corresponding [FlexAlignment] or a [IllegalStateException] if the value isn't recognised.
 */
public expect fun UInt.toFlexAlignment(): FlexAlignment
