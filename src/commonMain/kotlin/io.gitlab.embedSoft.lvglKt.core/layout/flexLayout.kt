package io.gitlab.embedSoft.lvglKt.core.layout

import io.gitlab.embedSoft.lvglKt.core.lvglObject.LvglObjectBase

/**
 * Sets up the Flexbox layout on the object. All possible values for this function include the following:
 * - `ROW` : Place the children in a row without wrapping
 * - `COLUMN` : Place the children in a column without wrapping
 * - `ROW_WRAP` Place the children in a row with wrapping
 * - `COLUMN_WRAP` : Place the children in a column with wrapping
 * - `ROW_REVERSE` : Place the children in a row without wrapping but in reversed order
 * - `COLUMN_REVERSE` : Place the children in a column without wrapping but in reversed order
 * - `ROW_WRAP_REVERSE` : Place the children in a row without wrapping but in reversed order
 * - `COLUMN_WRAP_REVERSE` : Place the children in a column without wrapping but in reversed order
 * @param flow The type of Flexbox layout to use.
 * @see LvglObjectBase.setLayout
 */
public expect fun LvglObjectBase.setFlexFlow(flow: FlexFlow)

/**
 * Used to make one or more children fill the available space on the track. If more children has grow the
 * available space will be distributed proportionally to the grow values. For example, let's say there is *400*
 * pixels remaining space and *4* objects with grow:
 * - `A` with grow = 1
 * - `B` with grow = 1
 * - `C` with grow = 2
 *
 * With the above example `A` and `B` will have *100* pixel size, and `C` will have *200* pixel size. Flex grow
 * can be set on a child with [setFlexFlow], where the **flow** parameter needs to be > *1* or *0* to disable grow
 * on the child.
 */
public expect fun LvglObjectBase.setFlexGrow(grow: UByte)

/**
 * Manages placement of children in a Flexbox layout. The possible values are the following:
 * - `START` means left on a horizontally and top vertically (default)
 * - `END` means right on a horizontally and bottom vertically
 * - `CENTER` simply center
 * - `SPACE_EVENLY` items are distributed so that the spacing between any two items (and the space
 * to the edges) is equal; doesn't apply to [trackCrossPlace]
 * - `SPACE_AROUND` items are evenly distributed in the track with equal space around them; note that
 * visually the spaces aren't equal, since all the items have equal space on both sides; the first item will have
 * one unit of space against the container edge, but two units of space between the next item because that next
 * item has its own spacing that applies; doesn't apply to [trackCrossPlace]
 * - `SPACE_BETWEEN` items are evenly distributed in the track: first item is on the start line, last item on the
 * end line; doesn't apply to [trackCrossPlace]
 * @param mainPlace Determines how to distribute the items in their track on the main axis. For example, flush the
 * items to the right on [FlexFlow.ROW_WRAP] (it's called **justify-content** in CSS).
 * @param crossPlace Determines how to distribute the items in their track on the cross axis. For example, if the
 * items have different height place them to the bottom of the track (it's called **align-items** in CSS).
 * @param trackCrossPlace Determines how to distribute the tracks (it's called **align-content** in CSS).
 */
public expect fun LvglObjectBase.setFlexAlign(
    mainPlace: FlexAlignment,
    crossPlace: FlexAlignment,
    trackCrossPlace: FlexAlignment
)
