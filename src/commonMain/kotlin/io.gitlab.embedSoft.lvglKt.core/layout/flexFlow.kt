package io.gitlab.embedSoft.lvglKt.core.layout

/** Provides options used to align children in a Flex layout. */
public enum class FlexFlow {
    ROW,
    COLUMN,
    ROW_WRAP,
    ROW_REVERSE,
    ROW_WRAP_REVERSE,
    COLUMN_WRAP,
    COLUMN_REVERSE,
    COLUMN_WRAP_REVERSE
}

/** A unique value for [FlexFlow]. */
public expect val FlexFlow.value: UInt

/**
 * Converts a value to a [FlexFlow].
 * @return The corresponding [FlexFlow] or a [IllegalStateException] if the value isn't recognised.
 */
public expect fun UInt.toFlexFlow(): FlexFlow
