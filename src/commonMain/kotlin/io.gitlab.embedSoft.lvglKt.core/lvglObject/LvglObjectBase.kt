package io.gitlab.embedSoft.lvglKt.core.lvglObject

import io.gitlab.embedSoft.lvglKt.core.Area
import io.gitlab.embedSoft.lvglKt.core.Closable
import io.gitlab.embedSoft.lvglKt.core.Point
import io.gitlab.embedSoft.lvglKt.core.StringReference
import io.gitlab.embedSoft.lvglKt.core.event.LvglEventCallback
import io.gitlab.embedSoft.lvglKt.core.event.LvglEventType
import io.gitlab.embedSoft.lvglKt.core.layout.GridAlignment
import io.gitlab.embedSoft.lvglKt.core.styling.Style

/** Represents a LVGL object. */
public expect interface LvglObjectBase : Closable {
    /** The vertical size of the object. */
    public open var height: Short
    /** The horizontal size of the object. */
    public open var width: Short
    /** The object's group.  */
    public open val group: LvglObjectBase?
    /** Check if any object is still **alive**, and part of the hierarchy. */
    public open val isValid: Boolean
    /** The object's parent. */
    public open var parent: LvglObjectBase?
    /** The object's screen. */
    public open val screen: LvglObjectBase
    /** The content width. */
    public open var contentWidth: Short
    /** The content height. */
    public open var contentHeight: Short
    /** Will be *true* if the object can be edited. */
    public open val isEditable: Boolean
    /** Will be *true* if the object is scrolling. */
    public open val isScrolling: Boolean
    /** Will be *true* if the object is visible. */
    public open val isVisible: Boolean
    /** Self width. */
    public open val selfWidth: Short
    /** Self height. */
    public open val selfHeight: Short
    /** X coordinate. */
    public open var xPos: Short
    /** Y coordinate. */
    public open var yPos: Short
    /** The bottom position of the scroll. */
    public open val scrollBottom: Short
    /** The top position of the scroll. */
    public open val scrollTop: Short
    /** The left position of the scroll. */
    public open val scrollLeft: Short
    /** The right position of the scroll. */
    public open val scrollRight: Short
    /** The end point of the scroll. */
    public open val scrollEnd: Point
    /** The horizontal position of the scroll. */
    public open val scrollXPos: Short
    /** The vertical position of the scroll. */
    public open val scrollYPos: Short
    /** The scroll bar area. Split into horizontal and vertical areas. */
    public open val scrollBarArea: Pair<Area, Area>
    /**
     * The scroll direction (eg `LV_DIR_TOP`). Do note that this property can be set with or-ed values, eg
     * `LV_DIR_TOP or LV_DIR_LEFT`. Below are the possible values:
     * - LV_DIR_TOP : only scroll up
     * - LV_DIR_LEFT : only scroll left
     * - LV_DIR_BOTTOM : only scroll down
     * - LV_DIR_RIGHT : only scroll right
     * - LV_DIR_HOR : only scroll horizontally
     * - LV_DIR_TOP : only scroll vertically
     * - LV_DIR_ALL : scroll any directions
     */
    public open var scrollDir: UByte
    /** The horizontal position of the scroll snap. */
    public open val scrollSnapXPos: UByte
    /** The vertical position of the scroll snap. */
    public open val scrollSnapYPos: UByte
    /** The scroll bar mode. */
    public open var scrollBarMode: ScrollBarMode

    /** Moves the object's foreground. */
    public open fun moveForeground()

    /** Moves the object's background. */
    public open fun moveBackground()

    /**
     * Moves the object to a new position.
     * @param xPos X coordinate.
     * @param yPos Y coordinate
     */
    public open fun moveTo(xPos: Short, yPos: Short)

    /**
     * Changes the object's alignment. Can use [LvglAlignment] in combination with [LvglAlignment.value] as
     * [alignment].
     * @param alignment The new alignment value.
     * @see LvglAlignment
     */
    public open fun setAlignment(alignment: UByte)

    /**
     * Changes the position of the object.
     * @param xPos X coordinate.
     * @param yPos Y coordinate.
     */
    public open fun setPosition(xPos: Short, yPos: Short)

    /**
     * Aligns the object relative to its parent.
     * @param align The alignment to use.
     * @param xOffset X offset.
     * @param yOffset Y offset.
     */
    public open fun align(align: UByte, xOffset: Short, yOffset: Short)

    /**
     * Aligns the object to another [object][base].
     * @param base The base object that the object is aligned to.
     * @param align The alignment to use.
     * @param xOffset X offset.
     * @param yOffset Y offset.
     */
    public open fun alignTo(base: LvglObjectBase, align: UByte, xOffset: Short, yOffset: Short)

    /** Centers the object relative to its parent. */
    public open fun center()

    /** Removes all the object's children. */
    public open fun clean()

    /**
     * Add one or more states to the object. The other state bits will remain unchanged. If specified in the styles,
     * transition animation will be started from the previous state to the current.
     * @param state The states to add. E.g `LV_STATE_PRESSED or LV_STATE_FOCUSED`.
     */
    public open fun addState(state: UShort)

    /**
     * Add a state to the object. The other state bits will remain unchanged. If specified in the styles,
     * transition animation will be started from the previous state to the current.
     * @param state The state to add. E.g [LvglState.PRESSED].
     */
    public open fun addState(state: LvglState)

    /**
     * Remove one or more states to the object. The other state bits will remain unchanged. If specified in the styles,
     * transition animation will be started from the previous state to the current.
     * @param state The states to clear. E.g `LV_STATE_PRESSED or LV_STATE_FOCUSED`.
     */
    public open fun clearState(state: UShort)

    /**
     * Remove a state from the object. The other state bits will remain unchanged. If specified in the styles,
     * transition animation will be started from the previous state to the current.
     * @param state The state to clear. E.g [LvglState.FOCUSED].
     */
    public open fun clearState(state: LvglState)

    /**
     * Get the state of an object.
     * @return The state (or-ed values).
     */
    public open fun getState(): UShort

    /**
     * Check if the object is in a given state or not.
     * @param state A state or combination of states to check.
     * @return A value of *true* if the object is in [state].
     */
    public open fun hasState(state: UShort): Boolean

    /**
     * Check if the object is in a given state or not.
     * @param state The state to check.
     * @return A value of *true* if the object is in [state].
     */
    public open fun hasState(state: LvglState): Boolean

    /**
     * Set one or more flags.
     * @param flags The values to set (eg `LV_OBJ_FLAG_HIDDEN`).
     */
    public open fun addFlag(flags: UInt)

    /**
     * Set a flag.
     * @param flag The value to set (eg [LvglFlag.HIDDEN]).
     */
    public open fun addFlag(flag: LvglFlag)

    /**
     * Check if a given flag or all the given flags are set on an object.
     * @param flags The flag(s) to check (or-ed values can be used).
     * @return A value of *true* if all flags are set.
     */
    public open fun hasFlag(flags: UInt): Boolean

    /**
     * Check if a given flag is set on an object.
     * @param flag The flag to check.
     * @return A value of *true* if [flag] is set.
     */
    public open fun hasFlag(flag: LvglFlag): Boolean

    /**
     * Clear one or more flags.
     * @param flags The xor-ed values to set.
     */
    public open fun clearFlag(flags: UInt)

    /**
     * Clear a [flag].
     * @param flag The flag to clear.
     */
    public open fun clearFlag(flag: LvglFlag)

    /**
     * Check if a given flag, or any of the flags are set on an object.
     * @param flags The flags to check.
     * @return A value of *true* if at least one flag is set.
     */
    public open fun hasAnyFlag(flags: UInt): Boolean

    /**
     * Scale the given number of pixels (a distance or size) relative to a *160* DPI display, considering the DPI of
     * the object's display. It ensures that e.g. `lv_dpx(100)` will have the same physical size regardless to the DPI
     * of the display.
     * @param pixels The number of pixels to scale.
     * @return The number of pixels that the object has been scaled.
     */
    public open fun scalePixels(pixels: Short): Short

    /**
     * Adds an event callback to this object.
     * @param filter The event type to use for filtering events (eg [LvglEventType.CLICKED]).
     * @param callback The event callback to add.
     */
    public open fun addEventCallback(filter: LvglEventType, callback: LvglEventCallback)

    /**
     * Adds an event callback to this object.
     * @param filter The event type to use for filtering events. Note that [LvglEventType] can provide the event type
     * via the **value** extension property.
     * @param callback The event callback to add.
     */
    public open fun addEventCallback(filter: UInt, callback: LvglEventCallback)

    /**
     * Removes an existing event callback from this object.
     * @param callback The event callback to remove.
     * @return A value of *true* if the event callback was removed.
     */
    public open fun removeEventCallback(callback: LvglEventCallback): Boolean

    /**
     * Adds a new style to the LVGL object.
     * @param style The style to add.
     * @param selector A combination of parts and state (an or-ed value). Will use default style if this parameter
     * isn't specified.
     */
    public open fun addStyle(style: Style, selector: UInt = 0u)

    /**
     * Removes the style from the LVGL object.
     * @param style The style to remove.
     * @param selector A combination of parts and state (an or-ed value) that acts as a filter for removing the style.
     * If this parameter isn't specified then the default style is used as the filter.
     */
    public open fun removeStyle(style: Style, selector: UInt = 0u)

    /** Removes all styles from the LVGL object. */
    public open fun removeAllStyles()

    /** Redraws the LVGL object. */
    public open fun invalidate()

    /**
     * Changes the object's size.
     * @param width The new width to use.
     * @param height The new height to use.
     */
    public open fun setSize(width: Short, height: Short)

    /**
     * Changes the layout.
     * @param layout The layout to use for the object.
     */
    public open fun setLayout(layout: UInt)

    /** Updates layout on the object. */
    public open fun updateLayout()

    /**
     * Manually sends an event to this object.
     * @param eventCode The event code (eg `LV_EVENT_VALUE_CHANGED`).
     * @param param The parameter to pass through, or *null* for no parameter.
     */
    public open fun sendEvent(eventCode: UInt, param: StringReference? = null)

    /**
     * Manually sends an event to this object.
     * @param eventCode The event code (eg `LV_EVENT_VALUE_CHANGED`).
     * @param param The parameter to pass through, or null for no parameter.
     * @return The [StringReference] object that represents [param], or *null* for no parameter.
     */
    public open fun sendEvent(eventCode: UInt, param: String? = null): StringReference?

    /** Forces the object's scroll bar to be redrawn. */
    public open fun invalidateScrollBar()

    /**
     * Scrolls the object by [x][xPos] and [y][yPos].
     * @param xPos The horizontal position.
     * @param yPos The vertical position.
     * @param enableAnimation If *true* then enable the animation.
     */
    public open fun scrollBy(xPos: Short, yPos: Short, enableAnimation: Boolean)

    /**
     * Scrolls the object to the position ([x][xPos] and [y][yPos]).
     * @param xPos The horizontal position.
     * @param yPos The vertical position.
     * @param enableAnimation If *true* then enable the animation.
     */
    public open fun scrollTo(xPos: Short, yPos: Short, enableAnimation: Boolean)

    /**
     * Scrolls to the object.
     * @param enableAnimation If *true* then enable the animation.
     */
    public open fun scrollToView(enableAnimation: Boolean)

    /**
     * Scrolls to the object recursively.
     * @param enableAnimation If *true* then enable the animation.
     */
    public open fun scrollToViewRecursive(enableAnimation: Boolean)

    /**
     * Scrolls the object to [xPos].
     * @param xPos The horizontal position.
     * @param enableAnimation If *true* then enable the animation.
     */
    public open fun scrollToX(xPos: Short, enableAnimation: Boolean)

    /**
     * Scrolls the object to [yPos].
     * @param yPos The vertical position.
     * @param enableAnimation If *true* then enable the animation.
     */
    public open fun scrollToY(yPos: Short, enableAnimation: Boolean)

    /** Readjusts scrolling. */
    public open fun readjustScroll(enableAnimation: Boolean)
}
