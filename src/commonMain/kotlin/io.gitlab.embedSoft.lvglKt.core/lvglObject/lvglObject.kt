package io.gitlab.embedSoft.lvglKt.core.lvglObject

/** The default implementation of [LvglObjectBase]. */
public expect class LvglObject : LvglObjectBase {
    public companion object {
        /**
         * Create a base object (a rectangle).
         * @param parent The parent object, or *null* to create a screen.
         * @return The new object.
         */
        public fun create(parent: LvglObjectBase? = null): LvglObject
    }
}

/**
 * Create a LVGL object (a rectangle).
 * @param parent The parent object, or *null* to create a screen.
 * @param init An initialization block that sets up the LVGL object.
 * @return The new LVGL object.
 */
public fun lvglObject(parent: LvglObjectBase? = null, init: LvglObject.() -> Unit = {}): LvglObject {
    val result = LvglObject.create(parent)
    result.init()
    return result
}
