package io.gitlab.embedSoft.lvglKt.core.lvglObject

/**
 * Represents an [LVGL object][LvglObjectBase] state. The states are usually automatically changed by the library as
 * the user presses, releases, focuses etc an object. However the states can be changed manually too. To set or clear
 * given state (but leave the other states untouched) use [LvglObjectBase.addState] or [LvglObjectBase.clearState]. In
 * both cases ORed state values can be used as well. For example:
 * ```kotlin
 * // ...
 * obj.addState(LV_STATE_PRESSED or LV_PRESSED_CHECKED)
 * ```
 */
public enum class LvglState {
    /** Normal, released state. */
    DEFAULT,
    /** Toggled or checked state. */
    CHECKED,
    /** Focused via keypad or encoder or clicked via touchpad/mouse. */
    FOCUSED,
    /** Focused via keypad or encoder but not via touchpad/mouse. */
    FOCUS_KEY,
    /** Edit by an encoder. */
    EDITED,
    /** Hovered by mouse (not supported now). */
    HOVERED,
    /** Being pressed. */
    PRESSED,
    /** Being scrolled. */
    SCROLLED,
    /** Disabled state. */
    DISABLED,
    /** Custom state. */
    USER_1,
    /** Custom state. */
    USER_2,
    /** Custom state. */
    USER_3,
    /** Custom state. */
    USER_4
}

/** A unique value for the [LvglState]. */
public expect val LvglState.value: UInt

/**
 * Converts a value to a [LvglState].
 * @return The corresponding [LvglState] or a [IllegalStateException] if the value isn't recognised.
 */
public expect fun UInt.toLvglState(): LvglState
