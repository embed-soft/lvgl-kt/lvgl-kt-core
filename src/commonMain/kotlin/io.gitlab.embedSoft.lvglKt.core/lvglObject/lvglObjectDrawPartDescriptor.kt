package io.gitlab.embedSoft.lvglKt.core.lvglObject

import io.gitlab.embedSoft.lvglKt.core.Area
import io.gitlab.embedSoft.lvglKt.core.Point
import io.gitlab.embedSoft.lvglKt.core.drawing.*

/** Represents a draw part for a LVGL object. */
public expect class LvglObjectDrawPartDescriptor {
    /** The area of the part being drawn. */
    public val drawArea: Area
    /** A point calculated during drawing. For example, a point of chart or the center of an arc. */
    public val point1: Point
    /** A point calculated during drawing. For example, a point from a chart. */
    public val point2: Point
    /** The current part for which the event is sent. */
    public val part: UInt
    /** The index of the part. For example, a button's index on button matrix or table cell index. */
    public val id: UInt
    /** The radius of an arc (not the corner radius). */
    public val radius: Short
    /** A value calculated during drawing. E.g. Chart's tick line value. */
    public val value: Int
    /** A text calculated during drawing. Can be modified. For example, tick labels on a chart axis. */
    public val text: String
    /** A draw descriptor that can be modified to changed what LVGL will draw. Set only for rectangle-like parts. */
    public val rectDescriptor: RectangleDescriptor
    /** A draw descriptor that can be modified to changed what LVGL will draw. Set only for text-like parts. */
    public val labelDescriptor: LabelDescriptor
    /** A draw descriptor that can be modified to changed what LVGL will draw. Set only for line-like parts. */
    public val lineDescriptor: LineDescriptor
    /** A draw descriptor that can be modified to changed what LVGL will draw. Set only for image-like parts. */
    public val imageDescriptor: DrawImageDescriptor
    /** A draw descriptor that can be modified to changed what LVGL will draw. Set only for arc-like parts. */
    public val arcDescriptor: ArcDescriptor
}
