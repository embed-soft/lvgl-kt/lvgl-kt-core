package io.gitlab.embedSoft.lvglKt.core.lvglObject

/** On/Off features controlling the object's behavior. Note that OR-ed values are possible. */
public enum class LvglFlag {
    /** Make the object hidden. Like it wasn't there at all. */
    HIDDEN,
    /** Make the object clickable by the input devices. */
    CLICKABLE,
    /** Add focused state to the object when clicked. */
    CLICK_FOCUSABLE,
    /** Toggle checked state when the object is clicked. */
    CHECKABLE,

    /** Make the object scrollable. */
    SCROLLABLE,
    /** Allow scrolling inside but with slower speed. */
    SCROLL_ELASTIC,
    /** Make the object scroll further when **thrown**. */
    SCROLL_MOMENTUM,
    /** Allow scrolling only one snapable children. */
    SCROLL_ONE,
    /** Allow propagating the horizontal scroll to a parent. */
    SCROLL_CHAIN_HOR,
    /** Allow propagating the vertical scroll to a parent. */
    SCROLL_CHAIN_VER,
    /** Allow propagating the scroll to a parent. */
    SCROLL_CHAIN,
    /** Automatically scroll object to make it visible when focused. */
    SCROLL_ON_FOCUS,
    /** Allow scrolling the focused object with arrow keys. */
    SCROLL_WITH_ARROW,

    /** If scroll snap is enabled on the parent it can snap to this object. */
    SNAPABLE,
    /** Keep the object pressed even if the press slid from the object. */
    PRESS_LOCK,
    /** Propagate the events to the parent too. */
    EVENT_BUBBLE,
    /** Propagate the gestures to the parent. */
    GESTURE_BUBBLE,
    /** Allow performing more accurate hit (click) test. For example, consider rounded corners. */
    ADVANCED_HIT_TEST,
    /** Make the object position-able by the layouts. */
    IGNORE_LAYOUT,
    /** Do not scroll the object when the parent scrolls and ignore layout. */
    FLOATING,
    /** Do not clip the children's content to the parent's boundary. */
    OVERFLOW_VISIBLE,

    /** Custom flag, free to use by layouts. */
    LAYOUT_1,
    /** Custom flag, free to use by layouts. */
    LAYOUT_2,

    /** Custom flag, free to use by widget. */
    WIDGET_1,
    /** Custom flag, free to use by widget. */
    WIDGET_2,

    /** Custom flag, free to use by user. */
    USER_1,
    /** Custom flag, free to use by user. */
    USER_2,
    /** Custom flag, free to use by user. */
    USER_3,
    /** Custom flag, free to use by user. */
    USER_4
}

/** A unique value for the [LvglFlag]. */
public expect val LvglFlag.value: UInt

/**
 * Converts a value to a [LvglFlag].
 * @return The corresponding [LvglFlag] or a [IllegalStateException] if the value isn't recognised.
 */
public expect fun UInt.toLvglFlag(): LvglFlag
