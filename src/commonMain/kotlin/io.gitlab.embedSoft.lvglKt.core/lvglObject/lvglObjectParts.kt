package io.gitlab.embedSoft.lvglKt.core.lvglObject

/**
 * Represents a part of an [LVGL object][LvglObjectBase]. For example a [Base object][LvglObjectBase] uses the main and
 * scrollbar parts, but a Slider uses the main, indicator, and the knob parts. Parts are similar to pseudo elements in
 * CSS.
 */
public enum class LvglPart {
    /** A background like rectangle. */
    MAIN,
    /** The scrollbars. */
    SCROLLBAR,
    /** Indicator, e.g. for slider, bar, switch, or the tick box of the checkbox. */
    INDICATOR,
    /** Like a handle to grab to adjust the value. */
    KNOB,
    /** Indicate the currently selected option or section. */
    SELECTED,
    /** Used if the widget has multiple similar elements (e.g. table cells). */
    ITEMS,
    /** Ticks on scales e.g. for a chart or meter. */
    TICKS,
    /** Mark a specific place e.g. text area's or chart's cursor. */
    CURSOR,
    /** Custom parts can be added from here. */
    CUSTOM_FIRST
}

/** A unique value for the [LvglPart]. */
public expect val LvglPart.value: UInt

/**
 * Converts a value to a [LvglPart].
 * @return The corresponding [LvglPart] or a [IllegalStateException] if the value isn't recognised.
 */
public expect fun UInt.toLvglPart(): LvglPart
