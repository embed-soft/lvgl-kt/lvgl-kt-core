package io.gitlab.embedSoft.lvglKt.core.lvglObject

/** Represents the scroll bar mode. How the scroll bar behaves. */
public enum class ScrollBarMode(public val value: UByte) {
    /** Never show scrollbars. */
    OFF(0.toUByte()),
    /** Always show scrollbars. */
    ON(1.toUByte()),
    /** Show scroll bars when object is being scrolled. */
    ACTIVE(2.toUByte()),
    /** Show scroll bars when the content is large enough to be scrolled. */
    AUTO(3.toUByte())
}

public expect fun UByte.toScrollBarMode(): ScrollBarMode
