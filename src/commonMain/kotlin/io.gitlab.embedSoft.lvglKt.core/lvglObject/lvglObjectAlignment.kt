package io.gitlab.embedSoft.lvglKt.core.lvglObject

/** Provides options used to align a [LVGL object][LvglObjectBase] outside of a reference LVGL object. */
public enum class LvglOutsideAlignment {
    TOP_LEFT,
    TOP_MID,
    TOP_RIGHT,
    BOTTOM_LEFT,
    BOTTOM_MID,
    BOTTOM_RIGHT,
    LEFT_TOP,
    LEFT_MID,
    LEFT_BOTTOM,
    RIGHT_TOP,
    RIGHT_MID,
    RIGHT_BOTTOM
}

/** A unique value for the [LvglOutsideAlignment]. */
public expect val LvglOutsideAlignment.value: UInt

/**
 * Converts a value to a [LvglOutsideAlignment].
 * @return The corresponding [LvglOutsideAlignment] or a [IllegalStateException] if the value isn't recognised.
 */
public expect fun UInt.toLvglOutsideAlignment(): LvglOutsideAlignment

/** Provides options used to align a [LVGL object][LvglObjectBase]. */
public enum class LvglAlignment {
    TOP_LEFT,
    TOP_MID,
    TOP_RIGHT,
    BOTTOM_LEFT,
    BOTTOM_MID,
    BOTTOM_RIGHT,
    LEFT_MID,
    RIGHT_MID,
    CENTER
}

/** A unique value for the [LvglAlignment]. */
public expect val LvglAlignment.value: UInt

/**
 * Converts a value to a [LvglAlignment].
 * @return The corresponding [LvglAlignment] or a [IllegalStateException] if the value isn't recognised.
 */
public expect fun UInt.toLvglAlignment(): LvglAlignment
