package io.gitlab.embedSoft.lvglKt.core

/** Represents a set of x/y coordinates. */
public expect class Point : Closable {
    /** The horizontal position. */
    public var xPos: Short
    /** The vertical position. */
    public var yPos: Short

    public companion object {
        /**
         * Creates a new [Point].
         * @return The new [Point] instance.
         */
        public fun create(): Point
    }
}

public fun point(newXPos: Short, newYPos: Short): Point = Point.create().apply {
    xPos = newXPos
    yPos = newYPos
}
