package io.gitlab.embedSoft.lvglKt.core

public interface Closable {
    public fun close()
}
