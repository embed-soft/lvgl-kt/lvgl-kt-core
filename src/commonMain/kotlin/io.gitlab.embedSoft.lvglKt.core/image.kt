package io.gitlab.embedSoft.lvglKt.core

import io.gitlab.embedSoft.lvglKt.core.lvglObject.LvglObjectBase

public expect class Image : LvglObjectBase {
    /**
     * Set the image data to display.
     * @param src A path to the image file or a symbol (eg `LV_SYMBOL_OK`).
     */
    public fun setSrc(src: String)

    /**
     * Set the image data to display.
     * @param src The image descriptor to use as the source.
     */
    public fun setSrc(src: ImageDescriptor)

    /**
     * Gets the image data as a [String].
     * @return The image data.
     */
    public fun getSrcAsString(): String

    public companion object {
        /**
         * Creates a new [Image] that has a [parent].
         * @param parent The parent of the image (eg a screen).
         * @return A new [Image] instance.
         */
        public fun create(parent: LvglObjectBase): Image
    }
}
