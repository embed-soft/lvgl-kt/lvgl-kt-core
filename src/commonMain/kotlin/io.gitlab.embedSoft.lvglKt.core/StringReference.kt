package io.gitlab.embedSoft.lvglKt.core

/** Provides a reference to a Kotlin [String], which can be cleaned up later on. */
public expect class StringReference : Closable {
    /** The Kotlin [String]. */
    public val str: String

    public companion object {
        /**
         * Creates a new [StringReference]
         * @param value The [String] value to use.
         * @return The new [StringReference] instance.
         */
        public fun create(value: String): StringReference
    }
}
