package io.gitlab.embedSoft.lvglKt.core.styling

import io.gitlab.embedSoft.lvglKt.core.StringReference
import io.gitlab.embedSoft.lvglKt.core.layout.FlexAlignment
import io.gitlab.embedSoft.lvglKt.core.lvglObject.LvglObjectBase

/** Get the radius on every corner. */
public expect fun LvglObjectBase.getRadiusStyle(part: UInt): Short

/** Set the radius on every corner. */
public expect fun LvglObjectBase.setRadiusStyle(value: Short, selector: UInt = 0u)

/** Get the width of the border. */
public expect fun LvglObjectBase.getBorderWidthStyle(part: UInt): Short

/** Set the width of the border. */
public expect fun LvglObjectBase.setBorderWidthStyle(value: Short, selector: UInt = 0u)

/** Set if the points of the arcs rounded or not. */
public expect fun LvglObjectBase.setArcRoundedStyle(value: Boolean, selector: UInt = 0u)

/** Get whether the end points of the arcs are rounded or not. */
public expect fun LvglObjectBase.getArcRoundedStyle(part: UInt): Boolean

/** Set the color of the arc. */
public expect fun LvglObjectBase.setArcColorStyle(value: Color, selector: UInt = 0u)

/** Get the color of the arc. */
public expect fun LvglObjectBase.getArcColorStyle(part: UInt): Color

/** Set the width (thickness) of the arcs in pixels. */
public expect fun LvglObjectBase.setArcWidthStyle(value: Short, selector: UInt = 0u)

/** Get the width (thickness) of the arcs in pixels. */
public expect fun LvglObjectBase.getArcWidthStyle(part: UInt): Short

/** Set the opacity of the arcs. */
public expect fun LvglObjectBase.setArcOpacityStyle(value: UByte, selector: UInt = 0u)

/** Get the opacity of the arcs. */
public expect fun LvglObjectBase.getArcOpacityStyle(part: UInt): UByte

/** Gets the arc color filter. */
public expect fun LvglObjectBase.getArcColorFilteredStyle(part: UInt): Color

/** Set the color of the border. */
public expect fun LvglObjectBase.setBorderColorStyle(value: Color, selector: UInt = 0u)

/** Get the color of the border. */
public expect fun LvglObjectBase.getBorderColorStyle(part: UInt): Color

/** Sets whether the border should be drawn **before** or **after** the children are drawn. */
public expect fun LvglObjectBase.setBorderPostStyle(value: Boolean, selector: UInt = 0u)

/** Get whether the border should be drawn **before** or **after** the children are drawn. */
public expect fun LvglObjectBase.getBorderPostStyle(part: UInt): Boolean

/** Get the border color filter. */
public expect fun LvglObjectBase.getBorderColorFilteredStyle(part: UInt): Color

/** Set the alignment which determines from which point of the parent the X and Y coordinates should be interpreted. */
public expect fun LvglObjectBase.setAlignStyle(value: UByte, selector: UInt = 0u)

/** Get the alignment which determines from which point of the parent the X and Y coordinates should be interpreted. */
public expect fun LvglObjectBase.getAlignStyle(part: UInt): UByte

/** Sets the width of the LVGL object. Pixel, percentage and `LV_SIZE_CONTENT` values can be used. */
public expect fun LvglObjectBase.setWidthStyle(value: Short, selector: UInt = 0u)

/** Get the width of the LVGL object. */
public expect fun LvglObjectBase.getWidthStyle(part: UInt): Short

/**
 * Sets the minimum width. Pixel and percentage values can be used. Percentage values are relative to the width of
 * the parent's content area.
 */
public expect fun LvglObjectBase.setMinWidthStyle(value: Short, selector: UInt = 0u)

/** Gets the minimum width. */
public expect fun LvglObjectBase.getMinWidthStyle(part: UInt): Short

/**
 * Sets the maximum width. Pixel and percentage values can be used. Percentage values are relative to the width of
 * the parent's content area.
 */
public expect fun LvglObjectBase.setMaxWidthStyle(value: Short, selector: UInt = 0u)

/** Get the maximum width. */
public expect fun LvglObjectBase.getMaxWidthStyle(part: UInt): Short

/**
 * Sets the height of the LVGL object. Pixel, percentage and `LV_SIZE_CONTENT` can be used. Percentage values are
 * relative to the height of the parent's content area.
 */
public expect fun LvglObjectBase.setHeightStyle(value: Short, selector: UInt = 0u)

/** Sets the height of the LVGL object. */
public expect fun LvglObjectBase.getHeightStyle(part: UInt): Short

/**
 * Sets the minimum height. Pixel and percentage values can be used. Percentage values are relative to the width
 * of the parent's content area.
 */
public expect fun LvglObjectBase.setMinHeightStyle(value: Short, selector: UInt = 0u)

/** Get the minimum height. */
public expect fun LvglObjectBase.getMinHeightStyle(part: UInt): Short

/**
 * Sets the maximum height. Pixel and percentage values can be used. Percentage values are relative to the height
 * of the parent's content area.
 */
public expect fun LvglObjectBase.setMaxHeightStyle(value: Short, selector: UInt = 0u)

/** Get the maximum height. */
public expect fun LvglObjectBase.getMaxHeightStyle(part: UInt): Short

/**
 * Set the X coordinate of the LVGL object considering the set align. Pixel and percentage values can be used.
 * Percentage values are relative to the width of the parent's content area.
 */
public expect fun LvglObjectBase.setXPosStyle(value: Short, selector: UInt = 0u)

/** Get the X coordinate of the LVGL object considering the set align. */
public expect fun LvglObjectBase.getXPosStyle(part: UInt): Short

/**
 * Set the Y coordinate of the LVGL object considering the set align. Pixel and percentage values can be used.
 * Percentage values are relative to the height of the parent's content area.
 */
public expect fun LvglObjectBase.setYPosStyle(value: Short, selector: UInt = 0u)

/** Get the Y coordinate of the LVGL object considering the set align. */
public expect fun LvglObjectBase.getYPosStyle(part: UInt): Short

/**
 * Make the object wider on both sides with this value. Pixel and percentage (with `lv_pct(x)`) values can be
 * used. Percentage values are relative to the object's width.
 */
public expect fun LvglObjectBase.setTransformWidthStyle(value: Short, selector: UInt = 0u)

/** Get the transform width. */
public expect fun LvglObjectBase.getTransformWidthStyle(part: UInt): Short

/**
 * Make the object higher on both sides with this value. Pixel and percentage (with `lv_pct(x)`) values can be
 * used. Percentage values are relative to the object's height.
 */
public expect fun LvglObjectBase.setTransformHeightStyle(value: Short, selector: UInt = 0u)

/** Get the transform height. */
public expect fun LvglObjectBase.getTransformHeightStyle(part: UInt): Short

/**
 * Move the object with this value in X direction. Applied after layouts, aligns and other positioning. Pixel and
 * percentage (with `lv_pct(x)`) values can be used. Percentage values are relative to the object's width.
 */
public expect fun LvglObjectBase.setTranslateXStyle(value: Short, selector: UInt = 0u)

/** Get the X translation. */
public expect fun LvglObjectBase.getTranslateXStyle(part: UInt): Short

/**
 * Move the object with this value in Y direction. Applied after layouts, aligns and other positioning. Pixel and
 * percentage (with `lv_pct(x)`) values can be used. Percentage values are relative to the object's height.
 */
public expect fun LvglObjectBase.setTranslateYStyle(value: Short, selector: UInt = 0u)

/** Get the Y translation. */
public expect fun LvglObjectBase.getTranslateYStyle(part: UInt): Short

/**
 * Zoom image-like objects. Multiplied with the zoom set on the object. The value *256* (or `LV_IMG_ZOOM_NONE`)
 * means normal size, *128* half size, *512* double size, and so on.
 */
public expect fun LvglObjectBase.setTransformZoomStyle(value: Short, selector: UInt = 0u)

/** Get the zoom transformation. */
public expect fun LvglObjectBase.getTransformZoomStyle(part: UInt): Short

/**
 * Rotate image-like objects. Added to the rotation set on the object. The value is interpreted in *0.1* degree
 * unit. E.g. `45 deg. = 450`
 */
public expect fun LvglObjectBase.setTransformAngleStyle(value: Short, selector: UInt = 0u)

/** Get the transform angle. */
public expect fun LvglObjectBase.getTransformAngleStyle(part: UInt): Short

/** Sets the padding on all sides. */
public expect fun LvglObjectBase.setPadAllStyle(value: Short, selector: UInt = 0u)

/** Sets the padding on the top. It makes the content area smaller in this direction. */
public expect fun LvglObjectBase.setPadTopStyle(value: Short, selector: UInt = 0u)

/** Get the padding on the top. */
public expect fun LvglObjectBase.getPadTopStyle(part: UInt): Short

/** Sets the padding on the bottom. It makes the content area smaller in this direction. */
public expect fun LvglObjectBase.setPadBottomStyle(value: Short, selector: UInt = 0u)

/** Get the padding on the bottom. */
public expect fun LvglObjectBase.getPadBottomStyle(part: UInt): Short

/** Sets the padding on the left. It makes the content area smaller in this direction. */
public expect fun LvglObjectBase.setPadLeftStyle(value: Short, selector: UInt = 0u)

/** Get the padding on the left. */
public expect fun LvglObjectBase.getPadLeftStyle(part: UInt): Short

/** Sets the padding on the right. It makes the content area smaller in this direction. */
public expect fun LvglObjectBase.setPadRightStyle(value: Short, selector: UInt = 0u)

/** Get the padding on the right. */
public expect fun LvglObjectBase.getPadRightStyle(part: UInt): Short

/** Sets the padding between the rows. Used by the layouts. */
public expect fun LvglObjectBase.setPadRowStyle(value: Short, selector: UInt = 0u)

/** Get the padding between the rows. Used by the layouts. */
public expect fun LvglObjectBase.getPadRowStyle(part: UInt): Short

/** Sets the padding between the columns. Used by the layouts. */
public expect fun LvglObjectBase.setPadColumnStyle(value: Short, selector: UInt = 0u)

/** Get the padding between the columns. Used by the layouts. */
public expect fun LvglObjectBase.getPadColumnStyle(part: UInt): Short

/** Set the background color of the object. */
public expect fun LvglObjectBase.setBackgroundColorStyle(value: Color, selector: UInt = 0u)

/** Get the background color of the object. */
public expect fun LvglObjectBase.getBackgroundColorStyle(part: UInt): Color

/** Set the gradient color of the background. Used only if `grad_dir` isn't `LV_GRAD_DIR_NONE`. */
public expect fun LvglObjectBase.setBackgroundGradientColorStyle(value: Color, selector: UInt = 0u)

/** Get the gradient color of the background. Used only if `grad_dir` isn't `LV_GRAD_DIR_NONE`. */
public expect fun LvglObjectBase.getBackgroundGradientColorStyle(part: UInt): Color

/** Set the direction of the gradient of the background. The possible values are `LV_GRAD_DIR_NONE/HOR/VER`. */
public expect fun LvglObjectBase.setBackgroundGradientDirectionStyle(value: UByte, selector: UInt = 0u)

/** Get the direction of the gradient of the background. */
public expect fun LvglObjectBase.getBackgroundGradientDirectionStyle(part: UInt): UByte

/** Set the point from which the background color should start for gradients. */
public expect fun LvglObjectBase.setBackgroundMainStopStyle(value: Short, selector: UInt = 0u)

/** Get the point from which the background color should start for gradients. */
public expect fun LvglObjectBase.getBackgroundMainStopStyle(part: UInt): Short

/** Set the point from which the background's gradient color should start. */
public expect fun LvglObjectBase.setBackgroundGradientStopStyle(value: Short, selector: UInt = 0u)

/** Get the point from which the background's gradient color should start. */
public expect fun LvglObjectBase.getBackgroundGradientStopStyle(part: UInt): Short

/** Set a color to mix to the background image. */
public expect fun LvglObjectBase.setBackgroundImageRecolorStyle(value: Color, selector: UInt = 0u)

/** Set a color to mix to the background image. */
public expect fun LvglObjectBase.getBackgroundImageRecolorStyle(part: UInt): Color

/** If enabled the background image will be tiled. */
public expect fun LvglObjectBase.setBackgroundImageTiledStyle(value: Boolean, selector: UInt = 0u)

/** Whether the background image will be tiled. */
public expect fun LvglObjectBase.getBackgroundImageTiledStyle(part: UInt): Boolean

/**
 * Set which side(s) the border should be drawn. The possible values are
 * `LV_BORDER_SIDE_NONE/TOP/BOTTOM/LEFT/RIGHT/INTERNAL`. Note that OR-ed values can be used as well, e.g.
 * `LV_BORDER_SIDE_TOP | LV_BORDER_SIDE_LEFT`.
 */
public expect fun LvglObjectBase.setBorderSideStyle(value: UByte, selector: UInt = 0u)

/** Get which side(s) the border should be drawn. */
public expect fun LvglObjectBase.getBorderSideStyle(part: UInt): UByte

/** Set the color of the text. */
public expect fun LvglObjectBase.setTextColorStyle(value: Color, selector: UInt = 0u)

/** Get the color of the text. */
public expect fun LvglObjectBase.getTextColorStyle(part: UInt): Color

/** Set the letter space. */
public expect fun LvglObjectBase.setTextLetterSpaceStyle(value: Short, selector: UInt = 0u)

/** Get the letter space. */
public expect fun LvglObjectBase.getTextLetterSpaceStyle(part: UInt): Short

/** Set the line space. */
public expect fun LvglObjectBase.setTextLineSpaceStyle(value: Short, selector: UInt = 0u)

/** Get the line space. */
public expect fun LvglObjectBase.getTextLineSpaceStyle(part: UInt): Short

/**
 * Set decoration for the text. The possible values are `LV_TEXT_DECOR_NONE/UNDERLINE/STRIKETHROUGH`. Note that
 * OR-ed values can be used as well.
 */
public expect fun LvglObjectBase.setTextDecorStyle(value: UByte, selector: UInt = 0u)

/** Get decoration for the text. */
public expect fun LvglObjectBase.getTextDecorStyle(part: UInt): UByte

/**
 * Set how to align the lines of the text. Note that it doesn't align the object itself, only the lines inside the
 * object. The possible values are `LEFT/CENTER/RIGHT/AUTO`. Use [TextAlignment.AUTO] to detect the text base direction
 * and use left or right alignment accordingly.
 */
public expect fun LvglObjectBase.setTextAlignStyle(value: TextAlignment, selector: UInt = 0u)

/** Get how to align the lines of the text. */
public expect fun LvglObjectBase.getTextAlignStyle(part: UInt): TextAlignment

/**
 * Set the opacity of an image. Value *0*, `LV_OPA_0` or `LV_OPA_TRANSP` means fully transparent, *256*,
 * `LV_OPA_100` or `LV_OPA_COVER` means fully covering, other values or `LV_OPA_10`, `LV_OPA_20`, etc indicate
 * semi-transparency.
 */
public expect fun LvglObjectBase.setImageOpacityStyle(value: UByte, selector: UInt = 0u)

/** Get the opacity of an image. */
public expect fun LvglObjectBase.getImageOpacityStyle(part: UInt): UByte

/** Set color to mix to the image. */
public expect fun LvglObjectBase.setImageRecolorStyle(value: Color, selector: UInt = 0u)

/** Get color to mix to the image. */
public expect fun LvglObjectBase.getImageRecolorStyle(part: UInt): Color

/**
 * Set the intensity of the color mixing. Value *0*, `LV_OPA_0` or `LV_OPA_TRANSP` means fully transparent, *256*,
 * `LV_OPA_100` or `LV_OPA_COVER` means fully covering, other values or `LV_OPA_10`, `LV_OPA_20`, etc indicate
 * semi-transparency.
 */
public expect fun LvglObjectBase.setImageRecolorOpaStyle(value: UByte, selector: UInt = 0u)

/** Get the intensity of the color mixing. */
public expect fun LvglObjectBase.getImageRecolorOpaStyle(part: UInt): UByte

/** Set the width of the outline in pixels. */
public expect fun LvglObjectBase.setOutlineWidthStyle(value: Short, selector: UInt = 0u)

/** Get the width of the outline in pixels. */
public expect fun LvglObjectBase.getOutlineWidthStyle(part: UInt): Short

/** Set the color of the outline. */
public expect fun LvglObjectBase.setOutlineColorStyle(value: Color, selector: UInt = 0u)

/** Get the color of the outline. */
public expect fun LvglObjectBase.getOutlineColorStyle(part: UInt): Color

/**
 * Set the opacity of the outline. Value *0*, `LV_OPA_0` or `LV_OPA_TRANSP` means fully transparent, *256*,
 * `LV_OPA_100` or `LV_OPA_COVER` means fully covering, other values or `LV_OPA_10`, `LV_OPA_20`, etc indicate
 * semi-transparency.
 */
public expect fun LvglObjectBase.setOutlineOpacityStyle(value: UByte, selector: UInt = 0u)

/** Get the opacity of the outline. */
public expect fun LvglObjectBase.getOutlineOpacityStyle(part: UInt): UByte

/** Set the padding of the outline, i.e. the gap between object and the outline. */
public expect fun LvglObjectBase.setOutlinePadStyle(value: Short, selector: UInt = 0u)

/** Get the padding of the outline, i.e. the gap between object and the outline. */
public expect fun LvglObjectBase.getOutlinePadStyle(part: UInt): Short

/** Set the width of the shadow in pixels. The value should be >= *0*. */
public expect fun LvglObjectBase.setShadowWidthStyle(value: Short, selector: UInt = 0u)

/** Get the width of the shadow in pixels. */
public expect fun LvglObjectBase.getShadowWidthStyle(part: UInt): Short

/** Set an offset on the shadow in pixels in X direction. */
public expect fun LvglObjectBase.setShadowXOffsetStyle(value: Short, selector: UInt = 0u)

/** Get an offset on the shadow in pixels in X direction. */
public expect fun LvglObjectBase.getShadowXOffsetStyle(part: UInt): Short

/** Set an offset on the shadow in pixels in Y direction. */
public expect fun LvglObjectBase.setShadowYOffsetStyle(value: Short, selector: UInt = 0u)

/** Get an offset on the shadow in pixels in Y direction. */
public expect fun LvglObjectBase.getShadowYOffsetStyle(part: UInt): Short

/**
 * Make the shadow calculation to use a larger or smaller rectangle as base. The value can be in pixel to make the
 * area larger/smaller.
 */
public expect fun LvglObjectBase.setShadowSpreadStyle(value: Short, selector: UInt = 0u)

/** Get the shadow calculation to use a larger or smaller rectangle as base. */
public expect fun LvglObjectBase.getShadowSpreadStyle(part: UInt): Short

/** Set the color of the shadow. */
public expect fun LvglObjectBase.setShadowColorStyle(value: Color, selector: UInt = 0u)

/** Get the color of the shadow. */
public expect fun LvglObjectBase.getShadowColorStyle(part: UInt): Color

/**
 * Set the opacity of the shadow. Value *0*, `LV_OPA_0` or `LV_OPA_TRANSP` means fully transparent, *256*,
 * `LV_OPA_100` or `LV_OPA_COVER` means fully covering, other values or `LV_OPA_10`, `LV_OPA_20`, etc indicate
 * semi-transparency.
 */
public expect fun LvglObjectBase.setShadowOpacityStyle(value: UByte, selector: UInt = 0u)

/** Get the opacity of the shadow. */
public expect fun LvglObjectBase.getShadowOpacityStyle(part: UInt): UByte

/** Set the width of the lines in pixels. */
public expect fun LvglObjectBase.setLineWidthStyle(value: Short, selector: UInt = 0u)

/** Get the width of the lines in pixels. */
public expect fun LvglObjectBase.getLineWidthStyle(part: UInt): Short

/** Set the width of dashes in pixels. Note that dash works only on horizontal and vertical lines. */
public expect fun LvglObjectBase.setLineDashWidthStyle(value: Short, selector: UInt = 0u)

/** Get the width of dashes in pixels. */
public expect fun LvglObjectBase.getLineDashWidthStyle(part: UInt): Short

/** Set the gap between dashes in pixels. Note that dash works only on horizontal and vertical lines. */
public expect fun LvglObjectBase.setLineDashGapStyle(value: Short, selector: UInt = 0u)

/** Get the gap between dashes in pixels. */
public expect fun LvglObjectBase.getLineDashGapStyle(part: UInt): Short

/** Make the end points of the lines rounded. */
public expect fun LvglObjectBase.setLineRoundedStyle(value: Boolean, selector: UInt = 0u)

/** Whether end points of the lines are rounded. */
public expect fun LvglObjectBase.getLineRoundedStyle(part: UInt): Boolean

/** Set the color of the lines. */
public expect fun LvglObjectBase.setLineColorStyle(value: Color, selector: UInt = 0u)

/** Get the color of the lines. */
public expect fun LvglObjectBase.getLineColorStyle(part: UInt): Color

/** Set the opacity of the lines. */
public expect fun LvglObjectBase.setLineOpacityStyle(value: UByte, selector: UInt = 0u)

/** Get the opacity of the lines. */
public expect fun LvglObjectBase.getLineOpacityStyle(part: UInt): UByte

/**
 * Scale down all opacity values of the object by this factor. Value *0*, `LV_OPA_0` or `LV_OPA_TRANSP` means fully
 * transparent, *256*, `LV_OPA_100` or `LV_OPA_COVER` means fully covering, other values or `LV_OPA_10`,
 * `LV_OPA_20`, etc indicate semi-transparency.
 */
public expect fun LvglObjectBase.setOpacityStyle(value: UByte, selector: UInt = 0u)

/** Get the opacity of the object. */
public expect fun LvglObjectBase.getOpacityStyle(part: UInt): UByte

/** Set the intensity of mixing of color filter. */
public expect fun LvglObjectBase.setColorFilterOpacityStyle(value: UByte, selector: UInt = 0u)

/** Get the intensity of mixing of color filter. */
public expect fun LvglObjectBase.getColorFilterOpacityStyle(part: UInt): UByte

/**
 * Set the opacity of the background. Value *0*, `LV_OPA_0` or `LV_OPA_TRANSP` means fully transparent, *256*,
 * `LV_OPA_100` or `LV_OPA_COVER` means fully covering, other values or `LV_OPA_10`, `LV_OPA_20`, etc indicate
 * semi-transparency.
 */
public expect fun LvglObjectBase.setBackgroundOpacityStyle(value: UByte, selector: UInt = 0u)

/** Get the opacity of the background. */
public expect fun LvglObjectBase.getBackgroundOpacityStyle(part: UInt): UByte

/**
 * Set the opacity of the background image. Value *0*, `LV_OPA_0` or `LV_OPA_TRANSP` means fully transparent,
 * *256*, `LV_OPA_100` or `LV_OPA_COVER` means fully covering, other values or `LV_OPA_10`, `LV_OPA_20`, etc
 * indicate semi-transparency.
 */
public expect fun LvglObjectBase.setBackgroundImageOpacityStyle(value: UByte, selector: UInt = 0u)

/** Get the opacity of the background image. */
public expect fun LvglObjectBase.getBackgroundImageOpacityStyle(part: UInt): UByte

/**
 * Set the intensity of background image recoloring. Value *0*, `LV_OPA_0` or `LV_OPA_TRANSP` means no mixing,
 * *256*, `LV_OPA_100` or `LV_OPA_COVER` means full recoloring, other values or `LV_OPA_10`, `LV_OPA_20`, etc are
 * interpreted proportionally.
 */
public expect fun LvglObjectBase.setBackgroundImageRecolorOpacityStyle(value: UByte, selector: UInt = 0u)

/** Get the intensity of background image recoloring. */
public expect fun LvglObjectBase.getBackgroundImageRecolorOpacityStyle(part: UInt): UByte

/**
 * Set the opacity of the border. Value *0*, `LV_OPA_0` or `LV_OPA_TRANSP` means fully transparent, *256*,
 * `LV_OPA_100` or `LV_OPA_COVER` means fully covering, other values or `LV_OPA_10`, `LV_OPA_20`, etc indicate
 * semi-transparency.
 */
public expect fun LvglObjectBase.setBorderOpacityStyle(value: UByte, selector: UInt = 0u)

/** Get the opacity of the border. */
public expect fun LvglObjectBase.getBorderOpacityStyle(part: UInt): UByte

/**
 * Set the opacity of the text. Value *0*, `LV_OPA_0` or `LV_OPA_TRANSP` means fully transparent, *256*,
 * `LV_OPA_100` or `LV_OPA_COVER` means fully covering, other values or `LV_OPA_10`, `LV_OPA_20`, etc indicate
 * semi-transparency.
 */
public expect fun LvglObjectBase.setTextOpacityStyle(value: UByte, selector: UInt = 0u)

/** Get the opacity of the text. */
public expect fun LvglObjectBase.getTextOpacityStyle(part: UInt): UByte

/** Set the layout used as part of the object styling. */
public expect fun LvglObjectBase.setLayoutStyle(value: UShort, selector: UInt = 0u)

/** Get the layout used as part of the object styling. */
public expect fun LvglObjectBase.getLayoutStyle(part: UInt): UShort

/**
 * Sets up the Flexbox layout style on the object. All possible values for this function include the following:
 * - `LV_FLEX_FLOW_ROW` : Place the children in a row without wrapping
 * - `LV_FLEX_FLOW_COLUMN` : Place the children in a column without wrapping
 * - `LV_FLEX_FLOW_ROW_WRAP` Place the children in a row with wrapping
 * - `LV_FLEX_FLOW_COLUMN_WRAP` : Place the children in a column with wrapping
 * - `LV_FLEX_FLOW_ROW_REVERSE` : Place the children in a row without wrapping but in reversed order
 * - `LV_FLEX_FLOW_COLUMN_REVERSE` : Place the children in a column without wrapping but in reversed order
 * - `LV_FLEX_FLOW_ROW_WRAP_REVERSE` : Place the children in a row without wrapping but in reversed order
 * - `LV_FLEX_FLOW_COLUMN_WRAP_REVERSE` : Place the children in a column without wrapping but in reversed order
 * @see setLayoutStyle
 */
public expect fun LvglObjectBase.setFlexFlowStyle(value: UInt, selector: UInt = 0u)

/** Get the Flexbox layout setup style on the object. */
public expect fun LvglObjectBase.getFlexFlowStyle(part: UInt): UInt

/**
 * Used to make one or more children fill the available space on the track. If more children has grow the
 * available space will be distributed proportionally to the grow values. For example, let's say there is *400*
 * pixels remaining space and *4* objects with grow:
 * - `A` with grow = 1
 * - `B` with grow = 1
 * - `C` with grow = 2
 *
 * With the above example `A` and `B` will have *100* pixel size, and `C` will have *200* pixel size. Flex grow
 * can be set on a child with [setFlexFlowStyle], where the **flow** parameter needs to be > *1* or *0* to disable grow
 * on the child.
 */
public expect fun LvglObjectBase.setFlexGrowStyle(value: UByte, selector: UInt = 0u)

/** Get the Flexbox grow on the object. */
public expect fun LvglObjectBase.getFlexGrowStyle(part: UInt): UByte

/** Set a background image from a [String]. */
public expect fun LvglObjectBase.setBackgroundImageSourceStyle(value: String, selector: UInt = 0u): StringReference

/** Set a background image from a [StringReference]. */
public expect fun LvglObjectBase.setBackgroundImageSourceStyle(value: StringReference, selector: UInt = 0u)

/** Set an image from which the arc will be masked out. It's useful to display complex effects on the arcs. */
public expect fun LvglObjectBase.setArcImageSourceStyle(value: StringReference, selector: UInt = 0u)

/** Set an image from which the arc will be masked out. It's useful to display complex effects on the arcs. */
public expect fun LvglObjectBase.setArcImageSourceStyle(value: String, selector: UInt = 0u): StringReference

/** Set the Flexbox cross place. */
public expect fun LvglObjectBase.setFlexCrossPlaceStyle(value: FlexAlignment, selector: UInt)

/** Set the Flexbox track place. */
public expect fun LvglObjectBase.setFlexTrackPlaceStyle(value: FlexAlignment, selector: UInt)

/** Set the Flexbox main place. */
public expect fun LvglObjectBase.setFlexMainPlaceStyle(value: FlexAlignment, selector: UInt)

/** Get the Flexbox cross place. */
public expect fun LvglObjectBase.getFlexCrossPlaceStyle(part: UInt): FlexAlignment

/** Get the Flexbox track place. */
public expect fun LvglObjectBase.getFlexTrackPlaceStyle(part: UInt): FlexAlignment

/** Get the Flexbox main place. */
public expect fun LvglObjectBase.getFlexMainPlaceStyle(part: UInt): FlexAlignment

/** Set the text font. */
public expect fun LvglObjectBase.setTextFont(value: Font, selector: UInt = 0u)

/** Get the text font. */
public expect fun LvglObjectBase.getTextFont(part: UInt): Font
