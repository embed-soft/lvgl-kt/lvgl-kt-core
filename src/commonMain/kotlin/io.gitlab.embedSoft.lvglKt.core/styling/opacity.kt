package io.gitlab.embedSoft.lvglKt.core.styling

/** Represents opacity as a percentage. */
public enum class Opacity(public val value: UByte) {
    ZERO(0u.toUByte()),
    TEN(25u.toUByte()),
    TWENTY(51u.toUByte()),
    THIRTY(76u.toUByte()),
    FORTY(102u.toUByte()),
    FIFTY(127u.toUByte()),
    SIXTY(153u.toUByte()),
    SEVENTY(178u.toUByte()),
    EIGHTY(204u.toUByte()),
    NINETY(229u.toUByte()),
    ONE_HUNDRED(255u.toUByte())
}

public fun UByte.toOpacity(): Opacity = when (this) {
    0u.toUByte() -> Opacity.ZERO
    25u.toUByte() -> Opacity.TEN
    51u.toUByte() -> Opacity.TWENTY
    76u.toUByte() -> Opacity.THIRTY
    102u.toUByte() -> Opacity.FORTY
    127u.toUByte() -> Opacity.FIFTY
    153u.toUByte() -> Opacity.SIXTY
    178u.toUByte() -> Opacity.SEVENTY
    204u.toUByte() -> Opacity.EIGHTY
    229u.toUByte() -> Opacity.NINETY
    255u.toUByte() -> Opacity.ONE_HUNDRED
    else -> throw IllegalStateException("Unrecognized value")
}
