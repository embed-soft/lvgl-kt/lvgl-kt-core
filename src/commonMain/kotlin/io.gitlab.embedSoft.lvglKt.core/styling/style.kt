package io.gitlab.embedSoft.lvglKt.core.styling

import io.gitlab.embedSoft.lvglKt.core.Closable
import io.gitlab.embedSoft.lvglKt.core.StringReference
import io.gitlab.embedSoft.lvglKt.core.layout.FlexAlignment

/** Provides a style that can be applied to multiple LVGL objects. */
public expect class Style : Closable {
    /** Checks if a style is empty (has no properties). */
    public val isEmpty: Boolean

    /**
     * Set the radius on every corner.
     * @param value Interpreted in pixels (`>= 0`) or `LV_RADIUS_CIRCLE` for max radius.
     */
    public infix fun setRadius(value: Short)

    /**
     * Set the width of the border. Only pixel values can be used.
     * @param value A pixel value.
     */
    public infix fun setBorderWidth(value: Short)

    /**
     * Make the end points of the arcs rounded.
     * @param value If *true* then use rounded, however if *false* then use perpendicular line ending.
     */
    public infix fun setArcRounded(value: Boolean)

    /**
     * Set the color of the arc.
     * @param value The new value to use.
     */
    public infix fun setArcColor(value: Color)

    /**
     * Set the width (thickness) of the arcs in pixel.
     * @param value In pixels.
     */
    public infix fun setArcWidth(value: Short)

    /**
     * Set the opacity of the arcs.
     * @param value The new value to use.
     */
    public infix fun setArcOpacity(value: UByte)

    /**
     * Set the color of the border.
     * @param value The new value to use.
     */
    public infix fun setBorderColor(value: Color)

    /**
     * Sets whether the border should be drawn **before** or **after** the children are drawn.
     * @param value If *true* then draw border **after** children, however if *false* then draw border **before**
     * children.
     */
    public infix fun setBorderPost(value: Boolean)

    /**
     * Set the alignment which determines from which point of the parent the X and Y coordinates should be interpreted.
     * @param value One of the following values:
     * - LV_ALIGN_TOP_LEFT/MID/RIGHT
     * - LV_ALIGN_BOTTOM_LEFT/MID/RIGHT
     * - LV_ALIGN_LEFT/RIGHT_MID
     * - LV_ALIGN_CENTER
     */
    public infix fun setAlign(value: UByte)

    /**
     * Sets the width of the LVGL object. Pixel, percentage and `LV_SIZE_CONTENT` values can be used. Percentage values
     * are relative to the width of the parent's content area.
     * @param value The new value to use.
     */
    public infix fun setWidth(value: Short)

    /**
     * Sets the minimum width. Pixel and percentage values can be used. Percentage values are relative to the width of
     * the parent's content area.
     * @param value The new value to use.
     */
    public infix fun setMinWidth(value: Short)

    /**
     * Sets the maximum width. Pixel and percentage values can be used. Percentage values are relative to the width of
     * the parent's content area.
     * @param value The new value to use.
     */
    public infix fun setMaxWidth(value: Short)

    /**
     * Sets the height of the LVGL object. Pixel, percentage and `LV_SIZE_CONTENT` can be used. Percentage values are
     * relative to the height of the parent's content area.
     * @param value The new value to use.
     */
    public infix fun setHeight(value: Short)

    /**
     * Sets the minimum height. Pixel and percentage values can be used. Percentage values are relative to the width
     * of the parent's content area.
     * @param value The new value to use.
     */
    public infix fun setMinHeight(value: Short)

    /**
     * Sets the maximum height. Pixel and percentage values can be used. Percentage values are relative to the height
     * of the parent's content area.
     * @param value The new value to use.
     */
    public infix fun setMaxHeight(value: Short)

    /**
     * Set the X coordinate of the LVGL object considering the set align. Pixel and percentage values can be used.
     * Percentage values are relative to the width of the parent's content area.
     * @param value The new value to use.
     */
    public infix fun setXPos(value: Short)

    /**
     * Set the Y coordinate of the LVGL object considering the set align. Pixel and percentage values can be used.
     * Percentage values are relative to the height of the parent's content area.
     * @param value The new value to use.
     */
    public infix fun setYPos(value: Short)

    /**
     * Make the object wider on both sides with this value. Pixel and percentage (with `lv_pct(x)`) values can be
     * used. Percentage values are relative to the object's width.
     * @param value The new value to use.
     */
    public infix fun setTransformWidth(value: Short)

    /**
     * Make the object higher on both sides with this value. Pixel and percentage (with `lv_pct(x)`) values can be
     * used. Percentage values are relative to the object's height.
     * @param value The new value to use.
     */
    public infix fun setTransformHeight(value: Short)

    /**
     * Move the object with this value in X direction. Applied after layouts, aligns and other positioning. Pixel and
     * percentage (with `lv_pct(x)`) values can be used. Percentage values are relative to the object's width.
     * @param value The new value to use.
     */
    public infix fun setTranslateX(value: Short)

    /**
     * Move the object with this value in Y direction. Applied after layouts, aligns and other positioning. Pixel and
     * percentage (with `lv_pct(x)`) values can be used. Percentage values are relative to the object's height.
     * @param value The new value to use.
     */
    public infix fun setTranslateY(value: Short)

    /**
     * Zoom image-like objects. Multiplied with the zoom set on the object. The value *256* (or `LV_IMG_ZOOM_NONE`)
     * means normal size, *128* half size, *512* double size, and so on.
     * @param value The new value to use.
     */
    public infix fun setTransformZoom(value: Short)

    /**
     * Rotate image-like objects. Added to the rotation set on the object. The value is interpreted in *0.1* degree
     * unit. E.g. `45 deg. = 450`
     * @param value The new value to use.
     */
    public infix fun setTransformAngle(value: Short)

    /**
     * Sets the padding on all sides.
     * @param value The new value to use.
     */
    public infix fun setPadAll(value: Short)

    /**
     * Sets the padding on the top. It makes the content area smaller in this direction.
     * @param value The new value to use.
     */
    public infix fun setPadTop(value: Short)

    /**
     * Sets the padding on the bottom. It makes the content area smaller in this direction.
     * @param value The new value to use.
     */
    public infix fun setPadBottom(value: Short)

    /**
     * Sets the padding on the left. It makes the content area smaller in this direction.
     * @param value The new value to use.
     */
    public infix fun setPadLeft(value: Short)

    /**
     * Sets the padding on the right. It makes the content area smaller in this direction.
     * @param value The new value to use.
     */
    public infix fun setPadRight(value: Short)

    /**
     * Sets the padding between the rows. Used by the layouts.
     * @param value The new value to use.
     */
    public infix fun setPadRow(value: Short)

    /**
     * Sets the padding between the columns. Used by the layouts.
     * @param value The new value to use.
     */
    public infix fun setPadColumn(value: Short)

    /**
     * Set the background color of the object.
     * @param value The new value to use.
     */
    public infix fun setBackgroundColor(value: Color)

    /**
     * Set the gradient color of the background. Used only if `grad_dir` isn't `LV_GRAD_DIR_NONE`.
     * @param value The new value to use.
     */
    public infix fun setBackgroundGradientColor(value: Color)

    /**
     * Set the direction of the gradient of the background. The possible values are `LV_GRAD_DIR_NONE/HOR/VER`.
     * @param value The new value to use.
     */
    public infix fun setBackgroundGradientDirection(value: UByte)

    /**
     * Set the point from which the background color should start for gradients.
     * @param value Using *0* means to top/left side, *255* the bottom/right side, *128* the center, and so on.
     */
    public infix fun setBackgroundMainStop(value: Short)

    /**
     * Set the point from which the background's gradient color should start.
     * @param value Using *0* means to top/left side, *255* the bottom/right side, *128* the center, and so on.
     */
    public infix fun setBackgroundGradientStop(value: Short)

    /**
     * Set a color to mix to the background image.
     * @param value The new value to use.
     */
    public infix fun setBackgroundImageRecolor(value: Color)

    /**
     * If enabled the background image will be tiled.
     * @param value A value of *true* for tiled.
     */
    public infix fun setBackgroundImageTiled(value: Boolean)

    /**
     * Set which side(s) the border should be drawn. The possible values are
     * `LV_BORDER_SIDE_NONE/TOP/BOTTOM/LEFT/RIGHT/INTERNAL`. Note that OR-ed calues can be used as well, e.g.
     * `LV_BORDER_SIDE_TOP | LV_BORDER_SIDE_LEFT`.
     * @param value The new value to use.
     */
    public infix fun setBorderSide(value: UByte)

    /**
     * Sets the color of the text.
     * @param value The new color to use.
     */
    public infix fun setTextColor(value: Color)

    /**
     * Set the letter space.
     * @param value In pixels.
     */
    public infix fun setTextLetterSpace(value: Short)

    /**
     * Set the line space.
     * @param value In pixels.
     */
    public infix fun setTextLineSpace(value: Short)

    /**
     * Set decoration for the text. The possible values are `LV_TEXT_DECOR_NONE/UNDERLINE/STRIKETHROUGH`. Note that
     * OR-ed values can be used as well.
     * @param value The new value to use.
     */
    public infix fun setTextDecor(value: UByte)

    /**
     * Set how to align the lines of the text. Note that it doesn't align the object itself, only the lines inside the
     * object. The possible values are `LEFT/CENTER/RIGHT/AUTO`. Use [TextAlignment.AUTO] to detect the text base
     * direction and use left or right alignment accordingly.
     * @param value The new value to use.
     */
    public infix fun setTextAlign(value: TextAlignment)

    /**
     * Set the opacity of an image. Value *0*, `LV_OPA_0` or `LV_OPA_TRANSP` means fully transparent, *256*,
     * `LV_OPA_100` or `LV_OPA_COVER` means fully covering, other values or `LV_OPA_10`, `LV_OPA_20`, etc indicate
     * semi-transparency.
     * @param value The new value to use.
     */
    public infix fun setImageOpacity(value: UByte)

    /**
     * Set color to mix to the image.
     * @param value The new value to use.
     */
    public infix fun setImageRecolor(value: Color)

    /**
     * Set the intensity of the color mixing. Value *0*, `LV_OPA_0` or `LV_OPA_TRANSP` means fully transparent, *256*,
     * `LV_OPA_100` or `LV_OPA_COVER` means fully covering, other values or `LV_OPA_10`, `LV_OPA_20`, etc indicate
     * semi-transparency.
     * @param value The new value to use.
     */
    public infix fun setImageRecolorOpa(value: UByte)

    /**
     * Set the width of the outline in pixels.
     * @param value The new value to use.
     */
    public infix fun setOutlineWidth(value: Short)

    /**
     * Set the color of the outline.
     * @param value The new value to use.
     */
    public infix fun setOutlineColor(value: Color)

    /**
     * Set the opacity of the outline. Value *0*, `LV_OPA_0` or `LV_OPA_TRANSP` means fully transparent, *256*,
     * `LV_OPA_100` or `LV_OPA_COVER` means fully covering, other values or `LV_OPA_10`, `LV_OPA_20`, etc indicate
     * semi-transparency.
     * @param value The new value to use.
     */
    public infix fun setOutlineOpacity(value: UByte)

    /**
     * Set the padding of the outline, i.e. the gap between object and the outline.
     * @param value The new value to use.
     */
    public infix fun setOutlinePad(value: Short)

    /**
     * Set the width of the shadow in pixels. The value should be >= *0*.
     * @param value The new value to use.
     */
    public infix fun setShadowWidth(value: Short)

    /**
     * Set an offset on the shadow in pixels in X direction.
     * @param value In pixels.
     */
    public infix fun setShadowXOffset(value: Short)

    /**
     * Set an offset on the shadow in pixels in Y direction.
     * @param value In pixels.
     */
    public infix fun setShadowYOffset(value: Short)

    /**
     * Make the shadow calculation to use a larger or smaller rectangle as base. The value can be in pixel to make the
     * area larger/smaller
     * @param value The new value to use.
     */
    public infix fun setShadowSpread(value: Short)

    /**
     * Set the color of the shadow.
     * @param value The new value to use.
     */
    public infix fun setShadowColor(value: Color)

    /**
     * Set the opacity of the shadow. Value *0*, `LV_OPA_0` or `LV_OPA_TRANSP` means fully transparent, *256*,
     * `LV_OPA_100` or `LV_OPA_COVER` means fully covering, other values or `LV_OPA_10`, `LV_OPA_20`, etc indicate
     * semi-transparency.
     * @param value The new value to use.
     */
    public infix fun setShadowOpacity(value: UByte)

    /**
     * Set the width of the lines in pixel.
     * @param value In pixels.
     */
    public infix fun setLineWidth(value: Short)

    /**
     * Set the width of dashes in pixel. Note that dash works only on horizontal and vertical lines.
     * @param value In pixels.
     */
    public infix fun setLineDashWidth(value: Short)

    /**
     * Set the gap between dashes in pixel. Note that dash works only on horizontal and vertical lines.
     * @param value In pixels.
     */
    public infix fun setLineDashGap(value: Short)

    /**
     * Make the end points of the lines rounded.
     * @param value A value of *true* for rounder, otherwise *false* for perpendicular line ending.
     */
    public infix fun setLineRounded(value: Boolean)

    /**
     * Set the color fo the lines.
     * @param value The new value to use.
     */
    public infix fun setLineColor(value: Color)

    /**
     * Set the opacity of the lines.
     * @param value The new value to use.
     */
    public infix fun setLineOpacity(value: UByte)

    /**
     * Scale down all opacity values of the object by this factor. Value *0*, `LV_OPA_0` or `LV_OPA_TRANSP` means fully
     * transparent, *256*, `LV_OPA_100` or `LV_OPA_COVER` means fully covering, other values or `LV_OPA_10`,
     * `LV_OPA_20`, etc indicate semi-transparency.
     * @param value The new value to use.
     */
    public infix fun setOpacity(value: UByte)

    /**
     * The intensity of mixing of color filter.
     * @param value The new value to use.
     */
    public infix fun setColorFilterOpacity(value: UByte)

    /**
     * Set the opacity of the background. Value *0*, `LV_OPA_0` or `LV_OPA_TRANSP` means fully transparent, *256*,
     * `LV_OPA_100` or `LV_OPA_COVER` means fully covering, other values or `LV_OPA_10`, `LV_OPA_20`, etc indicate
     * semi-transparency.
     * @param value The new value to use.
     */
    public infix fun setBackgroundOpacity(value: UByte)

    /**
     * Set the opacity of the background image. Value *0*, `LV_OPA_0` or `LV_OPA_TRANSP` means fully transparent,
     * *256*, `LV_OPA_100` or `LV_OPA_COVER` means fully covering, other values or `LV_OPA_10`, `LV_OPA_20`, etc
     * indicate semi-transparency.
     * @param value The new value to use.
     */
    public infix fun setBackgroundImageOpacity(value: UByte)

    /**
     * Set the intensity of background image recoloring. Value *0*, `LV_OPA_0` or `LV_OPA_TRANSP` means no mixing,
     * *256*, `LV_OPA_100` or `LV_OPA_COVER` means full recoloring, other values or `LV_OPA_10`, `LV_OPA_20`, etc are
     * interpreted proportionally.
     * @param value The new value to use.
     */
    public infix fun setBackgroundImageRecolorOpacity(value: UByte)

    /**
     * Set the opacity of the border. Value *0*, `LV_OPA_0` or `LV_OPA_TRANSP` means fully transparent, *256*,
     * `LV_OPA_100` or `LV_OPA_COVER` means fully covering, other values or `LV_OPA_10`, `LV_OPA_20`, etc indicate
     * semi-transparency.
     * @param value The new value to use.
     */
    public infix fun setBorderOpacity(value: UByte)

    /**
     * Set the opacity of the text. Value *0*, `LV_OPA_0` or `LV_OPA_TRANSP` means fully transparent, *256*,
     * `LV_OPA_100` or `LV_OPA_COVER` means fully covering, other values or `LV_OPA_10`, `LV_OPA_20`, etc indicate
     * semi-transparency.
     * @param value The new value to use.
     */
    public infix fun setTextOpacity(value: UByte)

    public companion object {
        /**
         * Creates a new [Style].
         * @return A new [Style] instance.
         */
        public fun create(): Style
    }

    /** Clear all properties from a style, and free all allocated memory. */
    public fun reset()

    /**
     * Changes the layout.
     * @param value The layout to use for the object.
     */
    public fun setLayout(value: UShort)

    /**
     * Sets up the Flexbox layout on the object. All possible values for this function include the following:
     * - `LV_FLEX_FLOW_ROW` : Place the children in a row without wrapping
     * - `LV_FLEX_FLOW_COLUMN` : Place the children in a column without wrapping
     * - `LV_FLEX_FLOW_ROW_WRAP` Place the children in a row with wrapping
     * - `LV_FLEX_FLOW_COLUMN_WRAP` : Place the children in a column with wrapping
     * - `LV_FLEX_FLOW_ROW_REVERSE` : Place the children in a row without wrapping but in reversed order
     * - `LV_FLEX_FLOW_COLUMN_REVERSE` : Place the children in a column without wrapping but in reversed order
     * - `LV_FLEX_FLOW_ROW_WRAP_REVERSE` : Place the children in a row without wrapping but in reversed order
     * - `LV_FLEX_FLOW_COLUMN_WRAP_REVERSE` : Place the children in a column without wrapping but in reversed order
     * @param value The type of Flexbox layout to use.
     * @see setLayout
     */
    public fun setFlexFlow(value: UInt)

    /**
     * Used to make one or more children fill the available space on the track. If more children has grow the
     * available space will be distributed proportionally to the grow values. For example, let's say there is *400*
     * pixels remaining space and *4* objects with grow:
     * - `A` with grow = 1
     * - `B` with grow = 1
     * - `C` with grow = 2
     *
     * With the above example `A` and `B` will have *100* pixel size, and `C` will have *200* pixel size. Flex grow
     * can be set on a child with [setFlexFlow], where the **flow** parameter needs to be > *1* or *0* to disable grow
     * on the child.
     * @param value The Flexbox grow policy to use.
     */
    public fun setFlexGrow(value: UByte)

    /**
     * Set a background image from a [String].
     * @param value The path to the image file, or symbol.
     * @return A reference to the [value].
     */
    public fun setBackgroundImageSource(value: String): StringReference

    /**
     * Set a background image from a [StringReference].
     * @param value The path to the image file, or symbol.
     */
    public fun setBackgroundImageSource(value: StringReference)

    /**
     * Set an image from which the arc will be masked out. It's useful to display complex effects on the arcs.
     * @param value The path to the image file.
     */
    public fun setArcImageSource(value: StringReference)

    /**
     * Set an image from which the arc will be masked out. It's useful to display complex effects on the arcs.
     * @param value The path to the image file.
     * @return A reference to the [value].
     */
    public fun setArcImageSource(value: String): StringReference

    /**
     * Changes the Flexbox cross place.
     * @param value The new value to use.
     */
    public fun setFlexCrossPlace(value: FlexAlignment)

    /**
     * Changes the Flexbox main place.
     * @param value The new value to use.
     */
    public fun setFlexMainPlace(value: FlexAlignment)

    /**
     * Changes the Flexbox track place.
     * @param value The new value to use.
     */
    public fun setFlexTrackPlace(value: FlexAlignment)

    /**
     * Changes the text font.
     * @param value The new value to use.
     */
    public fun setTextFont(value: Font)
}

public fun style(init: Style.() -> Unit = {}): Style {
    val result = Style.create()
    result.init()
    return result
}
