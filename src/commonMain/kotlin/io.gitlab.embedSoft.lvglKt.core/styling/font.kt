package io.gitlab.embedSoft.lvglKt.core.styling

import io.gitlab.embedSoft.lvglKt.core.Closable

/** Collections of bitmaps and other information required to render the images of the letters (glyph). */
public expect class Font : Closable {
    public companion object {
        /**
         * Loads the font from a file.
         * @param fontName The file path to the font file.
         * @return The new [Font].
         */
        public fun load(fontName: String): Font

        /**
         * Retrieves the default font.
         * @return The default font.
         */
        public fun getDefault(): Font
    }
}
