package io.gitlab.embedSoft.lvglKt.core.styling

/**
 * Gets the main color from the palette.
 * @param paletteValue The palette color value (eg `Palette.RED.value`).
 * @return A color.
 */
public expect fun mainPaletteColor(paletteValue: UInt): Color

/**
 * Gets the main color from the palette.
 * @param paletteValue The palette color value (eg `Palette.RED`).
 * @return A color.
 */
public expect fun mainPaletteColor(paletteValue: Palette): Color

/**
 * Gets a lighter color from the palette.
 * @param paletteValue The palette color value (eg `Palette.RED.value`).
 * @param level How much to lighten the color.
 * @return A lighter color.
 */
public expect fun lighterPaletteColor(paletteValue: UInt, level: UByte): Color

/**
 * Gets a lighter color from the palette.
 * @param paletteValue The palette color value (eg `Palette.RED`).
 * @param level How much to lighten the color.
 * @return A lighter color.
 */
public expect fun lighterPaletteColor(paletteValue: Palette, level: UByte): Color

/**
 * Gets a darker color from the palette.
 * @param paletteValue The palette color value (eg `Palette.RED.value`).
 * @param level How much to darken the color.
 * @return A darker color.
 */
public expect fun darkerPaletteColor(paletteValue: UInt, level: UByte): Color

/**
 * Gets a darker color from the palette.
 * @param paletteValue The palette color value (eg `Palette.RED`).
 * @param level How much to darken the color.
 * @return A darker color.
 */
public expect fun darkerPaletteColor(paletteValue: Palette, level: UByte): Color

/** Contains all colors that are part of the color palette. */
public enum class Palette(public val value: UInt) {
    RED(0u),
    PINK(1u),
    PURPLE(2u),
    DEEP_PURPLE(3u),
    INDIGO(4u),
    BLUE(5u),
    LIGHT_BLUE(6u),
    CYAN(7u),
    TEAL(8u),
    GREEN(9u),
    LIGHT_GREEN(10u),
    LIME(11u),
    YELLOW(12u),
    AMBER(13u),
    ORANGE(14u),
    DEEP_ORANGE(15u),
    BROWN(16u),
    BLUE_GREY(17u),
    GREY(18u)
}
