package io.gitlab.embedSoft.lvglKt.core.styling

/** The text alignment policy. */
public enum class TextAlignment(public val value: UByte) {
    /** Align text automatically. */
    AUTO(0u.toUByte()),
    /** Align text to left. */
    LEFT(1u.toUByte()),
    /** Align text to center. */
    CENTER(2u.toUByte()),
    /** Align text to right. */
    RIGHT(3u.toUByte())
}

public fun UByte.toTextAlignment(): TextAlignment = when (this) {
    0u.toUByte() -> TextAlignment.AUTO
    1u.toUByte() -> TextAlignment.LEFT
    2u.toUByte() -> TextAlignment.CENTER
    3u.toUByte() -> TextAlignment.RIGHT
    else -> throw IllegalStateException("Unrecognized value.")
}
