package io.gitlab.embedSoft.lvglKt.core.styling

import io.gitlab.embedSoft.lvglKt.core.Closable

/** Represents an RGB color. */
public expect class Color : Closable {
    /** The full RGB (Red, Green, Blue) color value. */
    public var full: UShort
    /** Red colour value. */
    public var red: UShort
    /** Green colour value. */
    public var green: UShort
    /** Blue colour value. */
    public var blue: UShort

    /**
     * Gets a lighter color.
     * @param level How much to lighten the color.
     * @return A lighter color.
     */
    public fun lighten(level: UByte): Color

    /**
     * Gets a darker color.
     * @param level How much to darken the color.
     * @return A darker color.
     */
    public fun darken(level: UByte): Color

    /**
     * Mixes this color with the [other color][otherColor] via a given [ratio].
     * @param otherColor The other color to mix with this color.
     * @param ratio The mix to use.
     * @return A new Color.
     */
    public fun mix(otherColor: Color, ratio: UByte): Color

    /**
     * Mixes this color with the [other color][otherColor] via a given [ratio].
     * @param otherColor The other color to mix with this color.
     * @param ratio The mix to use (eg `Opacity.TEN`).
     * @return A new Color.
     */
    public fun mix(otherColor: Color, ratio: Opacity): Color

    public companion object {
        /**
         * Creates a new [Color].
         * @return The new [Color] instance.
         */
        public fun create(): Color
    }
}

/**
 * Creates a color from RGB (Red, Green, Blue) values.
 * @param red The red color value.
 * @param green The green color value.
 * @param blue The blue color value.
 * @return A new color.
 */
public expect fun colorFromRgb(red: UByte, green: UByte, blue: UByte): Color

/**
 * Creates a color from HSV (Hue, Saturation, Value) values.
 * @param hue The hue color value.
 * @param saturation The saturation color value.
 * @param value The value color value.
 * @return A new color.
 */
public expect fun rgbColorFromHsv(hue: UShort, saturation: UByte, value: UByte): Color

/**
 * Convenience function for obtaining the white color.
 * @return The white color.
 */
public expect fun whiteColor(): Color

/**
 * Convenience function for obtaining the black color.
 * @return The black color.
 */
public expect fun blackColor(): Color
