package io.gitlab.embedSoft.lvglKt.core.display

import io.gitlab.embedSoft.lvglKt.core.Area
import io.gitlab.embedSoft.lvglKt.core.Closable
import io.gitlab.embedSoft.lvglKt.core.styling.Color

/** Represents the display driver. */
public expect class DisplayDriver : Closable {
    /** Horizontal resolution. */
    public var horRes: Short
    /** Vertical resolution. */
    public var vertRes: Short
    /** The drawing buffer. */
    public var drawBuf: DisplayDrawBuffer?
    /** When *true* anti-aliasing is applied to drawing operations. */
    public var antiAliasing: Boolean
    /** Handles the onFlush event when the display is refreshed. */
    public var onFlush: (driver: DisplayDriver, area: Area, buf: Color) -> Unit

    public companion object {
        /**
         * Creates a new [DisplayDriver] that is initialized.
         * @param drawBuf The buffer to use for drawing.
         * @return The new [DisplayDriver].
         */
        public fun create(drawBuf: DisplayDrawBuffer): DisplayDriver
    }
}

public fun displayDriver(drawBuf: DisplayDrawBuffer, init: DisplayDriver.() -> Unit): DisplayDriver {
    val result = DisplayDriver.create(drawBuf)
    result.init()
    return result
}
