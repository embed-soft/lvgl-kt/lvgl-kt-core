package io.gitlab.embedSoft.lvglKt.core.display

import io.gitlab.embedSoft.lvglKt.core.Closable

/** Provides a display buffer for graphics drawing. */
public expect class DisplayDrawBuffer : Closable {
    public companion object {
        /**
         * Creates a new [DisplayDrawBuffer] that is initialized.
         * @param bufSize The size of the buffer. Usually calculated using a screen resolution, eg `1024x768`
         * @param dblBuffer If *true* then double buffering is used.
         * @return A new [DisplayDrawBuffer].
         */
        public fun create(bufSize: UInt, dblBuffer: Boolean = false): DisplayDrawBuffer
    }
}
