package io.gitlab.embedSoft.lvglKt.core.animation

import io.gitlab.embedSoft.lvglKt.core.Closable

/**
 * Represents an animation that can be applied to a
 * [LVGL Object][io.gitlab.embedSoft.lvglKt.core.lvglObject.LvglObjectBase].
 */
public expect class Animation : Closable {
    /** Delay before starting the animation in milliseconds. Use *0u* to disable. */
    public var delay: UInt
    /** Duration of the animation in milliseconds. */
    public var time: UInt
    /** The duration of the playback animation in milliseconds. Use *0u* to disable playback. */
    public var playbackTime: UInt
    /** Delay in milliseconds before starting the playback animation. */
    public var playbackDelay: UInt
    /** Repeat count or `LV_ANIM_REPEAT_INFINITE` for infinite repetition. Use *0* to disable repetition.  */
    public var repeatCount: UShort
    /** Delay in milliseconds before repeating the animation. */
    public var repeatDelay: UInt
    /**
     * If *true* then apply the start value immediately in [start]. Otherwise, if *false* then apply the start value
     * only when delay ms is elapsed, and the animations really starts.
     */
    public var earlyApply: Boolean

    public companion object {
        /** The number of running animations. */
        public val totalRunning: UShort

        /**
         * Creates a new [Animation].
         * @return The new [Animation] instance.
         */
        public fun create(): Animation

        /** Delete all the animations. */
        public fun deleteAll()

        /**
         * Calculate the time of an animation with a given speed and the start and end values.
         * @param speed Speed of animation in unit/sec.
         * @param start Start value of the animation.
         * @param end End value of the animation.
         * @return The required time in milliseconds for the animation with the given parameters.
         */
        public fun speedToTime(speed: UInt, start: Int, end: Int): UInt

        /**
         * Manually refresh the state of the animations. Useful to make the animations running in a blocking process
         * where `lv_timer_handler` can't run for a while. Shouldn't be used directly because it is called in
         * `lv_refr_now`.
         */
        public fun refreshNow()
    }

    /**
     * Calculate the current value of an animation applying linear characteristic.
     * @return The current value to set.
     */
    public fun linearPath(): Int

    /**
     * Calculate the current value of an animation slowing down the start phase.
     * @return The current value to set.
     */
    public fun easeInPath(): Int

    /**
     * Calculate the current value of an animation slowing down the end phase.
     * @return The current value to set.
     */
    public fun easeOutPath(): Int

    /**
     * Calculate the current value of an animation applying an **S** characteristic (cosine).
     * @return The current value to set.
     */
    public fun easeInOutPath(): Int

    /**
     * Calculate the current value of an animation with overshoot at the end.
     * @return The current value to set.
     */
    public fun overshootPath(): Int

    /**
     * Calculate the current value of an animation with 3 bounces.
     * @return The current value to set.
     */
    public fun bouncePath(): Int

    /**
     * Calculate the current value of an animation applying step characteristic. Set end value on the end of the
     * animation.
     * @return The current value to set.
     */
    public fun stepPath(): Int

    /**
     * Set the start and end values of an animation.
     * @param start The start value.
     * @param end The end value.
     */
    public fun setValues(start: Int, end: Int)

    /**
     * Create an animation.
     * @return A new [Animation].
     */
    public fun start(): Animation

    /**
     * Changes the callbacks used by this animation through [registry].
     * @param registry The animation callback registry to use that contains the callbacks.
     */
    public fun changeCallbacks(registry: AnimationCallbackRegistry)
}

/**
 * Creates a new animation that is initialized.
 * @param time Duration of the animation in ms.
 * @param start The start value
 * @param end The end value.
 * @param init The animation initializer.
 * @return A new [Animation] instance.
 */
public fun animation(time: UInt, start: Int, end: Int, init: Animation.() -> Unit): Animation {
    val result = Animation.create()
    result.time = time
    result.setValues(start = start, end = end)
    result.init()
    return result
}
