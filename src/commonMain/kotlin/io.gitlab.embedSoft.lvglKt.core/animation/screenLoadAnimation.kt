package io.gitlab.embedSoft.lvglKt.core.animation

/** Provides all the animations that can be used with screen loading. */
public enum class ScreenLoadAnimation {
    NONE,
    OVER_LEFT,
    OVER_RIGHT,
    OVER_TOP,
    OVER_BOTTOM,
    MOVE_LEFT,
    MOVE_RIGHT,
    MOVE_TOP,
    MOVE_BOTTOM,
    /** Fade in screen load animation. Replaces `FADE_ON`. */
    FADE_IN,
    FADE_OUT,
    OUT_LEFT,
    OUT_RIGHT,
    OUT_TOP,
    OUT_BOTTOM
}

public fun ScreenLoadAnimation.toLvScrLoadAnim(): UInt = ordinal.toUInt()
