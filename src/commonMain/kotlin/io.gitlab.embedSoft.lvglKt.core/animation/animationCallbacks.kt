package io.gitlab.embedSoft.lvglKt.core.animation

import io.gitlab.embedSoft.lvglKt.core.Closable

/** Registers animation callbacks. */
public expect class AnimationCallbackRegistry : Closable {
    public companion object {
        /**
         * Creates a new [AnimationCallbackRegistry] object.
         * @param execCallback The animation execute callback to use.
         * @param pathCallback The animation path callback to use.
         * @param readyCallback The animation ready callback to use.
         * @param startCallback The animation start callback to use.
         * @param getValueCallback The animation get value callback to use.
         * @param userData User data to pass to the callbacks. Will be *null* if empty.
         * @return A new [AnimationCallbackRegistry].
         */
        public fun create(
            execCallback: AnimationExecuteCallback,
            pathCallback: AnimationPathCallback = { _, _ -> 0 },
            readyCallback: AnimationReadyCallback = { _, _ -> },
            startCallback: AnimationStartCallback = { _, _ -> },
            getValueCallback: AnimationGetValueCallback = { _, _ -> 0 },
            userData: Any? = null
        ): AnimationCallbackRegistry
    }
}

/**
 * Generic prototype of **animator** functions. First parameter is the animation. Second parameter is the value to set.
 * Compatible with `lv_xxx_set_yyy(obj, value)` functions.
 */
public typealias AnimationExecuteCallback = (animation: Animation, value: Int, userData: Any?) -> Unit

/** Get the current value during an animation. */
public typealias AnimationPathCallback = (animation: Animation, userData: Any?) -> Int

/** Handles the event where the animation is ready. */
public typealias AnimationReadyCallback = (animation: Animation, userData: Any?) -> Unit

/** Handles the event where the animation really starts (considering delay). */
public typealias AnimationStartCallback = (animation: Animation, userData: Any?) -> Unit

/** Used when the animation values are relative to get the current value. */
public typealias AnimationGetValueCallback = (animation: Animation, userData: Any?) -> Int
