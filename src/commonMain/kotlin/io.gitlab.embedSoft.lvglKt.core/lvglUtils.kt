package io.gitlab.embedSoft.lvglKt.core

import io.gitlab.embedSoft.lvglKt.core.lvglObject.LvglObjectBase

/** Initialize LVGL library. Should be called **before** any other LVGL related function. */
public expect fun initLvgl()

/** De-initialize the LVGL library. */
public expect fun deinitLvgl()

/** Run the main event loop. */
public expect fun runEventLoop()

/** Provides a percentage from a value. */
public expect val Short.percent: Short

/** The top layer for the default display. Typically, used for menu bar, popup etc. */
public expect val topLayer: LvglObjectBase

/**
 * The system layer for the default display. Used to ensure something is always visible like a mouse cursor for
 * example.
 */
public expect val sysLayer: LvglObjectBase

/** Initializes Grid layout, so it can be used with LVGL objects that utilize the layout. */
public expect fun initGrid()
