package io.gitlab.embedSoft.lvglKt.core.concurrency

import io.gitlab.embedSoft.lvglKt.core.Closable

/** Represents a LVGL timer callback. */
public expect class TimerCallback : Closable {
    public companion object {
        /**
         * Creates a new [TimerCallback] object.
         * @param userData User data to pass to the [callback]. Will be *null* if empty.
         * @param callback The callback to use.
         * @return A new [TimerCallback].
         */
        public fun create(userData: Any? = null, callback: (timer: Timer, userData: Any?) -> Unit): TimerCallback
    }
}
