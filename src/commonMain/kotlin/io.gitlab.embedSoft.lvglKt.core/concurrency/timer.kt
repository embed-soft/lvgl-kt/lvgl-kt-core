package io.gitlab.embedSoft.lvglKt.core.concurrency

import io.gitlab.embedSoft.lvglKt.core.Closable

/** Calls a function periodically. */
public expect class Timer : Closable {
    /** Gets the next timer. */
    public val next: Timer
    /** Period in milli seconds. */
    public var period: UInt
    /** The number of times to repeat the timer. */
    public var repeatCount: Int

    public companion object {
        /** The idle time as a percentage for [handler]. */
        public val idle: UByte

        /**
         * Creates a new [Timer].
         * @param callback The callback to use.
         * @param period Period in milli seconds.
         */
        public fun create(callback: TimerCallback, period: UInt): Timer

        /**
         * Call it periodically to handle timers.
         * @return The time till it needs to be run next (in ms).
         */
        public fun handler(): UInt
    }

    /**
     * Changes the timer's enable state.
     * @param enable If *true* then enable the timer.
     */
    public fun setEnable(enable: Boolean)

    /** Pauses the timer. */
    public fun pause()

    /** Makes the timer run on the next call of [handler]. */
    public fun ready()

    /**
     * Resets the period of a timer. It will be called again after the defined period of milliseconds has elapsed.
     */
    public fun reset()

    /** Resumes the timer. */
    public fun resume()
}
