package io.gitlab.embedSoft.lvglKt.core.concurrency

import io.gitlab.embedSoft.lvglKt.core.Closable

/** Represents a LVGL async callback. */
public expect class AsyncCallback : Closable {
    public companion object {
        /**
         * Creates a new [AsyncCallback] object.
         * @param userData User data to pass to the [callback]. Will be *null* if empty.
         * @param callback The callback to use.
         * @return A new [AsyncCallback].
         */
        public fun create(userData: Any? = null, callback: (userData: Any?) -> Unit): AsyncCallback
    }
}
