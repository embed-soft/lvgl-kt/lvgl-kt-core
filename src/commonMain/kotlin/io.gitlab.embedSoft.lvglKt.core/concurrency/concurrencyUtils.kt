package io.gitlab.embedSoft.lvglKt.core.concurrency

/**
 * Makes a non-blocking call with a [callback].
 * @param callback The callback to use.
 */
public expect fun asyncCall(callback: AsyncCallback)
