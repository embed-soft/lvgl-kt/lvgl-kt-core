package io.gitlab.embedSoft.lvglKt.core

/** Represents an area (a rectangle) on the screen. */
public expect class Area : Closable {
    /** Vertical size in pixels. */
    public var height: Short
    /** Horizontal size in pixels. */
    public var width: Short
    /** Size of the area in pixels. */
    public val size: UInt

    /**
     * Moves the area to a different location.
     * @param xOffset The X offset.
     * @param yOffset The Y offset.
     */
    public fun move(xOffset: Short, yOffset: Short)

    /**
     * Changes the position and size of the area.
     * @param left Left edge as a X coordinate.
     * @param top Top edge as a Y coordinate.
     * @param right Right edge as a X coordinate.
     * @param bottom Bottom edge as a Y coordinate.
     */
    public fun setPositionAndSize(left: Short, top: Short, right: Short, bottom: Short)

    public companion object {
        /**
         * Creates a new [Area].
         * @return The new [Area] instance.
         */
        public fun create(): Area
    }
}
