package io.gitlab.embedSoft.lvglKt.core

import io.gitlab.embedSoft.lvglKt.core.animation.ScreenLoadAnimation
import io.gitlab.embedSoft.lvglKt.core.lvglObject.LvglObjectBase
import io.gitlab.embedSoft.lvglKt.core.lvglObject.lvglObject

/** Covers all basic screen functionality. */
public expect class Screen : Closable {
    /** The object that represents the screen itself. */
    public val lvglObject: LvglObjectBase?

    public companion object {
        /** Get the active screen of the default display. */
        public val activeScreen: LvglObjectBase

        /** Creates a new screen. */
        public fun create(obj: LvglObjectBase): Screen

        /** Loads a screen. */
        public fun load(screen: LvglObjectBase)

        /** Loads a screen. */
        public fun load(screen: Screen)

        /**
         * Switch screen with animation.
         * @param screen The new screen to load.
         * @param time Time of the animation.
         * @param delay Delay before the transition.
         * @param autoDel If *true* then automatically delete the old screen.
         * @param animType The type of animation to use, eg `ScreenLoadAnimation.MOVE_LEFT`.
         */
        public fun loadAnimation(
            screen: LvglObjectBase,
            time: UInt,
            delay: UInt,
            autoDel: Boolean,
            animType: ScreenLoadAnimation
        )

        /**
         * Switch screen with animation.
         * @param screen The new screen to load.
         * @param time Time of the animation.
         * @param delay Delay before the transition.
         * @param autoDel If *true* then automatically delete the old screen.
         * @param animType The type of animation to use, eg `ScreenLoadAnimation.MOVE_LEFT`.
         */
        public fun loadAnimation(
            screen: Screen,
            time: UInt,
            delay: UInt,
            autoDel: Boolean,
            animType: ScreenLoadAnimation
        )
    }
}

/** Creates a new screen that is initialized with a LVGL object. */
public fun screen(init: LvglObjectBase.() -> Unit): Screen = Screen.create(lvglObject(null, init))
