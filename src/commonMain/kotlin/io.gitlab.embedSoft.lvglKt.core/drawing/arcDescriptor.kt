package io.gitlab.embedSoft.lvglKt.core.drawing

import io.gitlab.embedSoft.lvglKt.core.Closable
import io.gitlab.embedSoft.lvglKt.core.StringReference
import io.gitlab.embedSoft.lvglKt.core.styling.Color

/** Provides meta data on an arc. */
public expect class ArcDescriptor : Closable {
    public var blendMode: UByte
    public var color: Color
    public var opacity: UByte
    public var width: Short
    public var rounded: UByte

    /** Uses the image file path as the image source. */
    public fun setImageSource(imageSrc: String): StringReference

    /** Uses the image file path as the image source. */
    public fun setImageSource(imageSrc: StringReference)

    /** Retrieves the image source as the path to the image file. */
    public fun getImageSourceAsString(): String

    public companion object {
        /**
         * Creates a new [ArcDescriptor].
         * @return A new [ArcDescriptor] instance.
         */
        public fun create(): ArcDescriptor
    }
}

public fun arcDescriptor(init: ArcDescriptor.() -> Unit): ArcDescriptor {
    val result = ArcDescriptor.create()
    result.init()
    return result
}
