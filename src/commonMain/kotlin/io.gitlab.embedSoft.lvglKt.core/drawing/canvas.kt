package io.gitlab.embedSoft.lvglKt.core.drawing

import io.gitlab.embedSoft.lvglKt.core.Area
import io.gitlab.embedSoft.lvglKt.core.Point
import io.gitlab.embedSoft.lvglKt.core.StringReference
import io.gitlab.embedSoft.lvglKt.core.lvglObject.LvglObjectBase
import io.gitlab.embedSoft.lvglKt.core.styling.Color

/**
 * Inherits from Image where the user can draw anything. Rectangles, texts, images, lines, arcs can be drawn here
 * using LVGL's drawing engine. Additionally, **effects** can be applied, such as rotation, zoom and blur.
 */
public expect class Canvas : LvglObjectBase {
    /**
     * Draws a rectangle on the canvas.
     * @param xPos X coordinate.
     * @param yPos Y coordinate.
     * @param width Width of the rectangle
     * @param height Height of the rectangle.
     * @param drawDesc The drawing metadata to use.
     */
    public fun drawRectangle(xPos: Short, yPos: Short, width: Short, height: Short, drawDesc: RectangleDescriptor)

    /**
     * Draws text on the canvas.
     * @param xPos X coordinate.
     * @param yPos Y coordinate.
     * @param maxWidth Max width of the text. The text will be wrapped to fit into this size.
     * @param txt The text to display.
     * @param drawDesc The drawing metadata to use.
     */
    public fun drawText(xPos: Short, yPos: Short, maxWidth: Short, txt: String, drawDesc: LabelDescriptor)

    /**
     * Sets the color palette.
     * @param id Unique identifier.
     * @param color The color palette to use.
     */
    public fun setPalette(id: UByte, color: Color)

    /**
     * Sets the pixel data.
     * @param xPos The X coordinate.
     * @param yPos The Y coordinate.
     * @param color Pixel data to set.
     */
    public fun setPixels(xPos: Short, yPos: Short, color: Color)

    /**
     * Gets the pixel data.
     * @param xPos The X coordinate.
     * @param yPos The Y coordinate.
     * @return The pixel data.
     */
    public fun getPixels(xPos: Short, yPos: Short): Color

    /**
     * Fills the background with [color].
     * @param color The color to use.
     * @param opacity The opacity to apply.
     */
    public fun fillBackground(color: Color, opacity: UByte)

    /**
     * Changes the [buffer][buf] used by the canvas.
     * @param buf The buffer to use.
     * @param width Horizontal size in pixels.
     * @param height Vertical size in pixels.
     * @param colorFormat The color format to use (eg `LV_IMG_CF_TRUE_COLOR`).
     */
    public fun setBuffer(buf: CanvasBuffer, width: Short, height: Short, colorFormat: UByte)

    public companion object {
        /**
         * Creates a new [Canvas].
         * @param parent The parent object.
         * @return A new [Canvas] instance.
         */
        public fun create(parent: LvglObjectBase): Canvas
    }

    /**
     * Apply horizontal blur on the canvas.
     * @param area The area to blur. If *null* the whole canvas will be blurred.
     * @param radius The radius of the blur.
     */
    public fun blurHorizontally(area: Area?, radius: UShort)

    /**
     * Apply vertical blur on the canvas.
     * @param area The area to blur. If *null* the whole canvas will be blurred.
     * @param radius The radius of the blur.
     */
    public fun blurVertically(area: Area?, radius: UShort)

    /**
     * Draw an image on the canvas.
     * @param xPos Left coordinate of the image.
     * @param yPos Top coordinate of the image.
     * @param src The path to the image file.
     * @param drawDesc The image descriptor.
     */
    public fun drawImage(xPos: Short, yPos: Short, src: StringReference, drawDesc: DrawImageDescriptor)

    /**
     * Draw an image on the canvas.
     * @param xPos Left coordinate of the image.
     * @param yPos Top coordinate of the image.
     * @param src The path to the image file.
     * @param drawDesc The image descriptor.
     * @return The reference to [src]. Remember to close it when finished with it.
     */
    public fun drawImage(xPos: Short, yPos: Short, src: String, drawDesc: DrawImageDescriptor): StringReference

    /**
     * Draw a line on the canvas.
     * @param drawDesc The line descriptor.
     * @param points Two or more points used for drawing the line.
     */
    public fun drawLine(drawDesc: LineDescriptor, vararg points: Point)

    /**
     * Draw a line on the canvas.
     * @param drawDesc The line descriptor.
     * @param points Two or more points used for drawing the line. Each point consists of the following:
     * 1. xPos - Horizontal position
     * 2. yPos - Vertical position
     */
    public fun drawLine(drawDesc: LineDescriptor, vararg points: Pair<Short, Short>)

    /**
     * Draw a polygon on the canvas.
     * @param drawDesc The rectangle descriptor.
     * @param points The points used for drawing the polygon.
     */
    public fun drawPolygon(drawDesc: RectangleDescriptor, vararg points: Point)

    /**
     * Draw a polygon on the canvas.
     * @param drawDesc The rectangle descriptor.
     * @param points The points used for drawing the polygon. Each point consists of the following:
     * 1. xPos - Horizontal position
     * 2. yPos - Vertical position
     */
    public fun drawPolygon(drawDesc: RectangleDescriptor, vararg points: Pair<Short, Short>)

    /**
     * Draw an arc on the canvas.
     * @param xPos X origin of the arc.
     * @param yPos Y origin of the arc.
     * @param radius The radius of the arc.
     * @param startAngle Start angle in degrees.
     * @param endAngle End angle in degrees.
     * @param drawDesc The line descriptor.
     */
    public fun drawArc(
        xPos: Short,
        yPos: Short,
        radius: Short,
        startAngle: Int,
        endAngle: Int,
        drawDesc: ArcDescriptor
    )
}

/**
 * Creates a new [Canvas], and initializes it. Remember to assign a [buffer][CanvasBuffer] via [Canvas.setBuffer] to
 * the canvas before using the canvas itself.
 * @param parent The canvas's parent.
 * @param init Initialization block for the canvas.
 * @return The new [Canvas] instance.
 */
public fun canvas(parent: LvglObjectBase, init: Canvas.() -> Unit): Canvas {
    val result = Canvas.create(parent)
    result.init()
    return result
}
