package io.gitlab.embedSoft.lvglKt.core.drawing

import io.gitlab.embedSoft.lvglKt.core.Closable

/** Provides a buffer for the [Canvas] to do drawing operations. */
public expect class CanvasBuffer : Closable {
    public companion object {
        /**
         * Creates a new [CanvasBuffer].
         * @param bufSize Buffer size. It is recommended to use the [trueColorBufferSize] or [trueColorAlphaBufferSize]
         * functions to calculate the buffer size.
         * @return A new [CanvasBuffer] instance.
         */
        public fun create(bufSize: UInt): CanvasBuffer
    }
}

/** Calculates the buffer size for True Color. */
public expect fun trueColorBufferSize(width: Short, height: Short): UInt

/** Calculates the buffer size for True Color with Alpha. */
public expect fun trueColorAlphaBufferSize(width: Short, height: Short): UInt

