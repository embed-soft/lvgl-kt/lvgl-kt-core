package io.gitlab.embedSoft.lvglKt.core.drawing

import io.gitlab.embedSoft.lvglKt.core.Closable
import io.gitlab.embedSoft.lvglKt.core.styling.Color

/** Provides meta data on a line. */
public expect class LineDescriptor : Closable {
    public var blendMode: UByte
    public var color: Color
    public var opacity: UByte
    public var dashGap: Short
    public var dashWidth: Short
    public var rawEnd: UByte
    public var roundEnd: UByte
    public var roundStart: UByte
    public var width: Short

    public companion object {
        /**
         * Creates a new [LineDescriptor].
         * @return A new [LineDescriptor] instance.
         */
        public fun create(): LineDescriptor
    }
}

public fun lineDescriptor(init: LineDescriptor.() -> Unit): LineDescriptor {
    val result = LineDescriptor.create()
    result.init()
    return result
}
