package io.gitlab.embedSoft.lvglKt.core.drawing

import io.gitlab.embedSoft.lvglKt.core.Closable
import io.gitlab.embedSoft.lvglKt.core.Point
import io.gitlab.embedSoft.lvglKt.core.styling.Color

/** Provides meta data on a image. */
public expect class DrawImageDescriptor : Closable {
    public var angle: Short
    public var opacity: UByte
    public var blendMode: UByte
    public var antiAlias: UByte
    public var frameId: Int
    public var recolor: Color
    public var recolorOpacity: UByte
    public var zoom: UShort
    public var pivot: Point

    public companion object {
        /**
         * Creates a new [DrawImageDescriptor].
         * @return A new [DrawImageDescriptor] instance.
         */
        public fun create(): DrawImageDescriptor
    }
}

public fun drawImageDescriptor(init: DrawImageDescriptor.() -> Unit): DrawImageDescriptor {
    val result = DrawImageDescriptor.create()
    result.init()
    return result
}
