package io.gitlab.embedSoft.lvglKt.core.drawing

import io.gitlab.embedSoft.lvglKt.core.Closable
import io.gitlab.embedSoft.lvglKt.core.styling.Color

/** Provides meta data on a label. */
public expect class LabelDescriptor : Closable {
    /** Label color. */
    public var color: Color
    /** Blend mode. */
    public var blendMode: UByte
    /** Decor applied to the label. */
    public var decor: UByte
    /** Alignment of the label. */
    public var align: UByte
    /** Bi directional direction. */
    public var bidiDir: UByte
    /** Text flags to apply. */
    public var flag: UByte
    /** Letter space. */
    public var letterSpace: Short
    /** Line space. */
    public var lineSpace: Short
    /** X offset. */
    public var xOffset: Short
    /** Y offset. */
    public var yOffset: Short
    /** Opacity level applied to the label. */
    public var opacity: UByte
    /** Selected color. */
    public var selectedColor: Color
    /** Selected background color. */
    public var selectedBackgroundColor: Color
    /** Selected start. */
    public var selectedStart: UInt
    /** Selected end. */
    public var selectedEnd: UInt

    // TODO: Implement font property (via lv_font_t type).

    public companion object {
        /**
         * Creates a new [LabelDescriptor].
         * @return A new [LabelDescriptor] instance.
         */
        public fun create(): LabelDescriptor
    }
}

public fun labelDescriptor(init: LabelDescriptor.() -> Unit): LabelDescriptor {
    val result = LabelDescriptor.create()
    result.init()
    return result
}
