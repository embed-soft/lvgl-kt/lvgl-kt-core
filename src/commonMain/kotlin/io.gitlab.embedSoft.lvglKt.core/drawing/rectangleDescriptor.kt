package io.gitlab.embedSoft.lvglKt.core.drawing

import io.gitlab.embedSoft.lvglKt.core.Closable
import io.gitlab.embedSoft.lvglKt.core.StringReference
import io.gitlab.embedSoft.lvglKt.core.styling.Color

/** Provides meta data on a rectangle. */
public expect class RectangleDescriptor : Closable {
    /** Background color. */
    public var backgroundColor: Color
    /** The image dithering for the background gradient. */
    public var backgroundGradientDither: UByte
    /** Total number of stops in the background gradient. */
    public var backgroundGradientStopsCount: UByte
    /** Background gradient direction. */
    public var backgroundGradientDirection: UByte
    /** Background image opacity. */
    public var backgroundImageOpacity: UByte
    /** Background gradient stop color. */
    public var backgroundGradientStopColor: Color
    /** Background image recolor. */
    public var backgroundImageRecolor: Color
    /** Background image recolor opacity. */
    public var backgroundImageRecolorOpacity: UByte
    /** Shadow color. */
    public var shadowColor: Color
    /** Shadow X offset. */
    public var shadowXOffset: Short
    /** Shadow Y offset. */
    public var shadowYOffset: Short
    /** Shadow opacity. */
    public var shadowOpacity: UByte
    /** Shadow spread. */
    public var shadowSpread: Short
    /** Shadow width. */
    public var shadowWidth: Short
    /** Outline color. */
    public var outlineColor: Color
    /** Outline opacity. */
    public var outlineOpacity: UByte
    /** Outline padding. */
    public var outlinePad: Short
    /** Outline width. */
    public var outlineWidth: Short
    /** The rectangle's radius. */
    public var radius: Short
    /** Border color. */
    public var borderColor: Color
    /** Border Opacity. */
    public var borderOpacity: UByte
    /** Border post. */
    public var borderPost: UByte
    /** Border side */
    public var borderSide: UByte
    /** Border width. */
    public var borderWidth: Short
    /** Blend mode. */
    public var blendMode: UByte
    /** Background opacity. */
    public var backgroundOpacity: UByte
    /** Background image tiled. */
    public var backgroundImageTiled: Boolean

    /**
     * Changes the background image source to use an image file path, or a symbol.
     * @param value Image file path or symbol.
     * @return A reference to [value].
     */
    public fun setBackgroundImageSource(value: String): StringReference

    /**
     * Changes the background image source to use an image file path, or a symbol.
     * @param value Image file path or symbol.
     */
    public fun setBackgroundImageSource(value: StringReference)

    /**
     * Gets the background image source as a [String].
     * @return The image file path or symbol.
     */
    public fun getBackgroundImageSourceAsString(): String

    public companion object {
        /**
         * Creates a new [RectangleDescriptor].
         * @return A new [RectangleDescriptor] instance.
         */
        public fun create(): RectangleDescriptor
    }
}

public fun rectangleDescriptor(init: RectangleDescriptor.() -> Unit): RectangleDescriptor {
    val result = RectangleDescriptor.create()
    result.init()
    return result
}
