package io.gitlab.embedSoft.lvglKt.core

/** Provides meta data on a image. */
public expect class ImageDescriptor : Closable {
    // TODO: Implement data variable.
    // TODO: Implement header variable.
    public var dataSize: UInt

    public companion object {
        /**
         * Creates a new [ImageDescriptor].
         * @return A new [ImageDescriptor] instance.
         */
        public fun create(): ImageDescriptor
    }
}

public fun drawImageDescriptor(init: ImageDescriptor.() -> Unit): ImageDescriptor {
    val result = ImageDescriptor.create()
    result.init()
    return result
}
