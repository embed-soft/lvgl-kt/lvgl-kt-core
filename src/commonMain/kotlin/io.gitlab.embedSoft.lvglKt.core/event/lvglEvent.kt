package io.gitlab.embedSoft.lvglKt.core.event

import io.gitlab.embedSoft.lvglKt.core.Area
import io.gitlab.embedSoft.lvglKt.core.Point
import io.gitlab.embedSoft.lvglKt.core.animation.Animation
import io.gitlab.embedSoft.lvglKt.core.lvglObject.LvglObjectBase
import io.gitlab.embedSoft.lvglKt.core.lvglObject.LvglObjectDrawPartDescriptor

/** Provides meta data for a LVGL event. */
public expect class LvglEvent {
    /** The event code (eg [LvglEventType.CLICKED]). */
    public val code: LvglEventType
    /** The object that the event is sent to. */
    public val target: LvglObjectBase
    /** The keyboard key code. */
    public val key: UInt
    /** The scroll animation. */
    public val scrollAnimation: Animation
    /** The previous size. */
    public val oldSize: Area
    /** The cover area. */
    public val coverArea: Area
    /** The self size information. */
    public val selfSizeInfo: Point
    /** The object that the event was originally sent to. */
    public val currentTarget: LvglObjectBase

    /**
     * Changes the external draw size.
     * @param size The size to use.
     */
    public fun setExternalDrawSize(size: Short)
}

/** Obtains the parameter passed through via [LvglObjectBase.sendEvent] as a [String]. */
public expect fun LvglEvent.getParamAsString(): String

/** Obtains the parameter passed through via [LvglObjectBase.sendEvent] as a [LvglObjectDrawPartDescriptor]. */
public expect fun LvglEvent.getParamAsLvglObjectDrawPartDescriptor(): LvglObjectDrawPartDescriptor
