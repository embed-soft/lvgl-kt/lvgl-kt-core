package io.gitlab.embedSoft.lvglKt.core.event

import io.gitlab.embedSoft.lvglKt.core.Closable

/** Represents a LVGL event callback. */
public expect class LvglEventCallback : Closable {
    public companion object {
        /**
         * Creates a new [LvglEventCallback] object.
         * @param userData User data to pass to the [callback]. Will be *null* if empty.
         * @param callback The callback to use.
         * @return A new [LvglEventCallback].
         */
        public fun create(userData: Any? = null, callback: (evt: LvglEvent, userData: Any?) -> Unit): LvglEventCallback
    }
}
