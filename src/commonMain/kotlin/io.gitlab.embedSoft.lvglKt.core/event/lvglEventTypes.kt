package io.gitlab.embedSoft.lvglKt.core.event

/** The type of event being sent to the LVGL object. */
public enum class LvglEventType {
    /** Cover all events. */
    ALL,
    // Input device events.
    /** The object has been pressed. */
    PRESSED,

    /** The object is being pressed (called continuously while pressing). */
    PRESSING,

    /** The object is still being pressed but slid cursor/finger off of the object. */
    PRESS_LOST,

    /** The object was pressed for a short period of time, then released. Not called if scrolled. */
    SHORT_CLICKED,

    /** Object has been pressed for at least `long_press_time`. Not called if scrolled. */
    LONG_PRESSED,

    /** Called after `long_press_time` in every `long_press_repeat_time` ms.  Not called if scrolled.*/
    LONG_PRESSED_REPEAT,

    /** Called on release if not scrolled (regardless to long press). */
    CLICKED,

    /** Called in every cases when the object has been released. */
    RELEASED,

    /** Scrolling begins. */
    SCROLL_BEGIN,

    /** Scrolling ends. */
    SCROLL_END,

    /** Scrolling. */
    SCROLL,

    /** A gesture is detected. Get the gesture with `lv_indev_get_gesture_dir(lv_indev_get_act());`. */
    GESTURE,

    /** A key is sent to the object. Get the key with `lv_indev_get_key(lv_indev_get_act());`*/
    KEY,

    /** The object is focused. */
    FOCUSED,

    /** The object is defocused. */
    DEFOCUSED,

    /** The object is defocused but still selected. */
    LEAVE,

    /** Perform advanced hit-testing. */
    HIT_TEST,
    // Drawing events.
    /** Check if the object fully covers an area. The event parameter is `lv_cover_check_info_t *`. */
    COVER_CHECK,

    /**
     * Get the required extra draw area around the object (e.g. for shadow). The event parameter is `lv_coord_t *` to
     * store the size.
     */
    REFRESH_EXT_DRAW_SIZE,

    /** Starting the main drawing phase. */
    DRAW_MAIN_BEGIN,

    /** Perform the main drawing. */
    DRAW_MAIN,

    /** Finishing the main drawing phase. */
    DRAW_MAIN_END,

    /** Starting the post draw phase (when all children are drawn). */
    DRAW_POST_BEGIN,

    /** Perform the post draw phase (when all children are drawn). */
    DRAW_POST,

    /** Finishing the post draw phase (when all children are drawn). */
    DRAW_POST_END,

    /** To start drawing a part. The event parameter is `lv_obj_draw_dsc_t *`. */
    DRAW_PART_BEGIN,

    /** To finish drawing a part. The event parameter is `lv_obj_draw_dsc_t *`. */
    DRAW_PART_END,
    // Special events.
    /** The object's value has changed (i.e. slider moved). */
    VALUE_CHANGED,

    /** A text is inserted to the object. The event data is `char *` being inserted. */
    INSERT,

    /** Notify the object to refresh something on it (for the user). */
    REFRESH,

    /** A process has finished. */
    READY,

    /** A process has been cancelled. */
    CANCEL,
    // Other events.
    /** Object is being deleted. */
    DELETE,

    /** Child was removed/added. */
    CHILD_CHANGED,

    /** Object coordinates/size have changed. */
    SIZE_CHANGED,

    /** Object's style has changed. */
    STYLE_CHANGED,

    /** The children position has changed due to a layout recalculation. */
    LAYOUT_CHANGED,

    /** Get the internal size of a widget. */
    GET_SELF_SIZE,
}

/** A unique value for the [LvglEventType]. */
public expect val LvglEventType.value: UInt

/**
 * Converts a value to a [LvglEventType].
 * @return The corresponding [LvglEventType] or a [IllegalStateException] if the value isn't recognised.
 */
public expect fun UInt.toLvglEventType(): LvglEventType
