package io.gitlab.embedSoft.lvglKt.core

import io.gitlab.embedSoft.lvglKt.core.lvglObject.LvglObjectBase
import kotlinx.cinterop.*
import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.*

public actual class Image private constructor(
    ptr: CPointer<lv_img_t>? = null,
    parent: LvglObjectBase? = null
) : LvglObjectBase {
    override val lvObjPtr: CPointer<lv_obj_t>? =
        if (parent != null) lv_img_create(parent.lvObjPtr) else ptr?.reinterpret()
    public val lvImgPtr: CPointer<lv_img_t>?
        get() = lvObjPtr?.reinterpret()

    public actual companion object {
        public actual fun create(parent: LvglObjectBase): Image = Image(parent = parent)

        public fun fromPointer(ptr: CPointer<lv_img_t>?): Image = Image(ptr = ptr)
    }

    public actual fun setSrc(src: String) {
        lv_img_set_src(lvObjPtr, src.cstr)
    }

    public actual fun getSrcAsString(): String = lv_img_get_src(lvObjPtr)?.reinterpret<ByteVar>()?.toKString() ?: ""

    public actual fun setSrc(src: ImageDescriptor) {
        lv_img_set_src(lvObjPtr, src.lvImgDscPtr)
    }
}

public fun CPointer<lv_img_t>?.toImage(): Image = Image.fromPointer(this)
