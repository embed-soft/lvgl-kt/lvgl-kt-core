package io.gitlab.embedSoft.lvglKt.core.concurrency

import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.lv_async_call

public actual fun asyncCall(callback: AsyncCallback) {
    lv_async_call(callback.lvAsyncCbPtr, callback.stableRef.asCPointer())
}
