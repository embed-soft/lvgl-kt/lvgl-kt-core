package io.gitlab.embedSoft.lvglKt.core.concurrency

import io.gitlab.embedSoft.lvglKt.core.Closable
import kotlinx.cinterop.*
import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.lv_timer_cb_t
import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.lv_timer_t

public actual class TimerCallback private constructor(
    public val callback: (timer: Timer, userData: Any?) -> Unit,
    public val userData: Any? = null
): Closable {
    internal val stableRef = StableRef.create(this)
    public val lvTimerCbPtr: lv_timer_cb_t = staticCFunction { timer: CPointer<lv_timer_t>? ->
        val tmpTimer = timer.toTimer()
        val tmpObj = tmpTimer.lvTimerPtr?.pointed?.user_data?.asStableRef<TimerCallback>()?.get()
        tmpObj?.callback?.invoke(tmpTimer, tmpObj.userData)
    }

    public actual companion object {
        public actual fun create(
            userData: Any?,
            callback: (timer: Timer, userData: Any?) -> Unit
        ): TimerCallback = TimerCallback(callback, userData)
    }

    override fun close() {
        stableRef.dispose()
    }
}
