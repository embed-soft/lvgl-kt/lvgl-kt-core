package io.gitlab.embedSoft.lvglKt.core.styling

import io.gitlab.embedSoft.lvglKt.core.Closable
import kotlinx.cinterop.CPointer
import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.lv_font_default
import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.lv_font_free
import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.lv_font_load
import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.lv_font_t

public actual class Font private constructor(ptr: CPointer<lv_font_t>?) : Closable {
    public val lvFontPtr: CPointer<lv_font_t>? = ptr

    public actual companion object {
        public fun fromPointer(ptr: CPointer<lv_font_t>?): Font = Font(ptr)

        public actual fun load(fontName: String): Font = fromPointer(lv_font_load(fontName))

        public actual fun getDefault(): Font = fromPointer(lv_font_default())
    }

    override fun close() {
        lv_font_free(lvFontPtr)
    }
}

public fun CPointer<lv_font_t>?.toFont(): Font = Font.fromPointer(this)

public fun lv_font_t?.copy(dest: lv_font_t) {
    if (this != null) {
        dest.dsc = dsc
        dest.base_line = base_line
        dest.get_glyph_bitmap = get_glyph_bitmap
        dest.get_glyph_dsc = get_glyph_dsc
        dest.line_height = line_height
        dest.subpx = subpx
        dest.underline_position = underline_position
        dest.underline_thickness = underline_thickness
        dest.user_data = user_data
    }
}
