package io.gitlab.embedSoft.lvglKt.core

import kotlinx.cinterop.*

public actual class StringReference private constructor(value: String): Closable {
    private val arena = Arena()
    /** The reference to the [String]. */
    public val ref: CPointer<ByteVarOf<Byte>> = value.cstr.getPointer(arena)
    public actual val str: String
        get() = ref.toKString()

    override fun close() {
        arena.clear()
    }

    public actual companion object {
        public actual fun create(value: String): StringReference = StringReference(value)
    }
}
