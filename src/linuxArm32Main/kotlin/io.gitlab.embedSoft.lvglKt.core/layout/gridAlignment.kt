package io.gitlab.embedSoft.lvglKt.core.layout

import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.lv_grid_align_t
import io.gitlab.embedSoft.lvglKt.core.layout.GridAlignment

public fun GridAlignment.toLvGridAlign(): lv_grid_align_t = when (this) {
    GridAlignment.CENTER -> lv_grid_align_t.LV_GRID_ALIGN_CENTER
    GridAlignment.END -> lv_grid_align_t.LV_GRID_ALIGN_END
    GridAlignment.SPACE_AROUND -> lv_grid_align_t.LV_GRID_ALIGN_SPACE_AROUND
    GridAlignment.START -> lv_grid_align_t.LV_GRID_ALIGN_START
    GridAlignment.STRETCH -> lv_grid_align_t.LV_GRID_ALIGN_STRETCH
    GridAlignment.SPACE_BETWEEN -> lv_grid_align_t.LV_GRID_ALIGN_SPACE_BETWEEN
    GridAlignment.SPACE_EVENLY -> lv_grid_align_t.LV_GRID_ALIGN_SPACE_EVENLY
}

public fun lv_grid_align_t.toGridAlignment(): GridAlignment = when (this) {
    lv_grid_align_t.LV_GRID_ALIGN_CENTER -> GridAlignment.CENTER
    lv_grid_align_t.LV_GRID_ALIGN_END -> GridAlignment.END
    lv_grid_align_t.LV_GRID_ALIGN_SPACE_AROUND -> GridAlignment.SPACE_AROUND
    lv_grid_align_t.LV_GRID_ALIGN_START -> GridAlignment.START
    lv_grid_align_t.LV_GRID_ALIGN_STRETCH -> GridAlignment.STRETCH
    lv_grid_align_t.LV_GRID_ALIGN_SPACE_BETWEEN -> GridAlignment.SPACE_BETWEEN
    lv_grid_align_t.LV_GRID_ALIGN_SPACE_EVENLY -> GridAlignment.SPACE_EVENLY
    else -> throw IllegalStateException("Unrecognised value.")
}
