package io.gitlab.embedSoft.lvglKt.core.layout

import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.*
import io.gitlab.embedSoft.lvglKt.core.layout.FlexAlignment

public actual val FlexAlignment.value: UInt
    get() = when(this) {
        FlexAlignment.START -> lv_flex_align_t.LV_FLEX_ALIGN_START.value
        FlexAlignment.CENTER -> lv_flex_align_t.LV_FLEX_ALIGN_CENTER.value
        FlexAlignment.END -> lv_flex_align_t.LV_FLEX_ALIGN_END.value
        FlexAlignment.SPACE_EVENLY -> lv_flex_align_t.LV_FLEX_ALIGN_SPACE_EVENLY.value
        FlexAlignment.SPACE_AROUND -> lv_flex_align_t.LV_FLEX_ALIGN_SPACE_AROUND.value
        FlexAlignment.SPACE_BETWEEN -> lv_flex_align_t.LV_FLEX_ALIGN_SPACE_BETWEEN.value
    }

public actual fun UInt.toFlexAlignment(): FlexAlignment = when(this) {
    lv_flex_align_t.LV_FLEX_ALIGN_START.value -> FlexAlignment.START
    lv_flex_align_t.LV_FLEX_ALIGN_CENTER.value -> FlexAlignment.CENTER
    lv_flex_align_t.LV_FLEX_ALIGN_END.value -> FlexAlignment.END
    lv_flex_align_t.LV_FLEX_ALIGN_SPACE_EVENLY.value -> FlexAlignment.SPACE_EVENLY
    lv_flex_align_t.LV_FLEX_ALIGN_SPACE_AROUND.value -> FlexAlignment.SPACE_AROUND
    lv_flex_align_t.LV_FLEX_ALIGN_SPACE_BETWEEN.value -> FlexAlignment.SPACE_BETWEEN
    else -> throw IllegalStateException("Unrecognised value")
}

public fun FlexAlignment.toLvFlexAlign(): lv_flex_align_t = when (this) {
    FlexAlignment.START -> lv_flex_align_t.LV_FLEX_ALIGN_START
    FlexAlignment.CENTER -> lv_flex_align_t.LV_FLEX_ALIGN_CENTER
    FlexAlignment.END -> lv_flex_align_t.LV_FLEX_ALIGN_END
    FlexAlignment.SPACE_EVENLY -> lv_flex_align_t.LV_FLEX_ALIGN_SPACE_EVENLY
    FlexAlignment.SPACE_AROUND -> lv_flex_align_t.LV_FLEX_ALIGN_SPACE_AROUND
    FlexAlignment.SPACE_BETWEEN -> lv_flex_align_t.LV_FLEX_ALIGN_SPACE_BETWEEN
}
