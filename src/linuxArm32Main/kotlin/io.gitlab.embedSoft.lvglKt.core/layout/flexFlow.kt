package io.gitlab.embedSoft.lvglKt.core.layout

import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.*
import io.gitlab.embedSoft.lvglKt.core.layout.FlexFlow

public actual val FlexFlow.value: UInt
    get() = when(this) {
        FlexFlow.COLUMN -> LV_FLEX_FLOW_COLUMN
        FlexFlow.ROW -> LV_FLEX_FLOW_ROW
        FlexFlow.COLUMN_REVERSE -> LV_FLEX_FLOW_COLUMN_REVERSE
        FlexFlow.COLUMN_WRAP -> LV_FLEX_FLOW_COLUMN_WRAP
        FlexFlow.ROW_WRAP -> LV_FLEX_FLOW_ROW_WRAP
        FlexFlow.ROW_WRAP_REVERSE -> LV_FLEX_FLOW_ROW_WRAP_REVERSE
        FlexFlow.ROW_REVERSE -> LV_FLEX_FLOW_ROW_REVERSE
        FlexFlow.COLUMN_WRAP_REVERSE -> LV_FLEX_FLOW_COLUMN_WRAP_REVERSE
    }

public actual fun UInt.toFlexFlow(): FlexFlow = when(this) {
    LV_FLEX_FLOW_COLUMN -> FlexFlow.COLUMN
    LV_FLEX_FLOW_ROW -> FlexFlow.ROW
    LV_FLEX_FLOW_COLUMN_REVERSE -> FlexFlow.COLUMN_REVERSE
    LV_FLEX_FLOW_COLUMN_WRAP -> FlexFlow.COLUMN_WRAP
    LV_FLEX_FLOW_ROW_WRAP -> FlexFlow.ROW_WRAP
    LV_FLEX_FLOW_ROW_WRAP_REVERSE -> FlexFlow.ROW_WRAP_REVERSE
    LV_FLEX_FLOW_ROW_REVERSE -> FlexFlow.ROW_REVERSE
    LV_FLEX_FLOW_COLUMN_WRAP_REVERSE -> FlexFlow.COLUMN_WRAP_REVERSE
    else -> throw IllegalStateException("Unrecognised value")
}
