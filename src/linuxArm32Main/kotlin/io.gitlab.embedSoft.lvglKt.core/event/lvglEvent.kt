package io.gitlab.embedSoft.lvglKt.core.event

import io.gitlab.embedSoft.lvglKt.core.Area
import io.gitlab.embedSoft.lvglKt.core.Point
import io.gitlab.embedSoft.lvglKt.core.animation.Animation
import io.gitlab.embedSoft.lvglKt.core.animation.toAnimation
import io.gitlab.embedSoft.lvglKt.core.toArea
import io.gitlab.embedSoft.lvglKt.core.toPoint
import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.*
import io.gitlab.embedSoft.lvglKt.core.lvglObject.*
import kotlinx.cinterop.*

public actual class LvglEvent private constructor(ptr: CPointer<lv_event_t>?) {
    public val lvEventPtr: CPointer<lv_event_t>? = ptr
    public actual val code: LvglEventType
        get() = lv_event_get_code(lvEventPtr).toLvglEventType()
    public actual val target: LvglObjectBase
        get() = LvglObject.fromPointer(lv_event_get_target(lvEventPtr))
    public actual val key: UInt
        get() = lv_event_get_key(lvEventPtr)
    public actual val scrollAnimation: Animation
        get() = lv_event_get_scroll_anim(lvEventPtr).toAnimation()
    public actual val oldSize: Area
        get() = lv_event_get_old_size(lvEventPtr).toArea()
    public actual val coverArea: Area
        get() = lv_event_get_cover_area(lvEventPtr).toArea()
    public actual val selfSizeInfo: Point
        get() = lv_event_get_self_size_info(lvEventPtr).toPoint()
    public actual val currentTarget: LvglObjectBase
        get() = lv_event_get_current_target(lvEventPtr).toLvglObject()

    public companion object {
        public fun fromPointer(ptr: CPointer<lv_event_t>?): LvglEvent =
            LvglEvent(ptr)
    }

    public actual fun setExternalDrawSize(size: Short) {
        lv_event_set_ext_draw_size(lvEventPtr, size)
    }
}

public fun CPointer<lv_event_t>?.toLvglEvent(): LvglEvent = LvglEvent.fromPointer(this)

public actual fun LvglEvent.getParamAsString(): String =
    lv_event_get_param(lvEventPtr)?.reinterpret<ByteVar>()?.toKString() ?: ""

public actual fun LvglEvent.getParamAsLvglObjectDrawPartDescriptor(): LvglObjectDrawPartDescriptor =
    lv_event_get_param(lvEventPtr)?.reinterpret<lv_obj_draw_part_dsc_t>().toLvglObjectDrawPartDescriptor()
