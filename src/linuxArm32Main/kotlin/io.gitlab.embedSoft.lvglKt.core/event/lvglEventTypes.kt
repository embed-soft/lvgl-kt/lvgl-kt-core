package io.gitlab.embedSoft.lvglKt.core.event

import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.*

public actual val LvglEventType.value: UInt
    get() = when (this) {
        LvglEventType.ALL -> LV_EVENT_ALL
        LvglEventType.LEAVE -> LV_EVENT_LEAVE
        LvglEventType.FOCUSED -> LV_EVENT_FOCUSED
        LvglEventType.DEFOCUSED -> LV_EVENT_DEFOCUSED
        LvglEventType.GESTURE -> LV_EVENT_GESTURE
        LvglEventType.HIT_TEST -> LV_EVENT_HIT_TEST
        LvglEventType.KEY -> LV_EVENT_KEY
        LvglEventType.CLICKED -> LV_EVENT_CLICKED
        LvglEventType.PRESSED -> LV_EVENT_PRESSED
        LvglEventType.LONG_PRESSED -> LV_EVENT_LONG_PRESSED
        LvglEventType.PRESSING -> LV_EVENT_PRESSING
        LvglEventType.PRESS_LOST -> LV_EVENT_PRESS_LOST
        LvglEventType.RELEASED -> LV_EVENT_RELEASED
        LvglEventType.SCROLL -> LV_EVENT_SCROLL
        LvglEventType.SCROLL_BEGIN -> LV_EVENT_SCROLL_BEGIN
        LvglEventType.SCROLL_END -> LV_EVENT_SCROLL_END
        LvglEventType.SHORT_CLICKED -> LV_EVENT_SHORT_CLICKED
        LvglEventType.LONG_PRESSED_REPEAT -> LV_EVENT_LONG_PRESSED_REPEAT

        LvglEventType.COVER_CHECK -> LV_EVENT_COVER_CHECK
        LvglEventType.DRAW_MAIN -> LV_EVENT_DRAW_MAIN
        LvglEventType.DRAW_MAIN_END -> LV_EVENT_DRAW_MAIN_END
        LvglEventType.DRAW_PART_BEGIN -> LV_EVENT_DRAW_PART_BEGIN
        LvglEventType.DRAW_PART_END -> LV_EVENT_DRAW_PART_END
        LvglEventType.DRAW_POST -> LV_EVENT_DRAW_POST
        LvglEventType.DRAW_POST_BEGIN -> LV_EVENT_DRAW_POST_BEGIN
        LvglEventType.DRAW_POST_END -> LV_EVENT_DRAW_POST_END
        LvglEventType.DRAW_MAIN_BEGIN -> LV_EVENT_DRAW_MAIN_BEGIN
        LvglEventType.REFRESH_EXT_DRAW_SIZE -> LV_EVENT_REFR_EXT_DRAW_SIZE

        LvglEventType.CANCEL -> LV_EVENT_CANCEL
        LvglEventType.INSERT -> LV_EVENT_INSERT
        LvglEventType.REFRESH -> LV_EVENT_REFRESH
        LvglEventType.VALUE_CHANGED -> LV_EVENT_VALUE_CHANGED
        LvglEventType.READY -> LV_EVENT_READY

        LvglEventType.DELETE -> LV_EVENT_DELETE
        LvglEventType.CHILD_CHANGED -> LV_EVENT_CHILD_CHANGED
        LvglEventType.GET_SELF_SIZE -> LV_EVENT_GET_SELF_SIZE
        LvglEventType.LAYOUT_CHANGED -> LV_EVENT_LAYOUT_CHANGED
        LvglEventType.SIZE_CHANGED -> LV_EVENT_SIZE_CHANGED
        LvglEventType.STYLE_CHANGED -> LV_EVENT_STYLE_CHANGED
    }

public actual fun UInt.toLvglEventType(): LvglEventType = when (this) {
    LV_EVENT_ALL -> LvglEventType.ALL
    LV_EVENT_LEAVE -> LvglEventType.LEAVE
    LV_EVENT_FOCUSED -> LvglEventType.FOCUSED
    LV_EVENT_DEFOCUSED -> LvglEventType.DEFOCUSED
    LV_EVENT_GESTURE -> LvglEventType.GESTURE
    LV_EVENT_HIT_TEST -> LvglEventType.HIT_TEST
    LV_EVENT_KEY -> LvglEventType.KEY
    LV_EVENT_CLICKED -> LvglEventType.CLICKED
    LV_EVENT_PRESSED -> LvglEventType.PRESSED
    LV_EVENT_LONG_PRESSED -> LvglEventType.LONG_PRESSED
    LV_EVENT_PRESSING -> LvglEventType.PRESSING
    LV_EVENT_PRESS_LOST -> LvglEventType.PRESS_LOST
    LV_EVENT_RELEASED -> LvglEventType.RELEASED
    LV_EVENT_SCROLL -> LvglEventType.SCROLL
    LV_EVENT_SCROLL_BEGIN -> LvglEventType.SCROLL_BEGIN
    LV_EVENT_SCROLL_END -> LvglEventType.SCROLL_END
    LV_EVENT_SHORT_CLICKED -> LvglEventType.SHORT_CLICKED
    LV_EVENT_LONG_PRESSED_REPEAT -> LvglEventType.LONG_PRESSED_REPEAT

    LV_EVENT_COVER_CHECK -> LvglEventType.COVER_CHECK
    LV_EVENT_DRAW_MAIN -> LvglEventType.DRAW_MAIN
    LV_EVENT_DRAW_MAIN_END -> LvglEventType.DRAW_MAIN_END
    LV_EVENT_DRAW_PART_BEGIN -> LvglEventType.DRAW_PART_BEGIN
    LV_EVENT_DRAW_PART_END -> LvglEventType.DRAW_PART_END
    LV_EVENT_DRAW_POST -> LvglEventType.DRAW_POST
    LV_EVENT_DRAW_POST_BEGIN -> LvglEventType.DRAW_POST_BEGIN
    LV_EVENT_DRAW_POST_END -> LvglEventType.DRAW_POST_END
    LV_EVENT_DRAW_MAIN_BEGIN -> LvglEventType.DRAW_MAIN_BEGIN
    LV_EVENT_REFR_EXT_DRAW_SIZE -> LvglEventType.REFRESH_EXT_DRAW_SIZE

    LV_EVENT_CANCEL -> LvglEventType.CANCEL
    LV_EVENT_INSERT -> LvglEventType.INSERT
    LV_EVENT_REFRESH -> LvglEventType.REFRESH
    LV_EVENT_VALUE_CHANGED -> LvglEventType.VALUE_CHANGED
    LV_EVENT_READY -> LvglEventType.READY

    LV_EVENT_DELETE -> LvglEventType.DELETE
    LV_EVENT_CHILD_CHANGED -> LvglEventType.CHILD_CHANGED
    LV_EVENT_GET_SELF_SIZE -> LvglEventType.GET_SELF_SIZE
    LV_EVENT_LAYOUT_CHANGED -> LvglEventType.LAYOUT_CHANGED
    LV_EVENT_SIZE_CHANGED -> LvglEventType.SIZE_CHANGED
    LV_EVENT_STYLE_CHANGED -> LvglEventType.STYLE_CHANGED
    else -> throw IllegalStateException("Unrecognised value")
}
