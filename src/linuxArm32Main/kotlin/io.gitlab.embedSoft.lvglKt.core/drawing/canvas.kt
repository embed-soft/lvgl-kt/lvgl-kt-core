package io.gitlab.embedSoft.lvglKt.core.drawing

import io.gitlab.embedSoft.lvglKt.core.Area
import io.gitlab.embedSoft.lvglKt.core.Point
import io.gitlab.embedSoft.lvglKt.core.StringReference
import io.gitlab.embedSoft.lvglKt.core.lvglObject.LvglObjectBase
import io.gitlab.embedSoft.lvglKt.core.styling.Color
import io.gitlab.embedSoft.lvglKt.core.styling.toCValue
import io.gitlab.embedSoft.lvglKt.core.styling.toColor
import kotlinx.cinterop.*
import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.*

public actual class Canvas private constructor(parent: LvglObjectBase): LvglObjectBase {
    override val lvObjPtr: CPointer<lv_obj_t>? = lv_canvas_create(parent.lvObjPtr)
    public val lvCanvasPtr: CPointer<lv_canvas_t>?
        get() = lvObjPtr?.reinterpret()

    // TODO: Implement lv_canvas_copy_buf as copyBuffer.
    // TODO: Implement lv_canvas_get_img as getImage.
    // TODO: Implement lv_canvas_transform as transform.

    public actual fun setBuffer(buf: CanvasBuffer, width: Short, height: Short, colorFormat: UByte) {
        lv_canvas_set_buffer(canvas = lvObjPtr, w = width, h = height, cf = colorFormat, buf = buf.lvColorPtr)
    }

    public actual companion object {
        public actual fun create(parent: LvglObjectBase): Canvas = Canvas(parent)
    }

    public actual fun getPixels(xPos: Short, yPos: Short): Color =
        lvglkt_canvas_get_px(canvas = lvObjPtr, x = xPos, y = yPos).toColor()

    public actual fun fillBackground(color: Color, opacity: UByte) {
        lvglkt_canvas_fill_bg(canvas = lvObjPtr, color = color.toCValue(), opa = opacity)
    }

    public actual fun setPixels(xPos: Short, yPos: Short, color: Color) {
        lvglkt_canvas_set_px(canvas = lvObjPtr, x = xPos, y = yPos, color = color.toCValue())
    }

    public actual fun setPalette(id: UByte, color: Color) {
        lvglkt_canvas_set_palette(canvas = lvObjPtr, id = id, color = color.toCValue())
    }

    public actual fun drawRectangle(
        xPos: Short,
        yPos: Short,
        width: Short,
        height: Short,
        drawDesc: RectangleDescriptor
    ) {
        lv_canvas_draw_rect(canvas = lvObjPtr, w = width, h = height, x = xPos, y = yPos,
            draw_dsc = drawDesc.lvDrawRectDscPtr)
    }

    public actual fun drawText(
        xPos: Short,
        yPos: Short,
        maxWidth: Short,
        txt: String,
        drawDesc: LabelDescriptor
    ) {
        lv_canvas_draw_text(canvas = lvObjPtr, txt = txt, draw_dsc = drawDesc.lvDrawLabelDscPtr, x = xPos, y = yPos,
            max_w = maxWidth)
    }

    public actual fun blurHorizontally(area: Area?, radius: UShort) {
        lv_canvas_blur_hor(canvas = lvObjPtr, area = area?.lvAreaPtr, r = radius)
    }

    public actual fun blurVertically(area: Area?, radius: UShort) {
        lv_canvas_blur_ver(canvas = lvObjPtr, area = area?.lvAreaPtr, r = radius)
    }

    public actual fun drawImage(
        xPos: Short,
        yPos: Short,
        src: StringReference,
        drawDesc: DrawImageDescriptor
    ) {
        lv_canvas_draw_img(canvas = lvObjPtr, x = xPos, y = yPos, src = src.ref, draw_dsc = drawDesc.lvDrawImgDscPtr)
    }

    public actual fun drawImage(
        xPos: Short,
        yPos: Short,
        src: String,
        drawDesc: DrawImageDescriptor
    ): StringReference {
        val result = StringReference.create(src)
        lv_canvas_draw_img(canvas = lvObjPtr, x = xPos, y = yPos, src = result.ref, draw_dsc = drawDesc.lvDrawImgDscPtr)
        return result
    }

    public actual fun drawLine(
        drawDesc: LineDescriptor,
        vararg points: Point
    ): Unit = memScoped {
        if (points.size < 2) throw IllegalArgumentException("Two or more points must be supplied to this function!")
        val tmpArray = allocArray<lv_point_t>(points.size)
        points.forEachIndexed { pos, item ->
            tmpArray[pos].x = item.xPos
            tmpArray[pos].y = item.yPos
        }
        lv_canvas_draw_line(canvas = lvObjPtr, draw_dsc = drawDesc.lvDrawLineDscPtr, point_cnt = points.size.toUInt(),
            points = tmpArray)
    }

    public actual fun drawPolygon(
        drawDesc: RectangleDescriptor,
        vararg points: Point
    ): Unit = memScoped {
        val tmpArray = allocArray<lv_point_t>(points.size)
        points.forEachIndexed { pos, item ->
            tmpArray[pos].x = item.xPos
            tmpArray[pos].y = item.yPos
        }
        lv_canvas_draw_polygon(
            canvas = lvObjPtr,
            point_cnt = points.size.toUInt(),
            draw_dsc = drawDesc.lvDrawRectDscPtr,
            points = tmpArray
        )
    }

    public actual fun drawArc(
        xPos: Short,
        yPos: Short,
        radius: Short,
        startAngle: Int,
        endAngle: Int,
        drawDesc: ArcDescriptor
    ) {
        lv_canvas_draw_arc(
            canvas = lvObjPtr,
            draw_dsc = drawDesc.lvDrawArcDscPtr,
            x = xPos,
            y = yPos,
            r = radius,
            start_angle = startAngle,
            end_angle = endAngle
        )
    }

    public actual fun drawLine(
        drawDesc: LineDescriptor,
        vararg points: Pair<Short, Short>
    ): Unit = memScoped {
        if (points.size < 2) throw IllegalArgumentException("Two or more points must be supplied to this function!")
        val tmpArray = allocArray<lv_point_t>(points.size)
        points.forEachIndexed { pos, item ->
            tmpArray[pos].x = item.first
            tmpArray[pos].y = item.second
        }
        lv_canvas_draw_line(canvas = lvObjPtr, draw_dsc = drawDesc.lvDrawLineDscPtr, point_cnt = points.size.toUInt(),
            points = tmpArray)
    }

    public actual fun drawPolygon(
        drawDesc: RectangleDescriptor,
        vararg points: Pair<Short, Short>
    ): Unit = memScoped {
        val tmpArray = allocArray<lv_point_t>(points.size)
        points.forEachIndexed { pos, item ->
            tmpArray[pos].x = item.first
            tmpArray[pos].y = item.second
        }
        lv_canvas_draw_polygon(
            canvas = lvObjPtr,
            point_cnt = points.size.toUInt(),
            draw_dsc = drawDesc.lvDrawRectDscPtr,
            points = tmpArray
        )
    }
}
