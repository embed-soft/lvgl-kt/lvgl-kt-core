package io.gitlab.embedSoft.lvglKt.core.drawing

import io.gitlab.embedSoft.lvglKt.core.Closable
import io.gitlab.embedSoft.lvglKt.core.StringReference
import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.*
import io.gitlab.embedSoft.lvglKt.core.styling.Color
import io.gitlab.embedSoft.lvglKt.core.styling.toCValue
import io.gitlab.embedSoft.lvglKt.core.styling.toColor
import kotlinx.cinterop.*

public actual class RectangleDescriptor private constructor(ptr: CPointer<lv_draw_rect_dsc_t>? = null): Closable {
    // TODO: Improve background gradient stops support.
    private val arena = if (ptr == null) Arena() else null
    public val lvDrawRectDscPtr: CPointer<lv_draw_rect_dsc_t>? = ptr ?: createPointer()
    public actual var backgroundGradientDither: UByte
        get() = lvDrawRectDscPtr?.pointed?.bg_grad?.dither ?: 0u
        set(value) {
            lvDrawRectDscPtr?.pointed?.bg_grad?.dither = value
        }
    public actual var backgroundGradientStopsCount: UByte
        get() = lvDrawRectDscPtr?.pointed?.bg_grad?.stops_count ?: 0u
        set(value) {
            lvDrawRectDscPtr?.pointed?.bg_grad?.stops_count = value
        }
    public actual var backgroundColor: Color
        get() = lvglkt_draw_rect_dsc_get_bg_color(lvDrawRectDscPtr).toColor()
        set(value) {
            lvglkt_draw_rect_dsc_set_bg_color(lvDrawRectDscPtr, value.toCValue())
        }
    public actual var backgroundGradientDirection: UByte
        get() = lvDrawRectDscPtr?.pointed?.bg_grad?.dir ?: 0u.toUByte()
        set(value) {
            lvDrawRectDscPtr?.pointed?.bg_grad?.dir = value
        }
    public actual var backgroundImageOpacity: UByte
        get() = lvDrawRectDscPtr?.pointed?.bg_img_opa ?: 0u.toUByte()
        set(value) {
            lvDrawRectDscPtr?.pointed?.bg_img_opa = value
        }
    public actual var backgroundGradientStopColor: Color
        get() = lvglkt_draw_rect_dsc_get_bg_grad_stops_color(lvDrawRectDscPtr).toColor()
        set(value) {
            lvglkt_draw_rect_dsc_set_bg_grad_stops_color(lvDrawRectDscPtr, value.toCValue())
        }
    public actual var backgroundImageRecolor: Color
        get() = lvglkt_draw_rect_dsc_get_bg_img_recolor(lvDrawRectDscPtr).toColor()
        set(value) {
            lvglkt_draw_rect_dsc_set_bg_img_recolor(lvDrawRectDscPtr, value.toCValue())
        }
    public actual var backgroundImageRecolorOpacity: UByte
        get() = lvDrawRectDscPtr?.pointed?.bg_img_recolor_opa ?: 0u.toUByte()
        set(value) {
            lvDrawRectDscPtr?.pointed?.bg_img_recolor_opa = value
        }
    public actual var shadowColor: Color
        get() = lvglkt_draw_rect_dsc_get_shadow_color(lvDrawRectDscPtr).toColor()
        set(value) {
            lvglkt_draw_rect_dsc_set_shadow_color(lvDrawRectDscPtr, value.toCValue())
        }
    public actual var shadowXOffset: Short
        get() = lvDrawRectDscPtr?.pointed?.shadow_ofs_x ?: 0.toShort()
        set(value) {
            lvDrawRectDscPtr?.pointed?.shadow_ofs_x = value
        }
    public actual var shadowYOffset: Short
        get() = lvDrawRectDscPtr?.pointed?.shadow_ofs_y ?: 0.toShort()
        set(value) {
            lvDrawRectDscPtr?.pointed?.shadow_ofs_y = value
        }
    public actual var shadowOpacity: UByte
        get() = lvDrawRectDscPtr?.pointed?.shadow_opa ?: 0u.toUByte()
        set(value) {
            lvDrawRectDscPtr?.pointed?.shadow_opa = value
        }
    public actual var shadowSpread: Short
        get() = lvDrawRectDscPtr?.pointed?.shadow_spread ?: 0.toShort()
        set(value) {
            lvDrawRectDscPtr?.pointed?.shadow_spread = value
        }
    public actual var shadowWidth: Short
        get() = lvDrawRectDscPtr?.pointed?.shadow_width ?: 0.toShort()
        set(value) {
            lvDrawRectDscPtr?.pointed?.shadow_width = value
        }
    public actual var outlineColor: Color
        get() = lvglkt_draw_rect_dsc_get_outline_color(lvDrawRectDscPtr).toColor()
        set(value) {
            lvglkt_draw_rect_dsc_set_outline_color(lvDrawRectDscPtr, value.toCValue())
        }
    public actual var outlineOpacity: UByte
        get() = lvDrawRectDscPtr?.pointed?.outline_opa ?: 0u.toUByte()
        set(value) {
            lvDrawRectDscPtr?.pointed?.outline_opa = value
        }
    public actual var outlinePad: Short
        get() = lvDrawRectDscPtr?.pointed?.outline_pad ?: 0.toShort()
        set(value) {
            lvDrawRectDscPtr?.pointed?.outline_pad = value
        }
    public actual var outlineWidth: Short
        get() = lvDrawRectDscPtr?.pointed?.outline_width ?: 0.toShort()
        set(value) {
            lvDrawRectDscPtr?.pointed?.outline_width = value
        }
    public actual var radius: Short
        get() = lvDrawRectDscPtr?.pointed?.radius ?: 0.toShort()
        set(value) {
            lvDrawRectDscPtr?.pointed?.radius = value
        }
    public actual var borderColor: Color
        get() = lvglkt_draw_rect_dsc_get_border_color(lvDrawRectDscPtr).toColor()
        set(value) {
            lvglkt_draw_rect_dsc_set_border_color(lvDrawRectDscPtr, value.toCValue())
        }
    public actual var borderOpacity: UByte
        get() = lvDrawRectDscPtr?.pointed?.border_opa ?: 0u.toUByte()
        set(value) {
            lvDrawRectDscPtr?.pointed?.border_opa = value
        }
    public actual var borderPost: UByte
        get() = lvDrawRectDscPtr?.pointed?.border_post ?: 0u.toUByte()
        set(value) {
            lvDrawRectDscPtr?.pointed?.border_post = value
        }
    public actual var borderSide: UByte
        get() = lvDrawRectDscPtr?.pointed?.border_side ?: 0u.toUByte()
        set(value) {
            lvDrawRectDscPtr?.pointed?.border_side = value
        }
    public actual var borderWidth: Short
        get() = lvDrawRectDscPtr?.pointed?.border_width ?: 0.toShort()
        set(value) {
            lvDrawRectDscPtr?.pointed?.border_width = value
        }
    public actual var blendMode: UByte
        get() = lvDrawRectDscPtr?.pointed?.blend_mode ?: 0u.toUByte()
        set(value) {
            lvDrawRectDscPtr?.pointed?.blend_mode = value
        }
    public actual var backgroundOpacity: UByte
        get() = lvDrawRectDscPtr?.pointed?.bg_opa ?: 0u.toUByte()
        set(value) {
            lvDrawRectDscPtr?.pointed?.bg_opa = value
        }
    public actual var backgroundImageTiled: Boolean
        get() = lvDrawRectDscPtr?.pointed?.bg_img_tiled == 1u.toUByte()
        set(value) {
            lvDrawRectDscPtr?.pointed?.bg_img_tiled = if (value) 1u.toUByte() else 0u.toUByte()
        }

    private fun createPointer(): CPointer<lv_draw_rect_dsc_t>? {
        val result = arena?.alloc<lv_draw_rect_dsc_t>()?.ptr
        lv_draw_rect_dsc_init(result)
        return result
    }

    public actual companion object {
        public actual fun create(): RectangleDescriptor = RectangleDescriptor()

        public fun fromPointer(ptr: CPointer<lv_draw_rect_dsc_t>?): RectangleDescriptor = RectangleDescriptor(ptr)
    }

    override fun close() {
        arena?.clear()
    }

    public actual fun setBackgroundImageSource(value: String): StringReference {
        val result = StringReference.create(value)
        lvDrawRectDscPtr?.pointed?.bg_img_src = result.ref
        return result
    }

    public actual fun setBackgroundImageSource(value: StringReference) {
        lvDrawRectDscPtr?.pointed?.bg_img_src = value.ref
    }

    public actual fun getBackgroundImageSourceAsString(): String =
        lvDrawRectDscPtr?.pointed?.bg_img_src?.reinterpret<ByteVar>()?.toKString() ?: ""
}

public fun CPointer<lv_draw_rect_dsc_t>?.toRectangleDescriptor(): RectangleDescriptor =
    RectangleDescriptor.fromPointer(this)
