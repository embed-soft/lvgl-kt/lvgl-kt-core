package io.gitlab.embedSoft.lvglKt.core.drawing

import io.gitlab.embedSoft.lvglKt.core.Closable
import kotlinx.cinterop.*
import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.LV_COLOR_SIZE
import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.LV_IMG_PX_SIZE_ALPHA_BYTE
import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.lv_color_t

public actual class CanvasBuffer private constructor(bufSize: UInt) : Closable {
    private val arena = Arena()
    public val lvColorPtr: CPointer<lv_color_t> = arena.allocArray(bufSize.toInt())

    public actual companion object {
        public actual fun create(bufSize: UInt): CanvasBuffer = CanvasBuffer(bufSize)
    }

    override fun close() {
        arena.clear()
    }
}

public actual fun trueColorBufferSize(width: Short, height: Short): UInt =
    ((LV_COLOR_SIZE / 8) * width * height).toUInt()

public actual fun trueColorAlphaBufferSize(width: Short, height: Short): UInt =
    (LV_IMG_PX_SIZE_ALPHA_BYTE * width * height).toUInt()
