package io.gitlab.embedSoft.lvglKt.core

import io.gitlab.embedSoft.lvglKt.core.animation.ScreenLoadAnimation
import io.gitlab.embedSoft.lvglKt.core.animation.toLvScrLoadAnim
import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.lv_scr_act
import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.lv_scr_load
import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.lv_scr_load_anim
import io.gitlab.embedSoft.lvglKt.core.lvglObject.LvglObject
import io.gitlab.embedSoft.lvglKt.core.lvglObject.LvglObjectBase

public actual class Screen private constructor(newLvglObject: LvglObjectBase): Closable {
    private var _lvglObject: LvglObjectBase? = newLvglObject
    public actual val lvglObject: LvglObjectBase?
        get() = _lvglObject

    public actual companion object {
        public actual val activeScreen: LvglObjectBase
            get() = LvglObject.fromPointer(lv_scr_act())

        public actual fun create(obj: LvglObjectBase): Screen = Screen(obj)

        public actual fun load(screen: LvglObjectBase) {
            lv_scr_load(screen.lvObjPtr)
        }

        public actual fun load(screen: Screen) {
            lv_scr_load(screen.lvglObject?.lvObjPtr)
        }

        public actual fun loadAnimation(
            screen: LvglObjectBase,
            time: UInt,
            delay: UInt,
            autoDel: Boolean,
            animType: ScreenLoadAnimation
        ) {
            lv_scr_load_anim(scr = screen.lvObjPtr, time = time, delay = delay, auto_del = autoDel,
                anim_type = animType.toLvScrLoadAnim())
        }

        public actual fun loadAnimation(
            screen: Screen,
            time: UInt,
            delay: UInt,
            autoDel: Boolean,
            animType: ScreenLoadAnimation
        ) {
            lv_scr_load_anim(scr = screen.lvglObject?.lvObjPtr, time = time, delay = delay, auto_del = autoDel,
                anim_type = animType.toLvScrLoadAnim())
        }
    }

    override fun close() {
        _lvglObject?.close()
        _lvglObject = null
    }
}
