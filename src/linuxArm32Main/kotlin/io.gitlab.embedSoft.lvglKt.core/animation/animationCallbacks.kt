package io.gitlab.embedSoft.lvglKt.core.animation

import io.gitlab.embedSoft.lvglKt.core.Closable
import kotlinx.cinterop.*
import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.*

public actual class AnimationCallbackRegistry private constructor(
    public val execCallback: AnimationExecuteCallback,
    public val pathCallback: AnimationPathCallback,
    public val readyCallback: AnimationReadyCallback,
    public val startCallback: AnimationStartCallback,
    public val getValueCallback: AnimationGetValueCallback,
    public val userData: Any? = null
) : Closable {
    internal val stableRef = StableRef.create(this)
    public val lvAnimCustomExecCbPtr: lv_anim_custom_exec_cb_t =
        staticCFunction { animation: CPointer<lv_anim_t>?, value: Int ->
            val tmpObj = animation?.pointed?.user_data?.asStableRef<AnimationCallbackRegistry>()?.get()
            tmpObj?.execCallback?.invoke(animation.toAnimation(), value, tmpObj.userData)
        }
    public val lvAnimPathCbPtr: lv_anim_path_cb_t =
        staticCFunction { animation: CPointer<lv_anim_t>? ->
            val tmpObj = animation?.pointed?.user_data?.asStableRef<AnimationCallbackRegistry>()?.get()
            tmpObj?.pathCallback?.invoke(animation.toAnimation(), tmpObj.userData) ?: 0
        }
    public val lvAnimReadyCbPtr: lv_anim_ready_cb_t =
        staticCFunction { animation: CPointer<lv_anim_t>? ->
            val tmpObj = animation?.pointed?.user_data?.asStableRef<AnimationCallbackRegistry>()?.get()
            tmpObj?.readyCallback?.invoke(animation.toAnimation(), tmpObj.userData)
        }
    public val lvAnimStartCbPtr: lv_anim_start_cb_t =
        staticCFunction { animation: CPointer<lv_anim_t>? ->
            val tmpObj = animation?.pointed?.user_data?.asStableRef<AnimationCallbackRegistry>()?.get()
            tmpObj?.startCallback?.invoke(animation.toAnimation(), tmpObj.userData)
        }
    public val lvAnimGetValueCbPtr: lv_anim_get_value_cb_t =
        staticCFunction { animation: CPointer<lv_anim_t>? ->
            val tmpObj = animation?.pointed?.user_data?.asStableRef<AnimationCallbackRegistry>()?.get()
            tmpObj?.getValueCallback?.invoke(animation.toAnimation(), tmpObj.userData) ?: 0
        }

    public actual companion object {
        public actual fun create(
            execCallback: AnimationExecuteCallback,
            pathCallback: AnimationPathCallback,
            readyCallback: AnimationReadyCallback,
            startCallback: AnimationStartCallback,
            getValueCallback: AnimationGetValueCallback,
            userData: Any?
        ): AnimationCallbackRegistry = AnimationCallbackRegistry(
            execCallback = execCallback,
            pathCallback = pathCallback,
            readyCallback = readyCallback,
            startCallback = startCallback,
            getValueCallback = getValueCallback
        )
    }

    override fun close() {
        stableRef.dispose()
    }
}
