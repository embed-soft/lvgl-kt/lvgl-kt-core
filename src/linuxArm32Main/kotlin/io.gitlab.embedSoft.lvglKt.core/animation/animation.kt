package io.gitlab.embedSoft.lvglKt.core.animation

import io.gitlab.embedSoft.lvglKt.core.Closable
import kotlinx.cinterop.*
import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.*

public actual class Animation private constructor(ptr: CPointer<lv_anim_t>? = null): Closable {
    private val arena = if (ptr == null) Arena() else null
    public val lvAnimPtr: CPointer<lv_anim_t>? = ptr ?: createPointer()
    public actual var delay: UInt
        get() = lv_anim_get_delay(lvAnimPtr)
        set(value) {
            lv_anim_set_delay(lvAnimPtr, value)
        }
    public actual var time: UInt
        get() = lvAnimPtr?.pointed?.time?.toUInt() ?: 0u
        set(value) {
            lv_anim_set_time(lvAnimPtr, value)
        }
    public actual var playbackTime: UInt
        get() = lvAnimPtr?.pointed?.playback_time ?: 0u
        set(value) {
            lv_anim_set_playback_time(lvAnimPtr, value)
        }
    public actual var playbackDelay: UInt
        get() = lvAnimPtr?.pointed?.playback_delay ?: 0u
        set(value) {
            lv_anim_set_playback_delay(lvAnimPtr, value)
        }
    public actual var repeatCount: UShort
        get() = lvAnimPtr?.pointed?.repeat_cnt ?: 0.toUShort()
        set(value) {
            lv_anim_set_repeat_count(lvAnimPtr, value)
        }
    public actual var repeatDelay: UInt
        get() = lvAnimPtr?.pointed?.repeat_delay ?: 0u
        set(value) {
            lv_anim_set_repeat_delay(lvAnimPtr, value)
        }
    public actual var earlyApply: Boolean
        get() = lvAnimPtr?.pointed?.early_apply == 1u.toUByte()
        set(value) {
            lv_anim_set_early_apply(lvAnimPtr, value)
        }

    private fun createPointer(): CPointer<lv_anim_t>? {
        val result = arena?.alloc<lv_anim_t>()?.ptr
        lv_anim_init(result)
        return result
    }

    public actual companion object {
        public fun fromPointer(ptr: CPointer<lv_anim_t>?): Animation = Animation(ptr)

        public actual fun create(): Animation = Animation()

        public actual val totalRunning: UShort
            get() = lv_anim_count_running()

        public actual fun deleteAll() {
            lv_anim_del_all()
        }

        public actual fun speedToTime(speed: UInt, start: Int, end: Int): UInt =
            lv_anim_speed_to_time(speed = speed, start = start, end = end)

        public actual fun refreshNow() {
            lv_anim_refr_now()
        }
    }

    override fun close() {
        arena?.clear()
    }

    public actual fun setValues(start: Int, end: Int) {
        lv_anim_set_values(a = lvAnimPtr, start = start, end = end)
    }

    public actual fun start(): Animation = fromPointer(lv_anim_start(lvAnimPtr))

    public actual fun linearPath(): Int = lv_anim_path_linear(lvAnimPtr)

    public actual fun easeInPath(): Int = lv_anim_path_ease_in(lvAnimPtr)

    public actual fun easeOutPath(): Int = lv_anim_path_ease_out(lvAnimPtr)

    public actual fun easeInOutPath(): Int = lv_anim_path_ease_in_out(lvAnimPtr)

    public actual fun overshootPath(): Int = lv_anim_path_overshoot(lvAnimPtr)

    public actual fun bouncePath(): Int = lv_anim_path_bounce(lvAnimPtr)

    public actual fun stepPath(): Int = lv_anim_path_step(lvAnimPtr)

    public actual fun changeCallbacks(registry: AnimationCallbackRegistry) {
        lvAnimPtr?.pointed?.user_data = registry.stableRef.asCPointer()
        lv_anim_set_custom_exec_cb(lvAnimPtr, registry.lvAnimCustomExecCbPtr)
        lv_anim_set_path_cb(lvAnimPtr, registry.lvAnimPathCbPtr)
        lv_anim_set_ready_cb(lvAnimPtr, registry.lvAnimReadyCbPtr)
        lv_anim_set_start_cb(lvAnimPtr, registry.lvAnimStartCbPtr)
        lv_anim_set_get_value_cb(lvAnimPtr, registry.lvAnimGetValueCbPtr)
    }
}

public fun CPointer<lv_anim_t>?.toAnimation(): Animation = Animation.fromPointer(this)
