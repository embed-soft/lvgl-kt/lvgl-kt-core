package io.gitlab.embedSoft.lvglKt.core.lvglObject

import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.*

public actual val LvglAlignment.value: UInt
    get() = when(this) {
        LvglAlignment.LEFT_MID -> LV_ALIGN_LEFT_MID
        LvglAlignment.BOTTOM_LEFT -> LV_ALIGN_BOTTOM_LEFT
        LvglAlignment.BOTTOM_MID -> LV_ALIGN_BOTTOM_MID
        LvglAlignment.CENTER -> LV_ALIGN_CENTER
        LvglAlignment.BOTTOM_RIGHT -> LV_ALIGN_BOTTOM_RIGHT
        LvglAlignment.TOP_LEFT -> LV_ALIGN_TOP_LEFT
        LvglAlignment.TOP_MID -> LV_ALIGN_TOP_MID
        LvglAlignment.RIGHT_MID -> LV_ALIGN_RIGHT_MID
        LvglAlignment.TOP_RIGHT -> LV_ALIGN_TOP_RIGHT
    }

public actual fun UInt.toLvglAlignment(): LvglAlignment = when(this) {
    LV_ALIGN_LEFT_MID -> LvglAlignment.LEFT_MID
    LV_ALIGN_BOTTOM_LEFT -> LvglAlignment.BOTTOM_LEFT
    LV_ALIGN_BOTTOM_MID -> LvglAlignment.BOTTOM_MID
    LV_ALIGN_CENTER -> LvglAlignment.CENTER
    LV_ALIGN_BOTTOM_RIGHT -> LvglAlignment.BOTTOM_RIGHT
    LV_ALIGN_TOP_LEFT -> LvglAlignment.TOP_LEFT
    LV_ALIGN_TOP_MID -> LvglAlignment.TOP_MID
    LV_ALIGN_RIGHT_MID -> LvglAlignment.RIGHT_MID
    LV_ALIGN_TOP_RIGHT -> LvglAlignment.TOP_RIGHT
    else -> throw IllegalStateException("Unrecognised value")
}

public actual val LvglOutsideAlignment.value: UInt
    get() = when(this) {
        LvglOutsideAlignment.TOP_LEFT -> LV_ALIGN_OUT_TOP_LEFT
        LvglOutsideAlignment.BOTTOM_LEFT -> LV_ALIGN_OUT_BOTTOM_LEFT
        LvglOutsideAlignment.BOTTOM_MID -> LV_ALIGN_OUT_BOTTOM_MID
        LvglOutsideAlignment.BOTTOM_RIGHT -> LV_ALIGN_OUT_BOTTOM_RIGHT
        LvglOutsideAlignment.LEFT_MID -> LV_ALIGN_OUT_LEFT_MID
        LvglOutsideAlignment.LEFT_TOP -> LV_ALIGN_OUT_LEFT_TOP
        LvglOutsideAlignment.LEFT_BOTTOM -> LV_ALIGN_OUT_LEFT_BOTTOM
        LvglOutsideAlignment.RIGHT_BOTTOM -> LV_ALIGN_OUT_RIGHT_BOTTOM
        LvglOutsideAlignment.RIGHT_MID -> LV_ALIGN_OUT_RIGHT_MID
        LvglOutsideAlignment.RIGHT_TOP -> LV_ALIGN_OUT_RIGHT_TOP
        LvglOutsideAlignment.TOP_MID -> LV_ALIGN_OUT_TOP_MID
        LvglOutsideAlignment.TOP_RIGHT -> LV_ALIGN_OUT_TOP_RIGHT
    }

public actual fun UInt.toLvglOutsideAlignment(): LvglOutsideAlignment = when(this) {
    LV_ALIGN_OUT_TOP_LEFT -> LvglOutsideAlignment.TOP_LEFT
    LV_ALIGN_OUT_BOTTOM_LEFT -> LvglOutsideAlignment.BOTTOM_LEFT
    LV_ALIGN_OUT_BOTTOM_MID -> LvglOutsideAlignment.BOTTOM_MID
    LV_ALIGN_OUT_BOTTOM_RIGHT -> LvglOutsideAlignment.BOTTOM_RIGHT
    LV_ALIGN_OUT_LEFT_MID -> LvglOutsideAlignment.LEFT_MID
    LV_ALIGN_OUT_LEFT_TOP -> LvglOutsideAlignment.LEFT_TOP
    LV_ALIGN_OUT_LEFT_BOTTOM -> LvglOutsideAlignment.LEFT_BOTTOM
    LV_ALIGN_OUT_RIGHT_BOTTOM -> LvglOutsideAlignment.RIGHT_BOTTOM
    LV_ALIGN_OUT_RIGHT_MID -> LvglOutsideAlignment.RIGHT_MID
    LV_ALIGN_OUT_RIGHT_TOP -> LvglOutsideAlignment.RIGHT_TOP
    LV_ALIGN_OUT_TOP_MID -> LvglOutsideAlignment.TOP_MID
    LV_ALIGN_OUT_TOP_RIGHT -> LvglOutsideAlignment.TOP_RIGHT
    else -> throw IllegalStateException("Unrecognised value")
}
