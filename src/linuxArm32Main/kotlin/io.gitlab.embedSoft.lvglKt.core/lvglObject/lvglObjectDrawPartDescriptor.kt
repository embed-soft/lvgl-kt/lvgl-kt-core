package io.gitlab.embedSoft.lvglKt.core.lvglObject

import io.gitlab.embedSoft.lvglKt.core.Area
import io.gitlab.embedSoft.lvglKt.core.Point
import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.lv_obj_draw_part_dsc_t
import io.gitlab.embedSoft.lvglKt.core.drawing.*
import io.gitlab.embedSoft.lvglKt.core.toArea
import io.gitlab.embedSoft.lvglKt.core.toPoint
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.pointed
import kotlinx.cinterop.toKString

public actual class LvglObjectDrawPartDescriptor private constructor(ptr: CPointer<lv_obj_draw_part_dsc_t>?){
    public val lvObjDrawPartDscPtr: CPointer<lv_obj_draw_part_dsc_t>? = ptr
    public actual val drawArea: Area
        get() = lvObjDrawPartDscPtr?.pointed?.draw_area.toArea()
    public actual val point1: Point
        get() = lvObjDrawPartDscPtr?.pointed?.p1.toPoint()
    public actual val point2: Point
        get() = lvObjDrawPartDscPtr?.pointed?.p2.toPoint()
    public actual val part: UInt
        get() = lvObjDrawPartDscPtr?.pointed?.part ?: 0u
    public actual val id: UInt
        get() = lvObjDrawPartDscPtr?.pointed?.id ?: 0u
    public actual val radius: Short
        get() = lvObjDrawPartDscPtr?.pointed?.radius ?: 0.toShort()
    public actual val value: Int
        get() = lvObjDrawPartDscPtr?.pointed?.value ?: 0
    public actual val text: String
        get() = lvObjDrawPartDscPtr?.pointed?.text?.toKString() ?: ""
    public actual val rectDescriptor: RectangleDescriptor
        get() = lvObjDrawPartDscPtr?.pointed?.rect_dsc.toRectangleDescriptor()
    public actual val labelDescriptor: LabelDescriptor
        get() = lvObjDrawPartDscPtr?.pointed?.label_dsc.toLabelDescriptor()
    public actual val lineDescriptor: LineDescriptor
        get() = lvObjDrawPartDscPtr?.pointed?.line_dsc.toLineDescriptor()
    public actual val imageDescriptor: DrawImageDescriptor
        get() = lvObjDrawPartDscPtr?.pointed?.img_dsc.toDrawImageDescriptor()
    public actual val arcDescriptor: ArcDescriptor
        get() = lvObjDrawPartDscPtr?.pointed?.arc_dsc.toArcDescriptor()

    public companion object {
        public fun fromPointer(ptr: CPointer<lv_obj_draw_part_dsc_t>?): LvglObjectDrawPartDescriptor =
            LvglObjectDrawPartDescriptor(ptr)
    }
}

public fun CPointer<lv_obj_draw_part_dsc_t>?.toLvglObjectDrawPartDescriptor(): LvglObjectDrawPartDescriptor =
    LvglObjectDrawPartDescriptor.fromPointer(this)
