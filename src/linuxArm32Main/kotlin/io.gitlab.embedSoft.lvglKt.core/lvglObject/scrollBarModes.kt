package io.gitlab.embedSoft.lvglKt.core.lvglObject

import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.LV_SCROLLBAR_MODE_ACTIVE
import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.LV_SCROLLBAR_MODE_AUTO
import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.LV_SCROLLBAR_MODE_OFF
import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.LV_SCROLLBAR_MODE_ON

public actual fun UByte.toScrollBarMode(): ScrollBarMode = when (this) {
    LV_SCROLLBAR_MODE_OFF.toUByte() -> ScrollBarMode.OFF
    LV_SCROLLBAR_MODE_ON.toUByte() -> ScrollBarMode.ON
    LV_SCROLLBAR_MODE_ACTIVE.toUByte() -> ScrollBarMode.ACTIVE
    LV_SCROLLBAR_MODE_AUTO.toUByte() -> ScrollBarMode.AUTO
    else -> throw IllegalStateException("Unrecognised state")
}
