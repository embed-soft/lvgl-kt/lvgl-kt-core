import java.util.Properties
import org.jetbrains.dokka.gradle.DokkaTask

val homeDir = System.getenv("HOME") ?: ""
val projectSettings = fetchProjectSettings()
val mavenCentralSettings = fetchMavenCentralSettings()
val secretPropsFile = File("maven_central.properties")

group = "io.gitlab.embed-soft"
version = if (projectSettings.isDevVer) "${projectSettings.libVer}-dev" else projectSettings.libVer

plugins {
    kotlin("multiplatform") version "1.7.21"
    `maven-publish`
    id("org.jetbrains.dokka") version "1.7.20"
    signing
}

ext["signing.keyId"] = mavenCentralSettings.signingKeyId
ext["signing.password"] = mavenCentralSettings.signingPassword
ext["signing.secretKeyRingFile"] = mavenCentralSettings.signingKeyRingFile
ext["ossrhUsername"] = mavenCentralSettings.ossrhUsername
ext["ossrhPassword"] = mavenCentralSettings.ossrhPassword

repositories {
    mavenCentral()
}

val dokkaHtml by tasks.getting(DokkaTask::class)

val javadocJar by tasks.register("javadocJar", Jar::class) {
    dependsOn(dokkaHtml)
    archiveClassifier.set("javadoc")
    from(dokkaHtml.outputDirectory)
}

kotlin {
    val lvglDir = "$homeDir/c_libs/lvgl/lvgl-8.3.3"
    val lvglCompilerOpts = arrayOf("-DLV_LVGL_H_INCLUDE_SIMPLE", "-DLV_CONF_SKIP")
    explicitApi()

    linuxArm32Hfp("linuxArm32") {
        compilations.getByName("main") {
            cinterops.create("lvgl") {
                includeDirs(lvglDir)
                compilerOpts(*lvglCompilerOpts)
            }
        }
    }

    linuxX64 {
        compilations.getByName("main") {
            cinterops.create("lvgl") {
                includeDirs(lvglDir)
                compilerOpts(*lvglCompilerOpts)
            }
        }
    }

    sourceSets {
        commonMain {
            dependencies {
                val kotlinVer = "1.7.21"
                implementation(kotlin("stdlib", kotlinVer))
            }
        }
        val linuxCommonMain by creating {
            dependsOn(getByName("commonMain"))
        }
        getByName("linuxX64Main") {
            dependsOn(linuxCommonMain)
        }
        getByName("linuxArm32Main") {
            dependsOn(linuxCommonMain)
        }
    }
}

publishing {
    publications.withType<MavenPublication> {
        if (projectSettings.includeDocs) artifact(javadocJar)
        createPom()
    }
    repositories {
        if (mavenCentralSettings.publishingEnabled) createMavenCentralRepo()
    }
}

val Boolean.intValue: Int
    get() = if (this) 1 else 0

signing {
    if (secretPropsFile.exists()) sign(publishing.publications)
}

fun getExtraString(name: String) = ext[name]?.toString()

data class ProjectSettings(val libVer: String, val isDevVer: Boolean, val includeDocs: Boolean)

data class MavenCentralSettings(
    val signingKeyId: String,
    val signingPassword: String,
    val signingKeyRingFile: String,
    val ossrhUsername: String,
    val ossrhPassword: String,
    val publishingEnabled: Boolean
)

fun fetchMavenCentralSettings(): MavenCentralSettings {
    var signingKeyId = ""
    var signingPassword = ""
    var signingKeyRingFile = "keyRing"
    var ossrhUsername = ""
    var ossrhPassword = ""
    var publishingEnabled = false
    val properties = Properties()
    val file = File("maven_central.properties")
    if (file.exists()) {
        file.inputStream().use { inputStream ->
            properties.load(inputStream)
            signingKeyId = properties.getProperty("signing.keyId")
            @Suppress("RemoveSingleExpressionStringTemplate")
            publishingEnabled = "${properties.getProperty("mavenCentral.publishingEnabled")}".toBoolean()
            signingPassword = properties.getProperty("signing.password")
            signingKeyRingFile = properties.getProperty("signing.secretKeyRingFile")
            ossrhUsername = properties.getProperty("ossrhUsername")
            ossrhPassword = properties.getProperty("ossrhPassword")
        }
    }
    return MavenCentralSettings(
        signingKeyId = signingKeyId,
        signingPassword = signingPassword,
        signingKeyRingFile = signingKeyRingFile,
        publishingEnabled = publishingEnabled,
        ossrhUsername = ossrhUsername,
        ossrhPassword = ossrhPassword
    )
}

fun fetchProjectSettings(): ProjectSettings {
    var libVer = "test"
    var isDevVer = false
    var includeDocs = false
    val properties = Properties()
    val file = File("project.properties")
    if (file.exists()) {
        file.inputStream().use { inputStream ->
            properties.load(inputStream)
            libVer = properties.getProperty("libVer") ?: "SNAPSHOT"
            @Suppress("RemoveSingleExpressionStringTemplate")
            isDevVer = "${properties.getProperty("isDevVer")}".toBoolean()
            @Suppress("RemoveSingleExpressionStringTemplate")
            includeDocs = "${properties.getProperty("includeDocs")}".toBoolean()
        }
    }
    return ProjectSettings(libVer = libVer, isDevVer = isDevVer, includeDocs = includeDocs)
}

fun RepositoryHandler.createMavenCentralRepo() {
    maven {
        name = "sonatype"
        setUrl("https://s01.oss.sonatype.org/service/local/staging/deploy/maven2/")
        credentials {
            username = getExtraString("ossrhUsername")
            password = getExtraString("ossrhPassword")
        }
    }
}

fun MavenPublication.createPom() = pom {
    name.set("LVGL KT Core")
    description.set("A Kotlin Native library that provides core LVGL functionality in a Kotlin Native project.")
    url.set("https://gitlab.com/embed-soft/lvgl-kt/lvgl-kt-core")

    licenses {
        license {
            name.set("Apache 2.0")
            url.set("https://opensource.org/licenses/Apache-2.0")
        }
    }
    developers {
        developer {
            id.set("NickApperley")
            name.set("Nick Apperley")
            email.set("napperley@protonmail.com")
        }
    }
    scm {
        url.set("https://gitlab.com/embed-soft/lvgl-kt/lvgl-kt-core")
    }
}

tasks.register("generateCMappings") {
    doFirst { println("Generating C mappings...") }
    dependsOn("cinteropLvglLinuxArm32", "cinteropLvglLinuxX64")
}
